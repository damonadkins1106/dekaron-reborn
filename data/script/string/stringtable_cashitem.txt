<cate> 170 #Cash Item Related

code 6 #Character Name Change
{
<center><color=fffc00><NAME></color>

<center><color=ffb400>Level <VALUE> <CLASS></color>
}

code 8 #Character Name Change
{
Please enter a new name for your character.
}

code 9 #UI alert message when buying a Transportation item
{
This item can only be purchased one piece at a time.
}

code 10 #Character Name Change
{
<center> <color = fffc00> Do you confirm whether or not to change the character name? </color>
<center> <color = ff0000> Characters must not be in a party. Press the Guild and then </color>
<center> <color = ff0000> and delete all friends in the list before changing the name successfully </color>
}

code 11 #Character Name Change
{
<center> <color = fffc00> Do you confirm that the character name is changed to this name? </color>
<center><color=ffffff><NAME> </color>
<center> <color = fffc00> You will return to the character selection window </color>
<center> <color = fffc00> The new name of the character Will update after new login again </color>
}

code 14 #Guild Name Change
{
<center><color=fffc00>Would you like to reset your guild's history of name changes?</color>
<center><color=ff0000>This will allow you to change the guild name for 1 time.</color>
}

code 18 #Wealthy Merchant's Stall
{
<center> Want to use Wealthy Merchant's Stall items?
<center> when used Private stores will have 10 * 10 additional channels.
<center> and increase the number of items sold from 10 pieces to 20 pieces
<center> Expandable: <color=00ff00><VALUE> </color> times
}

code 19 #Traveler's Backpack
{
<center> Want to use the Traveler's Backpack item?
<center> when using this item add an Secondary Inventory with 5 * 10 slots.
<center> Expandable: <color=00ff00><VALUE> </color> times
}

code 20 #Message when using Storage Keeper's Key
{
<center> Want to use the Storage Keeper's Key item?
<Center> When used, this item adds 1 shared space with 10 * 10 slots.
<center> Expandable: <color=00ff00><VALUE> </color> times
}

code 21 #Message when using Storage Mastery
{
<center> Want to use a Storage Mastery item?
this item adds 1 space with 10 * 10 slots.
<center> Expandable: <color=00ff00><VALUE> </color> times
}

code 22 #Error Message
{
You have used the item in full amount once. Not available anymore
}

code 25 #Error message when using Master's Mistake
{
The number of times your resumes are 0 times. Master's Mistake cannot be used at this time.
}

code 26 #Message when using Master's Mistake
{
<center>Your number of skill resets will be reset as follows.
<center>Current Number of Skill Resets:
 <color=00ff00><CUR_NUM></color> times
<center>New Number of Skill Resets: <color=00ff00><POST_NUM></color> 
}

code 28 #Strength Regenerator, Health Regenerator, Dexterity Regenerator, Spirit Regenerator
{
<center> Reset the status of <color=00ff00><NAME> </color> Back to Level 1 episode
<center> Current status of <NAME>: <color=00ff00><VALUE1> </color>
<center> New status of <NAME>: <color=00ff00><VALUE2> </color>
<center> status points returned: <color=00ff00><VALUE3> </color>
}

code 30 #Strength Regenerator
{
Strength
}

code 31 #Health Regenerator
{
Health
}

code 32 #Dexterity Regenerator
{
Dexterity
}

code 33 #Spirit Regenerator
{
Spirit
}

code 34 #Apparently not in use
{
Use again: <VALUE> times
}

code 35 #Portal Scroll
{
<center> Do you want to use the Portal Scroll item?
}

code 36
{
Do you want to use Portal?
}

code 37
{
<center> Do you want to use the Guild of Glory item?
}

code 38
{
Portal remaining time: <TIME>
}

code 39 # Random Box Message
{
[Message] Please select <VALUE> the desired item.
}

code 40 #Character Name Change
{
This new name has already been used.
}

code 41
{
Character. You still have a friend's name in the list. Please remove it first.
}

code 42
{
Your character has not yet issued a guild. Please leave the guild first.
}

code 43
{
The characters you haven't partyed yet Please leave the party first
}

code 44 
{
The character is in an Expedition.
Your character has not left the survey team. Please leave the team first.
}

code 45 # End of Random Box Message
{
[Message] Cannot use the item to call the box. Or a monster in the map that stands
}

code 46 #Error message when using the Character Name Change item in the Battlefield
{
Cannot use the item to change the name in the map that is standing.
}

code 47 # Awakening Ocarina
{
<center> Do you want to use Ocarina Awakening flute?
<center> The green flute will wake up for 30 days.
<center> <color = ff0000> can change the pet name Awake episode </color>
}

code 48 # Ocarina of Eternity
{
<center> Do you want to use the Ocarina of Eternity flute?
<center> The golden flute will wake up permanently.
<center> <color = ff0000> can change the pet name Awake episode </color>
}

code 49
{
<Adds 10% EXP when used>
<Adds 10% EXP and can be used in conjunction with other EXP bonus items>
<Enables you to teleport to a selected Field (without spending dil)>
<Enables you to teleport to a selected Instance Dungeon (without spending dil)>
<Enables you to teleport to a selected Commission Center (without spending dil)>
<10% discount on Instance Dungeon admission fees (Only for charged dungeons)>\\
}

code 50 # Hunting Master Package
{
<center>Would you like to use a Hunting Master Package?
<center><The package is not subject to refund or return after it is used.>
}

code 51
{
Hunting Master: Free Warp Option

}

code 52
{
Hunting Master: Selective Return Point Option

}

code 53
{
<center>��ͧ�����  Alliance of Glory �������?
}

code 54 # Growth Blessing (Hunting Master 2)
{
<center>Do you want to use Growth Blessing?
<center><This item cannot be returned or refunded once used.>
}

code 55
{
<Adds 20% EXP when used>
<Adds 10% EXP and can be used in conjunction with other EXP bonus items>
<Enables you to teleport to a selected Field (without spending dil)>
<Enables you to teleport to a selected Instance Dungeon (without spending dil)>
<Enables you to teleport to a selected Commission Center (without spending dil)>
<20% discount on Instance Dungeon admission fees (Only for charged dungeons)>\\
}

</cate>

<cate> 171 #Cash Shop UI

code 1
{
Cash
}

code 2
{
number
}

code 3
{
price
}

code 4
{
price
}

code 5
{
Send gifts
}

code 6
{
a sample
}

code 7
{
Description
}

code 8
{
name
}

code 9
{
Shop items
}

code 10 #Dekaron Shop Tab (Dekaron Shop Home)
{
Home
}

code 11 
{
Upgrade items
}

code 12
{
Refresh bottle
}

code 13
{
Character development
}

code 14
{
Expand bag / storehouse
}

code 15
{
Pets / Vehicles
}

code 16
{
Skin series
}

code 17
{
Guild effect / partner
}

code 18
{
Fancy wings
}

code 19
{
General products
}

code 20
{
recommended product
}

code 21
{
How to add cash
}

code 22
{
Cash available
}

code 23
{
balance
}

code 24
{
all
}

code 25
{
Confirm the purchase or not?
}

code 26
{
Recipient's name
}

code 27
{
gift
}

code 28
{
Confirmation of the purchase or not? Warning: The gift age will be counted immediately.
}

code 29
{
There is no name for this character in the system.
}

code 30
{
Password is not correct.
}

code 31
{
To confirm the owner of your ID, please enter your password.
}

code 32
{
Purchased successfully
}

code 33
{
Send a gift and receive a gift at Mailbox near people. Teleporter
}

code 34
{
Items will be destroyed as soon as the ground is left. Do you want to destroy this item?
}

code 35
{

<center><color=ff0000>����͹ : The usable period and Binding chance</color>

<center><color=ff0000>of the package items are determined</color>

<center><color=ff0000>randomly upon opening the package.</color>
}

code 36
{
To confirm your ID, please enter your password.
}

code 37
{
<color = FFCC00> Cash What is? </color>
Ie money to buy items in stores, in-game items (Item Shop)
Will be derived from
1. Fill in the system with a True Money card.
2. From the activities of the server
3. Fill in the server with other ways, such as TrueWallet etc.


<color = FFCC00> How to fill in Cash </color>
Visit the website of the server. (http://ExtremeDekaron.com/)

}

code 38
{
<color = FFCC00> Cash What is? </color>
Ie money to buy items in stores, in-game items (Item Shop)
Will be derived from
1. Fill in the system with a True Money card.
2. From the activities of the server
3. Fill in the server with other ways, such as TrueWallet etc.


<color = FFCC00> How to fill in Cash </color>
Visit the website of the server. (http://ExtremeDekaron.com/)

}

code 39
{
<color = FFCC00> Cash What is? </color>
Ie money to buy items in stores, in-game items (Item Shop)
Will be derived from
1. Fill in the system with a True Money card.
2. From the activities of the server
3. Fill in the server with other ways, such as TrueWallet etc.


<color = FFCC00> How to fill in Cash </color>
Visit the website of the server. (http://ExtremeDekaron.com/)

}

code 40
{
Failed to verify the Cash at the DB server.
}

code 41
{
Failed to verify the Cash at the billing server.
}

code 42
{
Dekaron Shop is not available for use on the test server.
}

code 43
{
The gift has been sent to the recipient entered above.\Gifts are sent to the Cash Inventory.
}

</cate>

<cate> 172 #Acquired experience increased

code 20
{
Veteran's Amulet\\Effect: +20% EXP Reward
}

code 25
{
Veteran's Amulet\\Effect: +25% EXP Reward
}

code 50
{
Veteran's Talisman\\Effect: +50% EXP Reward
}

code 70
{
Chaos Blessing\\Effect: +70% EXP Reward
}

code 100
{
Veteran��s Lucky Charm\\Effect: +100% EXP Reward
}

code 150
{

}

code 200
{
Veteran's Periapt\\Effect: +200% EXP Reward
}

code 300
{
Veteran's Periapt\\Effect: +300% EXP Reward
}

code 500
{
Veteran's Periapt\\Effect: +500% EXP Reward
}

</cate>