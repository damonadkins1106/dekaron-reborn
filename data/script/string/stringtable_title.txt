<cate> 100

code 1 
{
Error
}

code 2
{
Results
}

code 3
{
Unknown Map
}

code 4 #Skill Master UI
{
Learn Skill
}

code 5 #Skill Up UI
{
Skill Level Up
}

code 6 #Party Name Change UI
{
Change Party Name
}

code 7 #Form Party UI
{
Form Party
}

code 8 #Party Info UI
{
Party
}

code 9 #Stash UI
{
Stash
}

code 10 #Socket Refining UI
{
Refine Socket
}

code 11 #Create/Remove Option UI
{
Create/Remove Option
}

code 12 #Option Upgrade UI
{
Reinforce Item
}

code 13 #Remove Socket Stone UI
{
Remove Socket Stone
}

code 14 #Party Invitation UI
{
Invite to Party
}

code 15 #Random Box UI
{
Select Item
}

code 16 #Change Name Change UI
{
Change Character Name
}

code 17 #Character Name Confirmation UI
{
New Character Name
}

code 18 #Guild of Glory UI
{
Guild of Glory
}

code 19 #Guild Master's Mistake UI
{
Guild Master's Mistake
}

code 20 #Wealth Merchant's Stall UI
{
Wealthy Merchant's Stall
}

code 21 #Traveler's Backpack UI
{
Traveler's Backpack
}

code 22 #Storage Keeper's Key UI
{
Storage Keeper's Key
}

code 23 #Storage Mastery UI
{
Storage Mastery
}

code 24 #Discard Item UI
{
Discard Item
}

code 25 #Vitality Regenerator UI
{
Vitality Regenerator
}

code 26 #Not Enough Deka UI
{
You do not have enough Deka.
}

code 27 #Not Enough Inventory Space UI
{
You do not have enough Inventory space.
}

code 28 #Master's Mistake UI
{
Master's Mistake
}

code 29 #Portal Scroll UI
{
Portal Scroll
}

code 30 #Portal UI
{
Portal
}

code 31 #Strength Regenerator
{
Strength Regenerator
}

code 32 #Agility Regenerator
{
Agility Regenerator
}

code 33 #Spirit Regenerator
{
Spirit Regenerator
}

code 34 #Personal Shop UI
{
Personal Shop
}

code 35 #Purchase Info UI
{
Purchase Information
}

code 36 #Character Transfer Authorization UI
{
Character Account Transfer Authorization Information
}

code 37 #Character Transfer UI
{
Character Account Transfer
}

code 38 #Character Transfer Request UI
{
Request Character Account Transfer
}

code 39 #Sales UI
{
Sales Information
}

code 40 #Sale UI
{
Sale
}

code 41 #Wait to Trade UI
{
Wait to Trade
}

code 42 #Request Trade UI
{
Request Trade
}

code 43 #Edit Guild Notice UI
{
Edit Notice
}

code 44 #Write GM Tool Notice UI
{
Write Notice
}

code 45 #Change Guild Name UI
{
Change Guild Name
}

code 46 #Change Guildsman Rank UI
{
Change Rank
}

code 47 #Donate Adventure Points to Guild UI
{
Donate Adventure Points
}

code 48 #Set Up Guildsman Permissions UI
{
Set Up Permissions
}

code 49 # Guild UI
{
Guild
}

code 50 # Guild Level UI
{
Guild Level
}

code 51 # Confirm Guild Mark UI
{
Confirm Guild Mark
}

code 52 # Guild Mark UI
{
Guild Mark
}

code 53 # Guildsman Info UI
{
Guildsman Information
}

code 54 # Guild Name UI
{
Guild Name
}

code 55 #Confirm New Guild Name UI
{
Confirm New Guild Name
}

code 56 #Disband Guild UI
{
Disband Guild
}

code 57 #Expel Guildsman UI
{
Expel Guildsman
}

code 58 #Leave Guild UI
{
Leave Guild
}

code 59 #Send to All UI
{
Send to All
}

code 60 #Add Friend UI
{
Add Friend
}

code 61 # Mailbox UI
{
Mailbox
}

code 62	#Read Mailbox UI
{
Read Mail
}

code 63 #Delete Character UI
{
Delete Character
}

code 64 #Quest Alert Window UI
{
Quest Alert
}

code 65 #Quest UI
{
Quest
}

code 66 #Wait for Party Match Acceptance UI
{
Wait for Acceptance
}

code 67 #Request Party Match UI
{
Request Party Match
}

code 68 #Cancel Party Match UI
{
Cancel Match
}

code 69 #Party Match Settings UI
{
OK
}

code 70 #Wait for Party Match Acceptance UI
{
Wait for Acceptance
}

code 71 #Request Party Match UI
{
Request Match
}

code 72 #Invite to Guild UI
{
Invite to Guild
}

code 73 #Siege Time Notice UI
{
Check Declared Siege Time
}

code 74 #Declare Siege Time UI
{
Declare Siege Time
}

code 75 #Siege Status UI
{
Siege Status
}

code 76 #Confirm Siege Castle Gate Reinforcement UI
{
Confirm Castle Gate Reinforcement
}

code 77 #Siege Castle Gate Status UI
{
Castle Gate Status
}

code 78 #Reinforce Castle Gate UI
{
Reinforce Castle Gate
}

code 79 #Hatch UI
{
Hatch
}

code 80 #Pet Name UI
{
Name Your Pet
}

code 81 #Pet Inventory UI
{
Pet
}

code 82 #Awakening Ocarina UI
{
Awakening Ocarina
}

code 83 #Ocarina of Eternity
{
Ocarina of Eternity
}

code 84 #Character Transfer Information UI
{
Character Transfer Information
}

code 85 #Transfer Character UI
{
Transfer Character
}

code 86 #Cash Shop Package Info UI
{
Details
}

code 87 #Cash Refill UI
{
How to Refill Cash
}

code 88 #Buy Cash Item UI
{
Buy Item
}

code 89 #Confirm Cash Item Purchase UI
{
Confirm Your Purchase
}

code 90 #Confirm Cash Item Gift UI
{
Confirm Your Gift
}

code 91 #Confirm Character UI
{
Confirm Character Name
}

code 92 #Confirm Password UI
{
Confirm Password
}

code 93 #Cash Item Gift UI
{
Send as a Gift
}

code 94 #Cash Item Purchase Caution UI
{
Caution
}

code 95 #Open Cash Item Package UI
{
Open Package
}

code 96 #Skill Reset UI
{
Reset Skills
}

code 97 #Stat Reset UI
{
Reset Stats
}

code 98 #Leave Expedition
{
Disband Raid
}

code 99 #Look for Party
{
Party Search
}

code 100 #Recruit Party Member
{
Recruit Party Member
}

code 101 #Copy Character UI
{
Copy Character
}

code 102
{
Hunting Master Package
}

code 103 # Change Alliance Name UI
{
Name Alliance
}

code 104 # Alliance Mark UI
{
Alliance Mark
}

code 105 # Confirm Alliance Mark UI
{
Confirm Alliance Mark
}

code 106 #Alliance of Glory UI
{
Alliance of Glory
}

code 107 #Invite to Alliance UI
{
Invite to Alliance
}

code 108 #Delegate Alliance Commander UI
{
Delegate Alliance Commander
}

code 109 #Expel Alliance Member UI
{
Expel Alliance Member
}

code 110 #Leave Alliance UI
{
Leave Alliance
}

code 111
{
Warning
}

code 112
{
Growth Blessing
}

code 113 # Quest details UI
{
Quest Details
}

code 114 # Quest
{
[General]
}

code 115 # Quest
{
[Request]
}

code 116 # Quest
{
[Daily]
}

code 117 # Quest Alert UI
{
Quest Progress Window
}

code 118 # Colosseum UI
{
Colosseum
}

code 121 # mapwnd
{
Transparency
}

code 122 #Movemaptooltip
{
<color=FFD700>Remember:</color> To Vote every 12 Hours for DSHOP Coins!!!
}


</cate>

