<cate> 2200

code 0
{
<font=4><color=0xFFF4BE3D>View Stats</color></font>
}

code 1
{
     Character Info
}

code 2
{
     Skill Info
}

code 3
{
     Inven Info
}

code 4
{
     Quest Info
}

code 5
{
     Party Info
}

code 6
{
     Exped Info
}

code 7
{
     Guild Info
}

code 8
{
     MSG Info
}

code 9
{
     Pet Info
}

code 10
{
     Indv. Shop
}

code 11
{
     Map
}

code 12
{
     Help
}

code 13
{
     Contents Guide
}

code 14
{
<font=4><color=0xFFF4BE3D>Character Main Action</color></font>
}

code 15
{
     Move to left
}

code 16
{
     Move to right
}

code 17
{
     Move forward
}

code 18
{
     Move backward
}

code 19
{
     Walk/run
}

code 20
{
     Summon transportation
}

code 21
{
     Summon pet
}

code 22
{
     Cast fishing rod
}

code 23
{
<font=4><color=0xFFF4BE3D>Character Sub Action</color></font>
}

code 24
{
     Auto-attack
}

code 25
{
     Forcibly attack
}

code 26
{
     Attack while standing on the current spot
}

code 27
{
     Self-targeting
}

code 28
{
     Target next
}

code 29
{
     Converse with a nearby NPC
}

code 30
{
     Pick up items
}

code 31
{
     Switch weapons
}

code 32
{
     Follow
}

code 33
{
     Switch Equipment
}

code 34
{
<font=4><color=0xFFF4BE3D>Command Summon Creature</color></font>
}

code 35
{
     Attack
}

code 36
{
     Let loose
}

code 37
{
     Call
}

code 38
{
     Stop
}

code 39
{
     Retreat
}

code 40
{
     Creature skill
}

code 41
{
<font=4><color=0xFFF4BE3D>Skill Slot Window</color></font>
}

code 42
{
     Select Skill Slot 1
}

code 43
{
     Select Skill Slot 2
}

code 44
{
     Select Skill Slot 3
}

code 45
{
     Select Skill Slot 4
}

code 46
{
     Select Skill Slot 5
}

code 47
{
     Select Skill Slot 6
}

code 48
{
     Select Skill Slot 7
}

code 49
{
     Select Skill Slot 8
}

code 50
{
     Select Skill Slot 9
}

code 51
{
     Select Skill Slot 10
}

code 52
{
     Select Skill Slot 11
}

code 53
{
     Select Skill Slot 12
}

code 54
{
     Switch skill slot sets (Lower)
}

code 55
{
     Switch skill slot sets (Upper)
}

code 56
{
<font=4><color=0xFFF4BE3D>Potion Slot Window</color></font>
}

code 57
{
     Select Potion Slot 1
}

code 58
{
     Select Potion Slot 2
}

code 59
{
     Select Potion Slot 3
}

code 60
{
     Select Potion Slot 4
}

code 61
{
     Select Potion Slot 5
}

code 62
{
     Select Potion Slot 6
}

code 63
{
     Select Potion Slot 7
}

code 64
{
     Select Potion Slot 8
}

code 65
{
     Select Potion Slot 9
}

code 66
{
     Select Potion Slot 10
}

code 67
{
<font=4><color=0xFFF4BE3D>Screen Display</color></font>
}

code 68
{
     Show character names
}

code 69
{
     Show dropped item names
}

code 70
{
     Move camera (Down)
}

code 71
{
     Move camera (Up)
}

code 72
{
     Move camera (Right)
}

code 73
{
     Move camera (Left)
}

code 74
{
     Zoom in
}

code 75
{
     Zoom out
}

code 76
{
<font=4><color=0xFFF4BE3D>Misc</color></font>
}

code 77
{
     Screenshot
}

code 78
{
     Control Type A
}

code 79
{
     Control Type B
}

code 80
{
     Show/hide the Chat window
}

code 100
{
Back to Default
}

code 101
{
Assigned <color=00ff00><keyname> </color>key.
}

code 102
{
Cannot assign <color=00ff00><keyname> </color>key.
}

code 103
{
<color=ff0000>There is an action without an assigned key.</color>
<color=ff0000>Please assign a key.</color>
}

code 104
{
<color=ffff00><lblname></color> Cancel assigned key (Overlapped keys)
}

code 105
{
<color=ff0000>That key cannot be used.</color>
}

code 106
{
<color=00ff00><keyname> </color> key is already assigned to another
action.
}

code 107
{
<color=00ff00><keyname> </color> key can be assigned.
}

code 108
{
<color=00ff00>Returned all control keys to the default settings.</color>
}

</cate>

<cate> 2201
code 0
{
CTRL
}

code 1
{
SHIFT
}

code 2
{
ALT
}

code 3
{
TAB
}

code 4
{
SPACE
}

code 5
{
BACKSPACE
}

code 6
{
��
}

code 7
{
��
}

code 8
{
��
}

code 9
{
��
}

code 10
{
F1
}

code 11
{
F2
}

code 12
{
F3
}

code 13
{
F4
}

code 14
{
F5
}

code 15
{
F6
}

code 16
{
F7
}

code 17
{
F8
}

code 18
{
F9
}

code 19
{
F10
}

code 20
{
F11
}

code 21
{
F12
}

code 22
{
ENTER
}

code 23
{
PAGEUP
}

code 24
{
PAGEDN
}

code 25
{
HOME
}

code 26
{
END
}

code 27
{
INSERT
}

code 28
{
DELETE
}

code 29
{
;
}

code 30
{
=
}

code 31
{
,
}

code 32
{
-
}

code 33
{
.
}

code 34
{
/
}

code 35
{
`
}

code 36
{
��
}

code 37
{
NUM0
}

code 38
{
NUM1
}

code 39
{
NUM2
}

code 40
{
NUM3
}

code 41
{
NUM4
}

code 42
{
NUM5
}

code 43
{
NUM6
}

code 44
{
NUM7
}

code 45
{
NUM8
}

code 46
{
NUM9
}

code 47
{
NUM LOCK
}

code 48
{
None
}

code 49
{
CAPS LOCK
}

code 50
{
*
}

code 51
{
-
}

code 52
{
+
}

code 53
{
PRINT SCREEN
}

code 54
{
SCROLL LOCK
}

code 55
{
PAUSE BREAK
}

code 56
{
[
}

code 57
{
]
}

code 58
{
'
}

code 59
{
Num Period
}

</cate>

