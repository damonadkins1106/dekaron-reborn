<cate> 2000
code 0
{
Stats
}

code 1
{
Equip
}

code 2
{
Costume
}

code 3
{
Accept
}

code 4
{
Decline
}

code 5
{
Yes
}

code 6
{
No
}

code 7
{
Edit
}

code 8
{
Discard
}

code 9
{
Approve
}

code 10
{
Option
}

code 11
{
Character
}

code 12
{
Log Out
}

code 13
{
Exit Game
}

code 14
{
Cancel
}

code 15
{
Ok
}

code 16
{
Preview
}

code 17
{
Request Change
}

code 18
{
Cancel Change
}

code 19
{
Notice
}

code 20
{
Death
}

code 21
{
Move to saved location.
}

code 22
{
Confirm
}

code 23
{
Cancel
}

code 24
{
Accept
}

code 25
{
Decline
}

code 26
{
Abandon
}

code 27
{
Buy
}

code 28
{
Continue
}

code 29
{
Exit
}

code 30
{
Find NPC
}

code 31
{
Virtual Keyboard On/Off
}

code 32
{
<center>Switch Equipment
<center>(Ctrl + Tab)
}

code 33
{
<center>The Switch Equipment command
<center>can only switch your current equipment
<center>with equipment in the Secondary tab.
}

code 34
{
Please add a secondary equipment item.
}

code 35
{
Resurrect
}

code 36
{
<center>Use the Redemption Seed item 
<center>to resurrect yourself.
}

code 37
{
Abort
}

code 38
{
Select Warp
}

code 39
{
Return Point
}

code 40
{
Search
}

code 41
{
Party
}

code 42
{
1:1 Chat
}

code 43
{
Select Channel
}

code 44
{
Cancel
}

code 45
{
Enter
}

code 46
{
Exit (%d sec.)
}

code 55
{
DK
}

code 55
{
NPCs
}

code 64
{
NPCs
}

</cate>

