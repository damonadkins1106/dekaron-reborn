<cate> 2005
code 1
{
You do not currently have a mount.
}

code 2
{
You may not open an individual shop while
mounted.
}

code 3
{
Cannot request a duel while mounted.
}

code 4
{
Action not permitted while mounted.
}

code 5
{
You cannot move there while mounted.
}

code 6
{
You cannot use the mount in this area.
}

code 7
{
Waiting to use the mount.
}

code 8
{
Waiting to dismount.
}

code 11
{
You do not have a mount.
}

code 12
{
You cannot use a mount in this area.
}

code 13
{
You are already mounted.
}

code 14
{
Failed to mount the creature.
}

code 15
{
Failed to dismount.
}

code 16
{
Mount (J)
}

code 17
{
You may not open an individual shop while
mounted.
}

code 18
{
You cannot request a duel while mounted.
}

code 19
{
You cannot trade during the wait time.
}

code 20
{
You cannot use the Cash Shop while mounted.
}

code 21
{
You cannot use the shop while mounted.
}

code 22
{
You cannot get mount while moving.
}

code 23
{
You cannot use skills while mounted.
}

code 24 #낚시중 이동수단 사용할 경우
{
You cannot use your mount while fishing.
}

code 25 #이동수단 이용중 낚시를 시도할 경우
{
You may not fish while riding a mount.
}

code 26 #펫 해제시 이동수단을 이용하려할 경우
{
You may not summon or unsummon a
mount while doing other activities.
}

code 27
{
You cannot summon any transportation during 
a transformation.
}

code 30 #길찾기 성공
{
You will be moved to the selected location.
}

code 31 #길찾기 실패 : 버퍼 오버
{
You cannot be moved to the selected location.
}

code 32 #길찾기 실패: 이동할 수 없는 지역
{
You may not move to the selected location.
}

code 33
{
You cannot move; the distance is too great.
}

code 34 #길찾기 실패: 시간 오버
{
You failed to move because the look up
time has expired<font=6>.</font>
}

code 35 #이동수단 탑승시 파티대결 신청을 할 수 없음
{
You cannot request for a party match while using
a transport.
}

code 36
{
You cannot summon your mount while using 
a skill.
}

code 62001
{
A racehorse from Foreign Realm that increases
the character's movement speed by 25%
<color=f000ff>Usage Time: 01 Day</color>
<color=ffcc00>Option Socket Item</color> :
Equip in the option socket window
<color=ff0000>Warning :</color> There can be areas where theMount
cannot be used, and it cannot be traded since
this item only belongs to buyer.
}

code 62002
{
Mount item [Lymparks]
<color=f000ff>Usage Time: 00 Day</color>
<color=ffcc00>Option Socket Item</color> :
Equip in the option socket window.
<color=ff0000>Warning :</color> There can be areas where theMount
cannot be used, and it cannot be traded since
this item only belongs to buyer.
}

code 62003
{
A beautiful fox that increases the character's
movement speed by 25%
<color=f000ff>Usage Time: 01 Day</color>
<color=ffcc00>Option Socket Item</color> :
Equip in the option socket window.
<color=ff0000>Warning :</color>
There can be areas where theMount cannot be
used, and it cannot be traded since this item only
belongs to buyer.
}

code 62004
{
Mount Item [Alludra]
<color=f000ff>Usage Time: 00 Day</color>
<color=ffcc00>Option Socket Item</color> :
Equip in the option socket window.
<color=ff0000>Warning :</color>
There can be areas where theMount cannot be
used, and it cannot be traded since this item only 
belongs to buyer.
}

code 62005
{
A trained beast that increases the character's
movement speed by 25%
<color=f000ff>Usage Time: 01 Day</color>
<color=ffcc00>Option Socket Item</color> :
Equip in the option socket window.
<color=ff0000>Warning :</color> There can be areas where theMount
cannot be used, and it cannot be traded since
this item only belongs to buyer.
}

code 62006
{
Mount Item [Bellum]
<color=f000ff>Usage Time: 00 Day</color>
<color=ffcc00>Option Socket Item</color> :
Equip in the option socket window.
<color=ff0000>Warning :</color>
There can be areas where theMount cannot be
used, and it cannot be traded since this item only
belongs to buyer.
}

code 62007
{
A racehorse from Foreign Realm that increases
the character's movement speed by 25%
<color=f000ff>Usage Time: 15 Days</color>
<color=ffcc00>Option Socket Item</color> :
Equip in the option socket window.
<color=ff0000>Warning :</color>
There can be areas where theMount cannot be
used, and it cannot be traded since this item
only belongs to buyer.
}

code 62008
{
A beautiful fox that increases the character's
movement speed by 25%
<color=f000ff>Usage Time: 15 Days</color>
<color=ffcc00>Option Socket Item</color> :
Equip in the option socket window.
<color=ff0000>Warning :</color>
There can be areas where theMount cannot be
used, and it cannot be traded since this item
only belongs to buyer.
}

code 62009
{
A trained beast that increases the character's
movement speed by 25%
<color=f000ff>Usage Time: 15 Days</color>
<color=ffcc00>Option Socket Item</color> :
Equip in the option socket window.
<color=ff0000>Warning :</color>
There can be areas where theMount cannot be
used, and it cannot be traded since this item
only belongs to buyer.
}

code 62010
{
A racehorse from Foreign Realm that increases
the character's movement speed by 25%
<color=f000ff>Usage Time: 30 Days</color>
<color=ffcc00>Option Socket Item</color> :
Equip in the option socket window.
<color=ff0000>Warning :</color>
There can be areas where theMount cannot be
used, and it cannot be traded since this item
only belongs to buyer.
}

code 62011
{
A beautiful fox that increases the character's
movement speed by 25%
<color=f000ff>Usage Time: 30 Days</color>
<color=ffcc00>Option Socket Item</color> :
Equip in the option socket window.
<color=ff0000>Warning :</color>
There can be areas where theMount cannot be
used, and it cannot be traded since this item
only belongs to buyer.
}

code 62012
{
A trained beast that increases the character's
movement speed by 25
<color=f000ff>Usage Time: 30 Days</color>
<color=ffcc00>Option Socket Item</color> :
Equip in the option socket window.
<color=ff0000>Warning :</color>
There can be areas where theMount cannot be
used, and it cannot be traded since this item
only belongs to buyer.
}

code 62013 #아퀴라 라티스
{
A beautiful creature that will increase the
character's movement speed by 30%.
<color=f000ff>Usage Time: 30 Days</color>
<color=ffcc00>Option Socket Item</color> :
Equip it in the option socket window.
<color=ff0000>Warning :</color>
There are areas where mounts cannot be used.
And since this item only belongs to the owner, it
cannot be traded.
}


code 62019
{
This otherworldly racing horse increases your Movement Speed by 25%.\\<color=F000FF>Period: 15 Days</color>\\<color=FFCC00>Option Socket Item</color>: Equip in the Option Socket window.\\<color=ff000>Caution:</color> Transportation items may not be available for use in certain maps,\and they cannot be traded like other Binding items.
}

code 62020
{
This beautiful fox increases your Movement Speed by 25%.\\<color=f000ff>Period: 15 Days</color>\\<color=FFCC00>Option Socket Item</color>: Equip in the Option Socket window.\\<color=ff000>Caution:</color> Transportation items may not be available for use in certain maps,\and they cannot be traded like other Binding items.
}

code 62021
{
This tamed beast increases your Movement Speed by 25%.\\<color=f000ff>Period: 15 Days</color>\\<color=FFCC00>Option Socket Item</color>: Equip in the Option Socket window.\\<color=ff000>Caution:</color> Transportation items may not be available for use in certain maps,\and they cannot be traded like other Binding items.
}

code 62537
{
This brave tiger increases your Movement Speed by 32%.\\<color=f000ff>Period: Permanent</color>\\<color=FFCC00>Option Socket Item</color>: Equip in the Option Socket window.\\<color=ff000>Caution:</color> Transportation items may not be available for use in certain maps,\and they cannot be traded like other Binding items.
}

code 62675
{
This brave tiger increases your Movement Speed by 32%.\\<color=f000ff>Period: 30 Days</color>\\<color=F000FF>Period: Permanent</color>\\<color=FFCC00>Option Socket Item</color>: Equip in the Option Socket window.\\<color=ff000>Caution:</color> Transportation items may not be available for use in certain maps,\and they cannot be traded like other Binding items.
}

</cate>

