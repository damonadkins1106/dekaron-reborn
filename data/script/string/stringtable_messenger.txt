<cate> 1006
code 1
{
has made a friend request. 
Would you like to accept it?
}

code 2
{
Register Friend
}

code 3
{
Edit Group
}

code 4
{
Delete Group
}

code 5
{
Talk
}

code 6
{
Send Message
}

code 7
{
Move Group
}

code 8
{
Delete Friend
}

code 9
{
Messenger
}

code 10
{
Save
}

code 11
{
Setting
}

code 21
{
Cannot deliver to messenger server.
}

code 22
{
Cannot find selected user in friend list.
}

code 23
{
You have sent the friend request to .
}

code 24
{
Cannot find selected user.
}

code 25
{
Already registered in friend list.
}

code 26
{
Reached the max number of friend list.
}

code 27
{
The user's friend list reached the max number.
}

code 28
{
Server error.
}

code 29
{
Friend request has been declined.
}

code 30
{
User does not exist.
}

code 31
{
Failed registering friend.
}

code 32
{
The user's friend list reached the max number.
}

code 33
{
Failed registering friend.
}

code 34
{
Deleted from friend list.
}

code 35
{
Not a registered friend.
}

code 36
{
Messenger chat server error.
}

code 37
{
Added to friend list.
}

code 38
{
Friend request has been cancelled because the user is busy.
}

code 39
{
Friend request has been cancelled because the user does not accept friend requests.
}

code 40
{
Messenger server has ended.
}

code 41
{
has exited the game.
}

code 42
{
does not accept any requests.
}

code 43
{
<VALUE> Messages
}

code 44
{
<VALUE> Notes
}

code 45
{
has connected to the game.
}

code 46
{
Messenger server has ended.
}

code 47
{
Decline messages
}

code 48
{
is not connected to the game, therefore cannot send the message.
}

code 49
{
Friend
}

code 50
{
Blacklist
}

code 51
{
Add
}

code 52
{
Add Friend
}

code 53
{
Add to Blacklist
}

code 54
{
is added to the blacklist.
}

code 55
{
Already in the blacklist.
}

code 56
{
My blacklist is already full.
}

code 57
{
is removed from the blacklist.
}

code 58
{
is not registered in the block list.
}

code 59
{
Cannot register yourself.
}

code 60
{
Party
}


code 61
{
Battle Server
}

code 62
{
You cannot add friends while in this map.
}

code 63
{
You cannot block characters while in this map.
}
</cate>

