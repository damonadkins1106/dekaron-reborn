<cate> 190

code 1
{
Amount
}

code 2
{
I
}

code 3
{
II
}

code 4
{
III
}

code 5
{
IV
}

code 6
{
Shared
}

code 7
{
You cannot store items in the selected location.
}

code 8
{
You do not have enough dil to pay the storage fee.
}

code 9
{
Failed to pay the storage fee.
}

code 10
{
Failed to withdraw dil.
}

code 11
{
Your dil will exceed the maximum 1,000,000,000 dil; cannot withdraw dil from the Stash.
}

code 12
{
Failed to deposit dil.
}

code 13
{
You do not have enough dil to deposit.
}

code 14
{
This amount of dil exceeds the maximum deposit limit.
}

code 15
{
Stash full. Make sure you have less than 136 items in stash.
}

code 16
{
Your Shared Stash is full; cannot register the item.
}

code 17
{
Cannot register items. You may have reached the limit.
}

code 18
{
Withdraw
}

</cate>
