<cate> 140

code 1
{
Alliance
}

code 2
{
Name
}

code 3
{
Commander
}

code 4
{
No.
}

code 5
{
Guild
}

code 6
{
Guild        ��
}

code 7
{
Guild        ��
}

code 8
{
Guild Master
}

code 9
{
Guild Master      ��
}

code 10
{
Guild Master      ��
}

code 11
{
Guildsman
}

code 12
{
The <color=0xff00ff00>%s</color> Guild
wants to <color=0xffff0000>form an Alliance</color> with your guild.
}

code 13
{
[Message] Guild <color=0xff00ff00>%s</color> has declined your Alliance invitation.
}

code 14 #Cancelled or disconnected while inviting or creating an Alliance
{
The Alliance creation/invitation has been cancelled or the target is offline.
}

code 15
{
[Message] You have joined an Alliance.
}

code 16
{
You have been invited to the <color=0xff00ff00>%s</color> Alliance. 
}

code 17
{
Delegate Alliance Commander
}

code 18
{
Expel Guild
}

code 19
{
Edit Notice
}

code 20 #Creation
{
You have invited the <color=0xff00ff00>%s</color> Guild. 
}

code 21 #Additional invite
{
You have invited the <color=0xff00ff00>%s</color> Guild. 
}

code 22
{
Only Alliance Commanders can invite guilds.
}

code 23
{
%s has become the new Alliance Commander!
}

code 24
{
You have not satisfied the condition for inviting guilds to your Alliance.
}

code 25
{
You cannot invite guilds while in this area.
}

code 26 #Alliance requester cancelled the formation
{
The Alliance formation has been cancelled.
}

code 27 #Alliance requester cancelled additional invite
{
The Alliance invitation has been cancelled.
}

code 28
{
You have agreed to create/join the Alliance.
}

code 29
{
You have declined to create/join the Alliance.
}

code 30
{
Your guild has left the Alliance.
}

code 31
{
The Alliance has been disorganized.
}

code 32
{
Your guild has been expelled from the Alliance.
}

code 33
{
Guild %s has been expelled from the Alliance.
}

code 34
{
Guild %s has joined the Alliance.
}

code 35
{
Guild %s has left the Alliance.
}

code 41
{
The guild has declined to form the Alliance.
}

code 42
{
The Alliance requester is not in a guild.
}

code 43
{
The Alliance requester's guild has not yet reached Level 2.
}

code 44
{
The Alliance requester does not have enough dil.
}

code 45
{
The Alliance requester already invited you or his guild is currently being invited to another Alliance.
}

code 46
{
The Alliance requester is in your guild.
}

code 47
{
The Alliance requester is already in an Alliance.
}

code 48
{
The Alliance requester is not a guild master.
}

code 49
{
You already invited the guild or the guild is currently being invited to another Alliance.
}

code 50
{
No guilds are available to invite.
}

code 51
{
The guild has not yet reached Level 2.
}

code 52
{
The guild is already in an Alliance.
}

code 53
{
The guild's master is not online.
}

code 54
{
Failed to form the Alliance.
}

code 55
{
Do you want to delete the Alliance of Glory?
}

code 56
{
You have cancelled deleting the Alliance of Glory.
}

code 57
{
You have deleted the Alliance of Glory.
}

code 58
{
The Alliance has been formed.\%d dil was paid to form the Alliance.
}

code 59 #Formation failed
{
This Alliance name already exists.
}

code 60
{
This Alliance name is not allowed.
}

code 61
{
The Alliance formation has been cancelled.
}

code 62
{
The requester guild does not exist.
}

code 63
{
The requester does not exist.
}

code 64
{
The requestee guild does not exist.
}

code 65
{
The requestee does not exist.
}

code 66
{
The name of the requestee guild does not match the target guild.
}

code 68
{
You have changed your Alliance Mark. %d dil was paid for this change.
}

code 69 #Failed to change Alliance Mark
{
There is not enough dil.
}

code 70
{
You are not a member of the Alliance.
}

code 71
{
You do not have permission to change the Alliance Mark.
}

code 72
{
Failed to change the Alliance Mark.
}

code 73 
{
You are not in the Alliance.
}

code 74
{
You do not have the Delete permission.
}

code 75
{
You do not have an Alliance Mark effect item.
}

code 76
{
Failed to delete the Alliance Mark effect item.
}

code 77 #Delegate Alliance Commander
{
You have delegated your Alliance Commander authority.
}

code 78
{
Cannot delegate the Alliance Commander authority.
}

code 79
{
You do not have permission to delegate the Alliance Commander authority.
}

code 80
{
The target's name is identical to the current Alliance Commander��s.
}

code 81
{
The target is not an executive of the Alliance or is not online.
}

code 82
{
Failed to delegate the Alliance Commander authority.
}

code 83 #Change notice
{
The notice has been changed.
}

code 84 #Change notice--not Alliance
{
You are not allied; you cannot register the notice.
}

code 85
{
You do not have permission to change the notice.
}

code 86
{
A notice cannot be longer than 800 characters.
}

code 87
{
Failed to change the notice.
}

code 88 #Joining Alliance
{
The guild has joined the Alliance.
}

code 89
{
The guild has declined to join the Alliance.
}

code 90
{
The Alliance inviter is not in a guild.
}

code 91
{
Another guild is currently being invited to the Alliance.
}

code 92
{
The Alliance inviter is not the Alliance Commander.
}

code 93 #Alliance requester/invited guild name not matched
{
The requester names do not match; the guild could not be invited to the Alliance.
}

code 94
{
An Alliance can contain only up to 4 guilds.
}

code 95 #Guild is inviting/being invited
{
The guild is currently being formed or has been invited to a different Alliance.
}

code 96
{
No guilds are available to invite.
}

code 97
{
The invited guild has not yet reached Level 2.
}

code 98
{
The guild is already in an Alliance.
}

code 99
{
The guild's master is not online.
}

code 100
{
Failed to invite to the Alliance.
}

code 101 #Expel
{
No guilds are available to expel.
}

code 102
{
You are not a member of the Alliance.
}

code 103
{
You do not have the Expel permission.
}

code 104
{
Cannot expel the guild of the Alliance Commander.
}

code 105
{
Failed to expel.
}

code 106  #Leave
{
You have requested to leave the Alliance.
}

code 107
{
You are not in a guild.
}

code 108
{
You are not in the Alliance.
}

code 109
{
You do not have permission to leave the Alliance.
}

code 110
{
Alliance Commanders cannot leave until they delegate their commander authority to someone else.
}

code 111
{
Failed to leave the Alliance.
}

code 112 #Chat
{
Only Alliance Executives can use Alliance chat.
}

code 113
{
<center>To delegate your Alliance Commander authority,
please enter "<color=F4BE3D>%s</color>."
}

code 114
{
 must be entered to delegate your Alliance Commander Authority.
}

code 115 #Forming Alliance
{
The guild's master has not satisfied the condition to accept the Alliance invitation.
}

code 116 #Forming Alliance
{
The requester/requestee guild's master is in a map in which Alliance invitation is not available.
}

code 117 #Forming Alliance
{
You cannot invite guilds to your Alliance while in the current map.
}

code 118 #Expel Alliance Guild
{
Do you want to expel the <color=0xff00ff00>%s</color> guild? 
}

code 119 #Leave Alliance
{
Do you want to leave the Alliance?
}

code 120 #Cancel forming Alliance
{
The requester has cancelled forming the Alliance.
}

code 121 #Cancel forming Alliance
{
The Alliance formation has been cancelled because the requester went offline.
}

code 122 #Cancel forming Alliance
{
Cannot find the requestee guild; the Alliance formation has been cancelled.
}

code 123 #Cancel forming Alliance
{
Cannot find the requester guild; the Alliance formation has been cancelled.
}

code 124 #Cancel forming Alliance
{
Cannot find the requester guild master; the Alliance formation has been cancelled.
}

code 125 #Cancel joining Alliance
{
The requester has cancelled your admission to his Alliance.
}

code 126 #Cancel joining Alliance
{
The requester has gone offline; the Alliance admission has been cancelled.
}

code 127 #Cancel joining Alliance
{
Cannot find the invitee guild; the Alliance admission has been cancelled.
}

code 128 #Cancel joining Alliance
{
Cannot find the host Alliance; the Alliance admission has been cancelled.
}

code 129 #Cancel joining Alliance
{
Cannot find the host Alliance Commander; the Alliance admission has been cancelled.
}

code 130
{
Cannot find the target guild.
}

code 131
{
The Alliance notice has been changed.
}

code 132
{
The Alliance effect has been changed.
}

code 133
{
You do not have permission to change the Alliance effect.
}

code 134
{
No Alliance Mark is available.
}

code 135
{
Cannot find the item.
}

code 136
{
Failed to change the Alliance effect.
}

code 137
{
You currently have not satisfied the condition to form an Alliance.
}

code 138 #Invalid map for Alliance formation
{
You can only form an Alliance while in Braiken or Loa Castle.
}


</cate>

