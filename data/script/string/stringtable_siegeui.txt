<cate> 290
code 1
{
Confirm Siege Battle Announcement
}

code 2
{
Siege Battle will start.
}

code 3
{
Confirm
}

code 4
{
Confirm
}

code 5
{
Cancel
}

code 6
{
Announce Siege Battle Time
}

code 7
{
<MONTH> / <DAY> / <YEAR>  <TIME>
}

code 8
{
<MONTH> / <DAY> / <YEAR>
}

code 9
{
o'clock
}

code 10
{
<center>You can only enter
(12:00 ~ 21:00)
}

code 30
{
Number of guards available to station: [BLINK]
}

code 31
{
Number of guards currently stationed: [BLINK]
}

code 32
{
Number of guards available to fight: [BLINK]
}

code 40
{
Respawn Location Window
}

code 41
{
Will be respawned after <color=FFFFFF>(%02d)</color>seconds.
}

code 42
{
<color=F9B600>Completing response</color>[ %2d sec ]
}

code 50
{
<color=F9B600>Before the Siege Battle</color>
}

code 51
{
<color=F9B600>Battle Time :</color> %d min
}

code 52
{
<color=F9B600>Battle Time :</color> %d sec
}

code 53
{
<color=F9B600>Finish Siege Battle</color>
}

code 54
{
<color=F9B600>Extend Battle Time</color>
}

code 101
{
Battle Status
}

code 102
{
Siege Battle
}

code 103
{
Guild Battle
}

code 104
{
-Siege Battle Proceeding Time-
}

code 105
{
[Defending Guild]
}

code 106
{
[Offending Guild]
}

code 107
{
%2d day(s) left until the Siege Battle starts
}

code 108
{
%2d hour(s) left until the Siege Battle starts
}

code 109
{
%2d min(s) left until the Siege Battle starts
}

code 110
{
Start Siege Battle
}

code 111
{
The Siege Battle has not started yet.
}

code 112
{
The Siege Battle is in progress right now.
}

code 113
{
South Gate
}

code 114
{
West Gate
}

code 115
{
East Gate
}

code 116
{
The starting time for Siege Battle has not been announced yet.
}

code 120
{
<center><color=76F756>Request for Siege Participation</color>
}

code 121
{
<center><color=F9B600>Seal Ritual is Available</color>
}

code 122
{
<center><color=76F756>Seal Ritual Completed</color>
}

code 123
{
<center><color=76F756>Siege Participation Confirmed</color>
}

code 124
{
<center><color=FEFE00>Start Siege War</color>
}

code 125
{
<center><color=76F756>Time Period for Siege Request</color>
}

code 128
{
<center><color=FEFE00>Start Siege War</color>
}

</cate>

<cate> 320
code 1
{
MAX durability for gates
}

code 2
{
Current durability
}

code 3
{
Current DIL
}

code 4
{
Investment for
fortification
}

code 5
{
,000 DIL
}

code 6
{
<center>You can enter fortification 
<center>amount by 1,000 DIL
[1,000 DIL = 1 durability point]
}

code 7
{
Fortify Gate
}

code 20
{
Confirm Fortification
}

code 21
{
Total Expenses
}

code 22
{
Additional Durability Points
}

code 23
{
Total Durability
}

code 24
{
Would you like to start the fortification?
}

code 25
{
Fortify
}

code 26
{
Cancel
}

code 27
{
DIL
}

code 30
{
Current Status of Gates
}

code 31
{
Current Durability of the Gates
}

code 32
{
South Gate
}

code 33
{
West Gate
}

code 34
{
East Gate
}

code 35
{
/
}

code 40
{
[message]<NAME> started to respond with Juto.
}

code 41
{
You cannot attack an opponent that's in 
invincible status.
}

code 50
{
<STONENAME>
Guild in Possession: <GUILD>
}

code 101
{
<center><color=AFC4D4>[ZEOLITENAME]</color>
<color=F9B600>Guild in Possession : </color>[GUILDNAME]
}

code 102
{
<center><color=39E100>[ZEOLITENAME]</color>
<color=F9B600>Guild in Possession : </color>[GUILDNAME]
}

code 103
{
<center><color=718CF5>[ZEOLITENAME]</color>
<color=F9B600>Guild in Possession : </color>[GUILDNAME]
}

code 104
{
<center><color=8BBEEE>[ZEOLITENAME]</color>
<color=F9B600>Guild in Possession : </color>[GUILDNAME]
}

code 105
{
<center><color=F08086>[ZEOLITENAME]</color>
<color=F9B600>Guild in Possession : </color>[GUILDNAME]
}

</cate>

<cate> 2101

code 1
{
Siege Portal - Defense
}

code 2
{
Spirit Guard Stone
}

code 3
{
Earth Guard Stone
}

code 4
{
Water Guard Stone
}

code 5
{
Wind Guard Stone
}

code 6
{
Fire Guard Stone
}

code 7
{
Juto
}

code 8
{
South Gate
}

code 9
{
West Gate
}

code 10
{
East Gate
}

code 11
{
Siege Portal - Offense
}

code 12
{
Turn-based Observer Mode
}
</cate>
