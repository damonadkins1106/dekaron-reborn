<cate> 70
code 1
{
 has left the Party.
}

code 2
{
 has been expelled from the party.
}

code 3
{
 is now the Party Leader.
}

code 4
{
The Party has changed its name to %s.
}

code 5
{
[Party Invitation Failed] The target character cannot be found.
}

code 6
{
[Party Invitation Failed] You are not the Party Leader.
}

code 7
{
[Party Invitation Failed] The target character is moving to a different map.
}

code 8
{
[Party Invitation Failed] The target character is already in a Party.
}

code 9
{
 is performing a different activity; your invitation has been cancelled.
}

code 10
{
 has declined your Party invitation.
}

code 11
{
 has joined the Party.
}

code 12
{
Invite %s to your Party.
}

code 13
{
Party Member 
}

code 14
{
 [
}

code 15
{
The Party's item share type is set to 
}

code 16
{
[Default]
}

code 17
{
[In Turn]
}

code 18
{
[Free for All]
}

code 19
{
[Random]
}

code 20
{
.
}

code 21
{
The Inventories of all Party Members are full.
}

code 22
{
You cannot give this item to the Party Member.
}

code 23
{
You cannot pick up these items.
}

code 24
{
The Inventories of all Party Members are full.
}

code 25
{
You cannot pick up this dil.
}

code 26
{
Would you like to leave the Party?
}

code 27
{
Please enter a party chant.
}

code 28
{
Please enter a party member to delegate.
}

code 29
{
 has invited you to his Party.
}

code 30
{
Do you want to leave the party?
}

code 31
{
 has declined all Party invitations; your request has been cancelled.
}

code 32
{
Delegate
}

code 33
{
Form Party
}

code 34
{
Leave Party
}

code 35
{
Leave Party
}

code 36
{
 obtained.
}

code 37
{
No such Party Member exists for sharing items.
}

code 38
{
There are no Party Members with whom to share items.
}

code 39
{
Map Settings
}

code 40
{
Level Settings
}

code 41
{
[Party Invitation Failed] You cannot invite Guildsmen while in a Siege map.
}

code 42
{
[Party Invitation Failed] You cannot invite Party Members while in Instance Dungeons.
}

code 43
{
[Party Invitation Failed] The Party currently cannot receive new members.
}

code 44
{
[Party Invitation Failed] The Party is full.
}

code 45
{
[Party Invitation Failed] No such Party exists.
}

code 46
{
[Party Invitation Failed] Dead Front Parties cannot invite new members.
}

code 47
{
Dead Front Parties cannot expel existing members.
}

code 48
{
Members of Dead Front Parties cannot leave their Party.
}

code 49 #Pet Inventory Full
{
Your Pet's Inventory is full.
}

code 50 #Party Invitation to Battlegroup Member
{
[Party Invitation Failed] You cannot invite the Battlegroup Members.
}

code 51 #Unavailable Area for Party Formation
{
You cannot form a Party while in this area.
}

code 52 #Cannot Disorganize Party
{
You cannot disband your Party during a Party Match.
}

code 53 #Party Member Cannot Leave
{
 is in progress; you cannot leave your Party right now.
}

code 54 #Cannot Invite during Party Match
{
You cannot join a Party that is currently participating in a Party Match.
}

code 55 #Cannot Invite during Party Match
{
[Party Invitation Failed] You cannot invite new members during a Party Match.
}

code 56 #Party Invitation Failed
{
[Party Invitation Failed] Your Party invitation has failed.
}

code 57
{
This character is performing a different activity; your request has been cancelled.
}

code 58
{
You are not in a Party or Raid.
}

code 59
{
The Party has been formed.
}

code 60
{
[Party Formation Failed] Failed to create Party Members.
}

code 61
{
[Party Formation Failed] Failed to create the Party.
}

code 62
{
[Party Invitation Failed] You cannot form a Party while in Instance Dungeons.
}

code 63
{
[Party Invitation Failed] Raid members cannot form a Party.
}

code 64
{
[Party Invitation Failed] You cannot form a Party while in this map.
}

code 65
{
[Party Failed] You cannot invite members of the opposing team to your party.
}

code 66
{
Invite to Party
}

code 67
{
's Party
}

code 68
{
Your Inventory is full; you cannot receive any more items.
}

code 69
{
Your Pet's Inventory is full; it cannot receive any more items.
}

code 70
{
's Inventory is full, and you cannot give any more items.
}

code 71
{
The Pet's Inventory is full, and you cannot give any more items.
}

code 72
{
You are too far away from the rest of your Party, and cannot receive items.
}

code 73
{
 is too far away from the rest of your Party, and cannot be given items.
}

code 74
{
You are dead, and cannot receive items.
}

code 75
{
 is dead, and cannot be given items.
}

code 76
{
Search
}

code 77
{
Cancel
}

code 78
{
Recruit
}

code 79
{
Cancel
}

code 80
{
The target is not in your Party.
}

code 81
{
Invalid conditions.
}

code 82
{
You already started recruiting members.
}

code 83
{
Only Party Leaders can use the Recruit Party Member option.
}

code 84
{
You cannot use the Recruit Party Member option while in DK Square.
}

code 85
{
You cannot use the Recruit Party Member option while in dungeons.
}

code 86
{
Dead Front Parties cannot use the Recruit Party Member option.
}

code 87
{
Cannot connect to the Party server.
}

code 88
{
Your Party Member recruitment has initiated.
}

code 89
{
You stopped recruiting Party Members.
}

code 90
{
Job Settings
}

code 91
{
~ Lv
}

code 92
{
Lv
}

code 93
{
Invalid information.
}

code 94
{
You are already in a Party.
}

code 95
{
Cannot find the Party Leader.
}

code 96
{
This cannot be used during a Party Match.
}

code 97
{
The target does not exist.
}

code 98 #Requested to a non-leader
{
The character is not the Party Leader.
}

code 99
{
Cannot recruit characters from different channels during a Siege.
}

code 100
{
Characters must be in the same Siege map to join and be invited to a Party.
}

code 101
{
You are not the Party Leader.
}

code 102
{
You already started looking for a Party.
}

code 103
{
You cannot use the Look for Party option while in dungeons.
}

code 104
{
You started looking for a Party.
}

code 105
{
You cannot switch channels while in dungeons.
}

code 106
{
You cannot switch channels during PvP.
}

code 107
{
You cannot switch channels during a Party Match.
}

code 108
{
You cannot switch channels while participating in Dead Front.
}

code 109
{
Party Members that have applied and registered for Dead Front cannot switch channels.
}

code 110
{
You cannot switch channels during a Siege.
}

code 111
{
You cannot switch channels while in DK.
}

code 112
{
You cannot switch channels while recruiting Party Members.
}

code 113
{
Party Search Failed: Please select the search conditions.
}

code 114
{
Changing the channel...
}

code 115
{
You cannot delegate your Party Leader authority while recruiting Party Members.
}

code 116
{
The Party server has been terminated.
}

code 117
{
The Party server has been restarted.
}

code 118
{
You stopped looking for a Party.
}

code 119
{
Select All
}

code 120
{
Remove All
}

code 121
{
Party Members cannot use the Recruit Party Member option.
}

code 122
{
Level Limit: %d ~ %d
}

code 123
{
This cannot be used while in a Raid.
}

code 124
{
You cannot recruit Party Members when your Party is full.
}

code 125
{
You cannot recruit Party Members during a Party Match.
}

code 126
{
Decline
}

code 127
{
Allow
}

code 128
{
Only 1 character
}

code 129
{
You cannot use the Look for Party option while in DK Square.
}

code 130
{
You cannot join Dead Front Parties.
}

code 131
{
Failed to leave the Party.
}

code 132
{
Party List
}

code 133
{
Display only Parties that are available to join.
}

code 134
{
Map
}

code 135
{
Slogan
}

code 136
{
The Party is not recruiting new members.
}

code 137
{
Auto Party Request
}

code 138
{
Failed to refresh the list.
}

code 139
{
No such Party exists. Please refresh the Party list.
}

code 140
{
This Party stopped recruiting new members. Please refresh the Party list.
}

code 141
{
Look for Party 1:1 Chat
}

code 142
{
Recruit Party Member 1:1 Chat - 
}

code 143
{
Request
}

code 144
{
[Party Invitation Failed] You cannot invite characters in Instance Dungeons.
}

code 145
{
[Party Invitation Failed] You are in a Raid.
}

code 146
{
[Party Invitation Failed] The character is in a Raid.
}

code 147
{
[Party Invitation Failed] An unknown error occurred.
}

code 148
{
[Party Invitation Failed] This character does not meet your criteria for new members.
}

code 149
{
The Party's recruitment criteria has changed; your message cannot be transferred to the Party.
}

code 150
{
The Party's recruitment criteria has changed.
}

code 151
{
The Party is full; you cannot join the Party.
}

code 152
{
[Party Invitation Failed] You cannot invite characters in the DK server.
}

code 153
{
Searching parties...
}

code 154
{
<color=00ff00><VALUE1></color> parties have been found.
}

code 155
{
No search results found.
}

code 156
{
You are currently in a Colosseum Match.
}

code 157
{
The character is currently in a Colosseum Match.
}

code 158
{
You cannot leave a party while in a Colosseum Match.
}

code 159
{
You cannot expel party members while in a Colosseum Match.
}

</cate>
