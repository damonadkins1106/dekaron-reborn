<cate> 310
code 1
{
<font=4>Reinforcement Socket
}

code 2
{
<font=4>Rate
}

code 3
{
<font=4>Fortify
}

code 4
{
<font=4>Cost
}

code 10
{
<font=4>Item Reinforcement
}

code 11
{
<font=4>Item
}

code 12
{
<font=4>Ingredients
}

code 13
{
<font=4>Required Ingredients by Level
}

code 14
{
<font=4>+1 ~ +3
}

code 15
{
<font=4>+4 ~ +6
}

code 16
{
<font=4>+7 ~ +9
}

code 21
{
<font=4>Remove Socket Stone
}

code 22
{
<font=4>Remove
}

code 23
{
<font=4>Cost
}

code 24 #Socket Refinement UI Window
{
<font=4>Refine Socket
}

code 25 #Refine Button Name
{
<font=4>Refine
}

code 26 #Name for the cost
{
<font=4>Cost of Refinement
}

code 30
{
<font=4>Creating options
}

code 31
{
<font=4>Expense
}

code 32
{
<font=4>Necessary materials for each item upgrade
}

code 33
{
<font=4>Magic
}

code 34
{
<font=4>Nobility
}

code 35
{
<font=4>Divinity
}

code 36
{
<font=4>Creation
}

code 37
{
<font=4>Probability
}

code 38
{
<font=4>X2
}

code 39
{
  
}

code 40
{
Items after Reinforcement
}

code 41
{
Materials per Item Level (Accessory)
}

code 42
{
+1
}

code 43
{
+2
}

code 44
{
+3
}

code 45
{
Remove
}

code 46
{
DK
}

code 47
{
Materials to Remove Options
}

code 48
{
All Grades
}

code 49
{
DK Option Creation
}

code 50
{
Remove Options
}

code 51
{
Create/Remove Options
}

</cate>

<cate> 350

code 1
{
[Message] The item has been sealed.
}

code 2
{
Target item not in the bag.
}

code 3
{
[Message] The item does not exist.
}

code 4
{
[Message] The item does not exist.
}

code 5
{
[Message] You do not have enough Sealing Powders.
}

code 6
{
[Message] Your Inventory is full; you cannot seal the item.
}

code 7
{
Seal
}

code 8
{
[Message] Equipped items cannot be sealed.
}

code 9
{
[Message] Please select an item first.
}

code 10
{
[Message] You do not have enough Sealing Powders.
}

code 11
{
Seal Item
}
</cate>

<cate> 1001 #Item Upgrade, Socket Refining, Socket Stone Removal
code 1
{
[Message] You cannot switch your weapons while reinforcing items.
}

code 2
{
[Message] Equipped items cannot be reinforced.
}

code 3
{
[Message] You do not have enough dil.
}

code 4
{
[Message] This item cannot be reinforced.
}

code 5
{
[Message] Only weapons, armors, accessories, and Reinforcement Stones can be used.
}

code 6
{
[Message] Congratulations, the item was successfully reinforced!
}

code 7
{
[Message] You failed to reinforce the item; the item has been destroyed.
}

code 8
{
[Message] The Reinforcement Stone has been destroyed.
}

code 9
{
[Message] The target item has been destroyed.
}

code 10
{
[Message] The target item's level has degraded.
}

code 11
{
[Message] You failed to reinforce the item.
}

code 12
{
[Message] You  failed to reinforce the item; the item's level has been decreased to <itemlevel>.
}

code 13
{
[Message] This cannot be used when the target item's level is 0.
}

code 20
{
Items at Level 4 or higher have 
a high chance of being destroyed.
Are you sure that you want to reinforce this item? 
}

code 21
{
Please check the level of the Reinforcement Stone.
}

code 22
{
Please select an item to reinforce.
}

code 30 #Select [Refining] Item
{
[Message] Please select a weapon or armor to refine its socket.
}

code 31 #[Refining] Unavailable to Refine
{
[Message] The item is not eligible for Socket Refining.
}

code 32 #[Refining] Not a Socket Stone
{
[Message] Only Socket Stones can be placed in the Refine Socket window.
}

code 33 #[Refining] Failed
{
[Message] You failed to refine the socket.
}

code 34 #[Refining] Dil Spent
{
You spent <VALUE> dil.
}

code 35 #[Refining] Success
{
[Message] Congratulations, you successfully refined the item socket.
}

code 36 #[Refining] Not a Cash Item
{
[Message] This item is not eligible for Socket Refining.
}

code 37 #[Refining] Cannot Refine Any Further
{
[Message] The item socket has been refined to the maximum level; it cannot be refined any further.
}

code 40 #[Removal] Socket Item's Index Overflow
{
[Message] Your request cannot be processed for now.
}

code 41 #[Removal] No Item to Remove
{
[Message] Please select an equipment item to remove its Socket Stone.
}

code 42 #[Removal] Not a Cash Item
{
[Message] This Cash item is not eligible for Socket Stone Removal.
}

code 43 #[Removal] No Socket or Agate
{
[Message] Only Socket Stones and Agates can be used to remove Socket Stones.
}

code 44 #[Removal] Not Enough Money
{
[Message] You do not have enough dil.
}

code 45 #[Removal] Item with No Socket
{
[Message] The item has no socket or Socket Stone to remove.
}

code 46 #[Removal] No Socket Stone in Item
{
[message] The item socket is empty.
}

code 47 #[Removal] Success
{
[Message] The Socket Stone has been successfully removed.
}

code 48 #[Removal] Failed
{
[Message] You failed to remove the Socket Stone.
}

code 49 #[Removal] Equipped Item Selected
{
[Message] You cannot remove a Socket Stone from equipped items.
}

code 50 #[Removal] Equipment Switch
{
[Message] You cannot switch your equipment while removing a Socket Stone.
}

code 51 #[Removal] Socket Removal for Invalid Item
{
[Message] This item is not eligible for Socket Stone Removal.
}

code 52 #[Removal] Unavailable Item for Socket Removal
{
[Message] This item is not eligible for Socket Stone Removal.
}

code 53 #[Refining] Refining Equipped Item
{
[Message] Equipped items cannot be refined.
}

code 60 #[Option] Success
{
[Message] Congratulations, the option was successfully created.
}

code 61 #[Option] No Available Item
{
[Message] Please select a weapon or armor first.
}

code 63 #[Option] No Option Item for Option Removal
{
[Message] The target item has no option to remove.
}

code 64 #[Option] Unavailable Item for Creation
{
[Message] The target item cannot have new options.
}

code 65 #[Option] Not Enough Money
{
[Message] You do not have enough dil.
}

code 66 #[Option] A cash item is used, but it is not in the correct location
{
[Message] This item cannot be used.
}

code 67 #[Option] An Option Stone is used, but it is not in the correct location
{
[Message] This item cannot be used.
}

code 68 #[Option] Option Stone with Inappropriate Level
{
[Message] This Option Stone's level is not appropriate for the target item.
}

code 69 #[Option]  PENALTY_0 Failure Results	( Option -0 )
{
[Message] You failed to create the option, but the option's level has not been decreased.
}

code 70 #[Option]  PENALTY_1 Failure Results	( Option -1 )
{
[Message] You failed to create the option, and the option's level has been decreased by 1.
}

code 71 #[Option]  PENALTY_2 Failure Results	( Option -2 )
{
[Message] You failed to create the option, and the option's level has been decreased by 2.
}

code 72 #[Option] Unavailable Item for Option Creation/Removal
{
[Message] You cannot create or remove options for this item.
}

code 73 #[Option] Select Item First
{
[Message] Please select a weapon or armor first.
}

code 74 #[Option] Inappropriate Option Stone Level
{
[Message] This Option Stone's level is not appropriate for the target item.
}

code 75 #[Option] Select Option Stone First
{
[Message] Please select an appropriate Option Stone for the level of the item option.
}

code 76 #[Option] Creating Option for Equipped Item
{
[Message] You cannot create an option for equipped items.
}

code 77 #[Option] Inappropriate Item
{
[Message] Only weapons, armor, and Option Stones can be used.
}

code 78 #[Option] Option Removed Already
{
[Message] This item cannot be used at its current stage.
}

code 79 #[Option] Success
{
[Message] You successfully removed the option.
}

code 80 #[Option] No Option to Remove
{
[Message] This item has no option to remove.
}

code 81 #[Option] Normal Option Creation
{
[Message] You have selected Option Creation.
}

code 82 #[Option] DK Option Craft
{
[Message] You have selected to create options for a DK/Colosseum item.
}

code 83 #[Option] Option Removal
{
[Message] You have selected Option Removal.
}

code 84 #[Option] Option protective cash items have been updated so that they cannot be used for normal items.
{
[Message] Normal items have no option to protect; this item cannot be used.
}

code 85 #[Option] Inappropriate Protective Item for Target Item
{
[Message] This item cannot be used to reinforce accessories.
}

code 86 #[Option] For Normal Item Reinforcement
{
[Message] This item cannot be used to reinforce accessories.
}

code 87 #[Option] For Accessory Reinforcement
{
[Message] This item cannot be used to reinforce weapons or armor.
}

code 88
{
Reinforcement Results
}

code 89
{
Success %
}

code 90 #Refining Button Name
{
Refine
}

code 91 #Button for Fee
{
 
}

code 92
{
Remove Option
}

code 93
{
Create DK Option
}

code 94
{
Create Option
}

code 95
{
Create
}

code 96
{
Remove
}

code 97
{
 
}

code 98
{
Chance
}

code 99
{
x2
}

code 100
{
Necessary Materials per Item Grade
}

code 101
{
Magic
}

code 102
{
Noble
}

code 103
{
Divine
}

code 104
{
DK
}

code 105
{

}

code 106
{
All Grades
}

code 107
{
Success %
}

code 108
{
Removal Fee
}

code 109
{
Item
}

code 110
{
Materials
}

code 111
{
Protect
}

code 112
{
Reinforce
}

code 113
{
Reinforcement Fee
}

code 114
{
Necessary Materials per Item Level
}

code 115
{
+1 ~ +3
}

code 116
{
+4 ~ +6
}

code 117
{
+7 ~ +9
}

code 118
{
The item cannot be found.
}

code 119
{
Would you like to reinforce this item?
}

code 120 #Opening Pet Inventory
{
You cannot open the Pet Inventory while removing/refining sockets, reinforcing items, or creating options.
}

code 121
{
 
}

code 157
{
Alchemist's Spirit will improve the success rate.
}

code 158
{
  Add an item which you would like to place a socket
}

code 160
{
<color=FFD700>DK Shop</color> items can be added to increase the removal rate.
}

code 162
{
Place an item which you would like to add options
}

code 164
{
Protection can be used to keep the item from being destroyed upon upgrading.
}

code 166
{
  Add the item which you would like to enhance or place a socket
}


code 167
{
 
}

</cate>