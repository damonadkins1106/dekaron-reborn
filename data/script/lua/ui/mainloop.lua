-- mainloop.lua

m_strCurScreenName = "NONE_SCREEN";

function Construct()
	WrapCreateLua("UI/Common/errorwnd.lua", "ERROR_LUA");
end

function Destroy()
	DestroyScreen();
	WrapDestroyLua("ERROR_LUA");	
end

--UIHANDLER의 Initialize()와 대응되는 함수.
function InitWindow( strNational, strLuaName )
	WrapRegister_LuaCreate();		

	WrapInitWindow( "UI/Game2/ui_mainloop.txt", strLuaName ); 
	
	Construct();
end

--UIHANDLER의 ListenEvent()와 대응되는 함수.
function ProcessEvent( CtrlKind, CtrlName, EventCode, EventValue )
	return false;
end

--UIHANDLER의 ProcessMessage()와 대응되는 함수.
function ProcessMessage( strMsgCode, strArg1, strArg2, strArg3, strArg4, strArg5 )

	if strMsgCode == "FACTORYMSG::s_dwSetScreen" then
		SetScreen( strArg1 );
		return true;
	end

	return false;
end

function SetScreen( strScreenName )

	if m_strCurScreenName == strScreenName then
		return;
	end

	DestroyScreen();
	m_strCurScreenName = strScreenName;

	if strScreenName == "LOGIN_SCREEN" then
		Create_LoginScreen();
		return;
	
	elseif strScreenName == "SELECT_SCREEN" then
		Create_SelectScreen();
		return
	end	
end

function DestroyScreen()

	if m_strCurScreenName == "LOGIN_SCREEN" then
		Destroy_LoginScreen();
		return;
	
	elseif m_strCurScreenName == "SELECT_SCREEN" then
		Destroy_SelectScreen();
		return
	end	
end


function Create_LoginScreen()
	WrapCreateLua("UI/Common/loginwnd_test.lua", "LOGIN_LUA_TEST");
	WrapCreateLua("UI/Common/loginwnd.lua", "LOGIN_LUA");
end

function Destroy_LoginScreen()
	WrapDestroyLua( "LOGIN_LUA" );
	WrapDestroyLua( "LOGIN_LUA_TEST" );
end

function Create_SelectScreen()

end

function Destroy_SelectScreen()

end



