-- loginwnd_test.lua

--UIHANDLER의 Initialize()와 대응되는 함수.
function InitWindow( strNational, strLuaName )
	WrapRegister_UIControl();

	WrapInitWindow( "UI/Game2/ui_LoginWnd_test_2.txt", strLuaName ); 
end

--UIHANDLER의 ListenEvent()와 대응되는 함수.
function ProcessEvent( CtrlKind, CtrlName, EventCode, EventValue )
	if CtrlKind == "BUTTON" and EventCode == "CLICKED" then

		if CtrlName == "btn_pantheon_test" then
			Click_Pantheon_test()
			return true;

		elseif CtrlName == "select_ok_test" then
			Click_SelectOk_test();
			return true;
		end
	end

	return false;
end

--UIHANDLER의 ProcessMessage()와 대응되는 함수.
function ProcessMessage( strMsgCode, strArg1, strArg2, strArg3, strArg4, strArg5 )
	return false;
end

-- 서버 목록에서 확인 버튼을 눌렀을때.
function Click_SelectOk_test()
			
	WrapCompositeVisible("pan_login_test", true);
	WrapCompositeVisible("pan_selectgroup_test", false);
end

-- pantheon을 눌러서 서버목록창으로 갈려고 할때.
function Click_Pantheon_test()
	WrapCompositeVisible("pan_login_test", false);
	WrapCompositeVisible("pan_selectgroup_test", true);
end
