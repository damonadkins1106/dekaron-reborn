-- errorwnd.lua

--UIHANDLER의 Initialize()와 대응되는 함수.
function InitWindow( strNational, strLuaName )
	WrapRegister_UIControl();

	WrapInitWindow( "UI/Game2/ui_luaerrorwnd.txt", strLuaName ); 
end

--UIHANDLER의 ListenEvent()와 대응되는 함수.
function ProcessEvent( CtrlKind, CtrlName, EventCode, EventValue )
	if CtrlKind == "BUTTON" and EventCode == "CLICKED" then

		if CtrlName == "btn_yes" then
			Click_BtnYes()
			return true;
		end
	end

	return false;
end

--UIHANDLER의 ProcessMessage()와 대응되는 함수.
function ProcessMessage( strMsgCode, strArg1, strArg2, strArg3, strArg4, strArg5 )
	
	if strMsgCode == "FORMATIONMSG::s_dwErrorMsgBox" then
		Show_LuaErrorMsg( strArg1 );
		return true;
	end

	return false;
end

function Click_BtnYes()
	WrapSetWindowVisible(false);
end

function Show_LuaErrorMsg( strArg1 )
	local bVisible = WrapGetWindowVisible();
	if bVisible == false then 
		WrapSetWindowVisible(true);
		WrapSetLabel( "lbl_text", strArg1 );
	end
end