-- loginwnd.lua
-- 항상 InitWindow, ProcessEvent, ProcessMessage 3가지 함수는 존재 해야만 한다.
-- 하나라도 존재 하지 않으면..오류를 일으켜 버그를 생산 한다.조심조심..
-- 나머지 함수는 필요에 따라 만들어 쓰도록 하자.

bySelectServer = 255;
bShowMessage = false;
dwMsgTime = 0;

--UIHANDLER의 Initialize()와 대응되는 함수.
function InitWindow( strNational, strLuaName )
	WrapRegister_UIControl();
	WrapRegister_Addition("WrapTryLogin");
	WrapRegister_Addition("WrapGetScriptString");
	WrapRegister_Addition("WrapGetServerAddressName");
	WrapRegister_Addition("WrapQuitGame");


	WrapInitWindow( "UI/Game2/ui_LoginWnd.txt", strLuaName ); 
	
	if strNational == "CHINESE" then
	WrapCompositeVisible("pan_15", false);
	WrapCompositeVisible("pan_18", false);
	end
	
	for i=0, 3, 1 do 
		local strName = string.format("groupbtn_%d", i);	
		local strServerName = WrapGetServerAddressName(i);

		if strServerName == "EMPTY" then
			WrapCompositeVisible(strName, false);
		else
			WrapSetButtonTitle(strName, strServerName);
		end
	end

	local strMsg = WrapGetScriptString(600, 9);
	WrapSetLabel("lbl_loginwaring", strMsg );
	
	bySelectServer = 255;
	dwMsgTime = 0;
	CloseSystemMessage()
end

--UIHANDLER의 ListenEvent()와 대응되는 함수.
function ProcessEvent( CtrlKind, CtrlName, EventCode, EventValue )
	if CtrlKind == "BUTTON" and EventCode == "CLICKED" then
		
		if CtrlName == "btn_login" then
			Click_LogIn();
			return true;

		elseif CtrlName == "btn_quit" then
			WrapQuitGame();
			return true;

		elseif CtrlName == "btn_pantheon" then
			Click_Pantheon()
			return true;

		elseif CtrlName == "select_ok" then
			Click_SelectOk();
			return true;

		else
			if Click_ServerList(CtrlName) == true then
				return true;
			end
		end
	end

	return false;
end

--UIHANDLER의 ProcessMessage()와 대응되는 함수.
function ProcessMessage( strMsgCode, strArg1, strArg2, strArg3, strArg4, strArg5 )
	
	if strMsgCode == "IK_Enter" then
		Click_LogIn();
		return true;

	elseif strMsgCode == "IK_Tab" then
		onTab();
		return true;

	elseif strMsgCode == "MSGBOXMSG::s_dwShowRank18" then

		WrapCompositeVisible("pan_15", false);
		local strMsg = 	WrapGetScriptString(100, 7);
		SetSystemMsg( strMsg );

		return true;

	elseif strMsgCode == "FORMATIONMSG::s_dwSystemMsg" then
		SetSystemMsg( strArg1 );

		return true;

	
	elseif strMsgCode == "COMMON_TIMEMSG" then
		ProcessTimeMsg( tonumber(strArg1) );
	end
	
	return false;
end


-- 이하 루아 스크립트내에서 필요에 의해 만들어진 함수들.
-- C와 대응되지 않으니 자유롭게 만들어 쓰도록..
function ProcessTimeMsg( dwElapsedTime )
	
	if bShowMessage == false then
		return;
	end

	dwMsgTime = dwMsgTime + dwElapsedTime;
	if dwMsgTime > 3000 then
		CloseSystemMessage();
	end

end

function SetSystemMsg( strMsg )
	dwMsgTime = 0;
	bShowMessage = true;
	WrapCompositeVisible( "pan_message", bShowMessage );
	WrapSetLabel( "lbl_message", strMsg );
end

function CloseSystemMessage()
	bShowMessage = false;
	WrapCompositeVisible( "pan_message", bShowMessage );
end

function GetSelected()
	return bySelectServer;
end

function Click_ServerList( CtrlName )
	local IsServerList = false;
	for i=0, 3, 1 do 
		local strName = string.format("groupbtn_%d", i);	
		if strName == CtrlName then
			bySelectServer = i;
			IsServerList = true;
			break;
		end
	end
			
	if IsServerList == false then
		return false;
	end

	for i=0, 3, 1 do 
		local strName = string.format("groupbtn_%d", i);	
		WrapSetButtonCheck(strName, false);
	end
			
	WrapSetButtonCheck(CtrlName, true);	
	return true;
end

function Click_SelectOk()
	if bySelectServer == 255 then
		return; 
	end
			
	WrapCompositeVisible("pan_login", true);
	WrapCompositeVisible("pan_selectgroup", false);
	WrapInitButton("select_ok");
	CloseSystemMessage();
	WrapSetFocus("edt_account");

	local strServerName = WrapGetServerAddressName(bySelectServer);
	WrapSetLabel("pan_pantheon", strServerName );
end

function Click_Pantheon()
	WrapCompositeVisible("pan_login", false);
	WrapCompositeVisible("pan_selectgroup", true);
	WrapInitButton("select_ok");
	CloseSystemMessage();
	WrapClearEdit("edt_account");
	WrapClearEdit("edt_password");
end

function Click_LogIn()
	local strID = WrapGetEditString("edt_account");
	local strPswd = WrapGetEditString("edt_password");

	if strID == "EMPTY" or strPswd == "EMPTY" then
		local strMsg = 	WrapGetScriptString(600, 10);
		SetSystemMsg( strMsg );
		return;
	end

	WrapTryLogin(bySelectServer, strID, strPswd);
end

function onTab()
	local bFocus = WrapGetFocus("edt_account");
	if bFocus == true then 
		WrapSetFocus("edt_password");
	else
		WrapSetFocus("edt_account");
	end
end
