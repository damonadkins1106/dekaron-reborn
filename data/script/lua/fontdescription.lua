-- define font description
font_description = {	
	{
		height		= 14, -- 14
		width			= 0,
		weight		= 300,
		shadow		= false,
		facename		= "MS Sans Serif"
	},		
	{
		height		= 14, -- 14
		width			= 0,
		weight		= 500,
		shadow		= false,
		facename		= "MS Sans Serif"
	},		
	{ 	height 		= 14,
		width			= 0,
		weight			= 300,
		shadow			= false,
		facename		= "MS Sans Serif"
	},
	{	height			= 14,
		width			= 0,
		weight			= 300,
		shadow			= false,
		facename		= "MS Sans Serif"
	},
	{	height			= 14,
		width			= 0,
		weight			= 500,
		shadow			= false,
		facename		= "MS Sans Serif"
	},
	{	height			= 14,
		width			= 0,
		weight			= 500,
		shadow			= true,
		facename		= "MS Sans Serif"
	},
	{	height			= 16, -- 16
		width			= 0,
		weight			= 300,
		shadow			= false,
		facename		= "MS Sans Serif"
	},
	{	height			= 16, -- 16
		width			= 0,
		weight			= 500,
		shadow			= false,
		facename		= "MS Sans Serif"
	},
	{	height			= 16,
		width			= 0,
		weight			= 700,
		shadow			= false,
		facename		= "MS Sans Serif"
	},
	{	height			= 18, -- 18
		width			= 0,
		weight			= 700,
		shadow			= false,
		facename		= "MS Sans Serif"
	},
	{	height			= 20,
		width			= 0,
		weight			= 300,
		shadow			= false,
		facename		= "MS Sans Serif"
	},
	{	height			= 20,
		width			= 0,
		weight			= 500,
		shadow			= false,
		facename		= "MS Sans Serif"
	},
	{	height			= 24,
		width			= 0,
		weight			= 300,
		shadow			= false,
		facename		= "MS Sans Serif"
	},
	{	height			= 24,
		width			= 0,
		weight			= 500,
		shadow			= false,
		facename		= "MS Sans Serif"
	},
		{
		height			= 14,
		width			= 0,
		weight			= 300,
		shadow			= false,
		facename		= "MS Sans Serif"
	},		
	{
		height			= 14,
		width			= 0,
		weight			= 500,
		shadow			= false,
		facename		= "MS Sans Serif"
	},		
	{ 	height 			= 14,
		width			= 0,
		weight			= 300,
		shadow			= false,
		facename		= "MS Sans Serif"
	},
	{	height			= 14,
		width			= 0,
		weight			= 300,
		shadow			= false,
		facename		= "MS Sans Serif"
	},
	{	height			= 14,
		width			= 0,
		weight			= 500,
		shadow			= false,
		facename		= "MS Sans Serif"
	},
	{	height			= 14,
		width			= 0,
		weight			= 500,
		shadow			= true,
		facename		= "MS Sans Serif"
	},
	{	height			= 16,
		width			= 0,
		weight			= 300,
		shadow			= false,
		facename		= "MS Sans Serif"
	},
	{	height			= 16,
		width			= 0,
		weight			= 500,
		shadow			= false,
		facename		= "MS Sans Serif"
	},
	{	height			= 18,
		width			= 0,
		weight			= 700,
		shadow			= false,
		facename		= "MS Sans Serif"
	},
	{	height			= 18,
		width			= 0,
		weight			= 700,
		shadow			= false,
		facename		= "MS Sans Serif"
	},
	{	height			= 20,
		width			= 0,
		weight			= 300,
		shadow			= false,
		facename		= "MS Sans Serif"
	},
	{	height			= 20,
		width			= 0,
		weight			= 500,
		shadow			= false,
		facename		= "MS Sans Serif"
	},
	{	height			= 24,
		width			= 0,
		weight			= 300,
		shadow			= false,
		facename		= "MS Sans Serif"
	},
	{	height			= 24,
		width			= 0,
		weight			= 500,
		shadow			= false,
		facename		= "MS Sans Serif"
	}
}