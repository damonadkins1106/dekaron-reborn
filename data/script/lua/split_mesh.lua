-----------------------------
-- input 형태
-- attack_direction: 	up_down, down_up, right_left, upright_downleft, downright_upleft,
-- 						left_right, upleft_downright, downleft_upright

-- attack_type:			strike, cut

-- weapon_class:		short_sword, sword, long_sword, great_sword,
--						spear,
--						mace, axe, wand,
--						bow, cross_bow, magic

-- monster_type:		twolimbs, fourlimbs, insect, head

-- finish_type:			normal_finish, big_finish, enormous_finish

-----------------------------
-- output 형태
--						meshname 		(ABCDEFG..)
--						direction		(1~5)
--						length			(meter)
--						height			(meter)
--						rotate_axis		(x+, x-, y+, y-, z+, z-)
--						rotate_speed	(positive: absolute rotation, negative: rotation degree per sec
--						rotate_point	(top, middle, bottom)
--						speed			(meter/sec)
--						property		(preserve, reaction, break)

function SplitMesh( attack_direction, 		
					attack_type, 
					damage, 
					finish_type,
					total_hp, 
					weapon_class, 
					monster_type )
					
	local splited_mesh 	= {}
	
	local goingLength	= GetLength( damage, total_hp, weapon_class )
	local speed			= GetSpeed( goingLength )
	local height		= GetHeight( goingLength )
	local property		= GetProperty( finish_type )
	
	-- test
	-- attack_type			= "cut"
	-- finish_type			= "enormous_finish"
	-- attack_direction	= "up_down"
	-- test
	
	if	attack_type			== "cut" then
		if attack_direction	== "up_down" then
			splited_mesh.split_count				= 2
			splited_mesh[1] 						= {}
			splited_mesh[2] 						= {}
			
			splited_mesh[1].rotate_point			= "bottom"
			splited_mesh[2].rotate_point			= "bottom"
			splited_mesh[1].direction				= 5
			splited_mesh[2].direction				= 1
			splited_mesh[1].rotate_axis				= "x+"
			splited_mesh[2].rotate_axis				= "x+"
			splited_mesh[1].rotate_speed			= 90
			splited_mesh[2].rotate_speed			= 90
			splited_mesh[1].length					= goingLength
			splited_mesh[2].length					= goingLength
			splited_mesh[1].height					= height;
			splited_mesh[2].height					= height;
			splited_mesh[1].speed					= speed
			splited_mesh[2].speed					= speed
			splited_mesh[1].property				= property
			splited_mesh[2].property				= property
	
			if monster_type == "twolimbs" then			
				splited_mesh[1].meshname		= "ACEGI"
				splited_mesh[2].meshname		= "BDFHJ"	
			elseif monster_type == "fourlimbs" then
				splited_mesh[1].meshname		= "ACEI"
				splited_mesh[2].meshname		= "BDFJ"
			elseif monster_type == "insect" then
				splited_mesh[1].meshname		= "AC"
				splited_mesh[2].meshname		= "BD"
			elseif monster_type == "head" then
				splited_mesh[1].meshname		= "A"
				splited_mesh[2].meshname		= "B"
			else
				error("invalid monster_type" .. monster_type )
			end
			
		elseif attack_direction == "down_up" then
			splited_mesh.split_count				= 2
			splited_mesh[1] 						= {}
			splited_mesh[2] 						= {}
			
			splited_mesh[1].rotate_point			= "bottom"
			splited_mesh[2].rotate_point			= "bottom"
			splited_mesh[1].direction				= 4
			splited_mesh[2].direction				= 2
			splited_mesh[1].rotate_axis				= "x+"
			splited_mesh[2].rotate_axis				= "x+"
			splited_mesh[1].rotate_speed			= 90
			splited_mesh[2].rotate_speed			= 90
			splited_mesh[1].length					= goingLength
			splited_mesh[2].length					= goingLength
			splited_mesh[1].height					= height;
			splited_mesh[2].height					= height;
			splited_mesh[1].speed					= speed
			splited_mesh[2].speed					= speed
			splited_mesh[1].property				= property
			splited_mesh[2].property				= property
			
			if monster_type == "twolimbs" then
				if finish_type == "normal_finish" then
					splited_mesh[1].meshname		= "ACEGI"
					splited_mesh[2].meshname		= "BDFHJ"
				elseif finish_type == "big_finish" then
					splited_mesh[1].meshname		= "ACEGI"
					splited_mesh[2].meshname		= "BDFHJ"
				elseif finish_type == "enormous_finish" then
					splited_mesh[1].meshname		= "ACEGI"
					splited_mesh[2].meshname		= "BDFHJ"
				end
			elseif monster_type == "fourlimbs" then
				if finish_type == "normal_finish" then
					splited_mesh[1].meshname		= "ACEI"
					splited_mesh[2].meshname		= "BDFJ"
				elseif finish_type == "big_finish" then
					splited_mesh[1].meshname		= "ACEI"
					splited_mesh[2].meshname		= "BDFJ"
				elseif finish_type == "enormous_finish" then
					splited_mesh[1].meshname		= "ACEI"
					splited_mesh[2].meshname		= "BDFJ"
				end
			elseif monster_type == "insect" then
				splited_mesh[1].meshname			= "AC"
				splited_mesh[2].meshname			= "BD"
			elseif monster_type == "head" then
				splited_mesh[1].meshname			= "A"
				splited_mesh[2].meshname			= "B"
			else 
				error("invalid monster_type" .. monster_type )
			end
		
		elseif attack_direction == "left_right" then
			splited_mesh.split_count				= 2
			splited_mesh[1] 						= {}
			splited_mesh[2] 						= {}
			
			splited_mesh[1].rotate_point			= "middle"
			splited_mesh[2].rotate_point			= "middle"
			splited_mesh[1].direction				= 1
			splited_mesh[2].direction				= 0
			splited_mesh[1].rotate_axis				= "z+"
			splited_mesh[2].rotate_axis				= "z+"
			splited_mesh[1].rotate_speed			= 360
			splited_mesh[2].rotate_speed			= 360
			splited_mesh[1].length					= goingLength
			splited_mesh[2].length					= goingLength
			splited_mesh[1].height					= height;
			splited_mesh[2].height					= height;
			splited_mesh[1].speed					= speed
			splited_mesh[2].speed					= speed
			splited_mesh[1].property				= property
			splited_mesh[2].property				= property
			
			if monster_type == "twolimbs"  then
				if finish_type == "normal_finish" then
					splited_mesh[1].meshname		= "AB"
					splited_mesh[2].meshname		= "CDEFGHIJ"
				elseif finish_type == "big_finish" then
					splited_mesh[1].meshname		= "ABCDEF"
					splited_mesh[2].meshname		= "GHIJ"
				elseif finish_type == "enormous_finish" then
					splited_mesh[1].meshname		= "ABCDEF"
					splited_mesh[2].meshname		= "GHIJ"
				end
			elseif monster_type == "fourlimbs" then
				if finish_type == "normal_finish" then
					splited_mesh[1].meshname		= "AB"
					splited_mesh[2].meshname		= "CDEFIJ"
				elseif finish_type == "big_finish" then
					splited_mesh[1].meshname		= "ABCDEF"
					splited_mesh[1].meshname		= "IJ"
				elseif finish_type == "enormous_finish" then
					splited_mesh[1].meshname		= "ABCDEF"
					splited_mesh[1].meshname		= "IJ"
				end
			elseif monster_type == "insect" then
				splited_mesh[1].meshname			= "AB"
				splited_mesh[2].meshname			= "DC"
				splited_mesh[2].direction			= splited_mesh[1].direction
			elseif monster_type == "head" then
				splited_mesh[1].meshname			= "A"
				splited_mesh[2].meshname			= "B"
				splited_mesh[2].direction			= splited_mesh[1].direction
			else 
				error("invalid monster_type" .. monster_type )
			end
		
		elseif attack_direction == "right_left" then
			splited_mesh.split_count				= 2
			splited_mesh[1] 						= {}
			splited_mesh[2] 						= {}
			
			splited_mesh[1].rotate_point			= "middle"
			splited_mesh[2].rotate_point			= "middle"
			splited_mesh[1].direction				= 5
			splited_mesh[2].direction				= 0
			splited_mesh[1].rotate_axis				= "z-"
			splited_mesh[2].rotate_axis				= "z-"
			splited_mesh[1].rotate_speed			= 360
			splited_mesh[2].rotate_speed			= 360
			splited_mesh[1].length					= goingLength
			splited_mesh[2].length					= goingLength
			splited_mesh[1].height					= height;
			splited_mesh[2].height					= height;
			splited_mesh[1].speed					= speed
			splited_mesh[2].speed					= speed
			splited_mesh[1].property				= property
			splited_mesh[2].property				= property
			
			if monster_type == "twolimbs" then
				if finish_type == "normal_finish" then
					splited_mesh[1].meshname		= "AB"
					splited_mesh[2].meshname		= "CDEFGHIJ"
				elseif finish_type == "big_finish" then
					splited_mesh[1].meshname		= "ABCDEF"
					splited_mesh[2].meshname		= "GHIJ"
				elseif finish_type == "enormous_finish" then
					splited_mesh[1].meshname		= "ABCDEF"
					splited_mesh[2].meshname		= "GHIJ"
				end
			elseif monster_type == "fourlimbs" then
				if finish_type == "normal_finish" then
					splited_mesh[1].meshname		= "AB"
					splited_mesh[2].meshname		= "CDEFIJ"
				elseif finish_type == "big_finish" then
					splited_mesh[1].meshname		= "ABCDEF"
					splited_mesh[2].meshname		= "IJ"
				elseif finish_type == "enormous_finish" then
					splited_mesh[1].meshname		= "ABCDEF"
					splited_mesh[2].meshname		= "IJ"
				end
			elseif monster_type == "insect" then
				splited_mesh[1].meshname			= "AB"
				splited_mesh[2].meshname			= "DC"
				splited_mesh[2].direction			= splited_mesh[1].direction
			elseif monster_type == "head" then
				splited_mesh[1].meshname			= "A"
				splited_mesh[2].meshname			= "B"
				splited_mesh[2].direction			= splited_mesh[1].direction
			else 
				error("invalid monster_type" .. monster_type )
			end
			
		elseif attack_direction == "upleft_downright" then
			splited_mesh.split_count				= 2
			splited_mesh[1] 						= {}
			splited_mesh[2] 						= {}
			
			splited_mesh[1].rotate_point			= "middle"
			splited_mesh[2].rotate_point			= "middle"
			splited_mesh[1].direction				= 1
			splited_mesh[2].direction				= 0
			splited_mesh[1].rotate_axis				= "z-"
			splited_mesh[2].rotate_axis				= "z-"
			splited_mesh[1].rotate_speed			= 180
			splited_mesh[2].rotate_speed			= 180
			splited_mesh[1].length					= goingLength
			splited_mesh[2].length					= goingLength
			splited_mesh[1].height					= height;
			splited_mesh[2].height					= height;
			splited_mesh[1].speed					= speed
			splited_mesh[2].speed					= speed
			splited_mesh[1].property				= property
			splited_mesh[2].property				= property
			
			if monster_type == "twolimbs" then					
				splited_mesh[1].meshname			= "ABCDFH"
				splited_mesh[2].meshname			= "EGIJ"
			elseif monster_type == "fourlimbs" then
				splited_mesh[1].meshname			= "ABCDF"
				splited_mesh[2].meshname			= "EIJ"
			elseif monster_type == "insect" then
				splited_mesh[1].meshname			= "AC"
				splited_mesh[2].meshname			= "BD"
				splited_mesh[2].direction			= splited_mesh[1].direction
			elseif monster_type == "head" then
				splited_mesh[1].meshname			= "A"
				splited_mesh[2].meshname			= "B"
				splited_mesh[2].direction			= splited_mesh[1].direction
			else 
				error("invalid monster_type" .. monster_type )
			end
		
		elseif attack_direction == "upright_downleft" then
			splited_mesh.split_count				= 2
			splited_mesh[1] 						= {}
			splited_mesh[2] 						= {}
			
			splited_mesh[1].rotate_point			= "middle"
			splited_mesh[2].rotate_point			= "middle"
			splited_mesh[1].direction				= 5
			splited_mesh[2].direction				= 0
			splited_mesh[1].rotate_axis				= "z+"
			splited_mesh[2].rotate_axis				= "z+"
			splited_mesh[1].rotate_speed			= 180
			splited_mesh[2].rotate_speed			= 180
			splited_mesh[1].length					= goingLength
			splited_mesh[2].length					= goingLength
			splited_mesh[1].height					= height;
			splited_mesh[2].height					= height;
			splited_mesh[1].speed					= speed
			splited_mesh[2].speed					= speed
			splited_mesh[1].property				= property
			splited_mesh[2].property				= property
			
			if monster_type == "twolimbs" then
				splited_mesh[1].meshname			= "ABCDEG"
				splited_mesh[2].meshname			= "FHIJ"
			elseif monster_type == "fourlimbs" then
				splited_mesh[1].meshname			= "ABCDE"
				splited_mesh[2].meshname			= "FIJ"
			elseif monster_type == "insect" then
				splited_mesh[1].meshname			= "AC"
				splited_mesh[2].meshname			= "BD"
				splited_mesh[2].direction			= splited_mesh[1].direction
			elseif monster_type == "head" then
				splited_mesh[1].meshname			= "A"
				splited_mesh[2].meshname			= "B"
				splited_mesh[2].direction			= splited_mesh[1].direction
			else 
				error("invalid monster_type" .. monster_type )
			end
		
		elseif attack_direction == "downleft_upright" then
			splited_mesh.split_count				= 2
			splited_mesh[1] 						= {}
			splited_mesh[2] 						= {}
			
			splited_mesh[1].rotate_point			= "bottom"
			splited_mesh[2].rotate_point			= "bottom"
			splited_mesh[1].direction				= 5
			splited_mesh[2].direction				= 0
			splited_mesh[1].rotate_axis				= "z+"
			splited_mesh[2].rotate_axis				= "z+"
			splited_mesh[1].rotate_speed			= 360
			splited_mesh[2].rotate_speed			= 360
			splited_mesh[1].length					= goingLength
			splited_mesh[2].length					= goingLength
			splited_mesh[1].height					= height;
			splited_mesh[2].height					= height;
			splited_mesh[1].speed					= speed
			splited_mesh[2].speed					= speed
			splited_mesh[1].property				= property
			splited_mesh[2].property				= property
			
			if monster_type == "twolimbs" then
				splited_mesh[1].meshname			= "ABCDFH"
				splited_mesh[2].meshname			= "EGIJ"
			elseif monster_type == "fourlimbs" then
				splited_mesh[1].meshname			= "ABCDF"
				splited_mesh[2].meshname			= "EIJ"
			elseif monster_type == "insect" then
				splited_mesh[1].meshname			= "AC"
				splited_mesh[2].meshname			= "BD"
				splited_mesh[2].direction			= splited_mesh[1].direction
			elseif monster_type == "head" then
				splited_mesh[1].meshname			= "A"
				splited_mesh[2].meshname			= "B"
				splited_mesh[2].direction			= splited_mesh[1].direction
			else 
				error("invalid monster_type" .. monster_type )
			end
		
		elseif attack_direction == "downright_upleft" then
			splited_mesh.split_count				= 2
			splited_mesh[1] 						= {}
			splited_mesh[2] 						= {}
			
			splited_mesh[1].rotate_point			= "bottom"
			splited_mesh[2].rotate_point			= "bottom"
			splited_mesh[1].direction				= 1
			splited_mesh[2].direction				= 0
			splited_mesh[1].rotate_axis				= "z-"
			splited_mesh[2].rotate_axis				= "z-"
			splited_mesh[1].rotate_speed			= 360
			splited_mesh[2].rotate_speed			= 360
			splited_mesh[1].length					= goingLength
			splited_mesh[2].length					= goingLength
			splited_mesh[1].height					= height;
			splited_mesh[2].height					= height;
			splited_mesh[1].speed					= speed
			splited_mesh[2].speed					= speed
			splited_mesh[1].property				= property
			splited_mesh[2].property				= property
			
			if monster_type == "twolimbs" then
				splited_mesh[1].meshname			= "ABCDEG"
				splited_mesh[2].meshname			= "EHIJ"
			elseif monster_type == "fourlimbs" then
				splited_mesh[1].meshname			= "ABCDE"
				splited_mesh[2].mehsname			= "FIJ"
			elseif monster_type == "insect" then
				splited_mesh[1].meshname			= "AC"
				splited_mesh[2].meshname			= "BD"
				splited_mesh[2].direction			= splited_mesh[1].direction
			elseif monster_type == "head" then
				splited_mesh[1].meshname			= "A"
				splited_mesh[2].meshname			= "B"
				splited_mesh[2].direction			= splited_mesh[1].direction
			else 
				error("invalid monster_type" .. monster_type )
			end
		end
		
	elseif 	attack_type			== "strike" then
	
		if attack_direction	== "up_down" then
		
			if monster_type == "twolimbs" then
			
				if finish_type == "normal_finish" then
					local mesh_property						= {}
					mesh_property.meshname					= { "A", "B", "C", "D" }
					mesh_property.start_index				= 1
					mesh_property.end_index					= 4
					mesh_property.rotate_point				= "middle"
					mesh_property.direction					= 4
					mesh_property.rotate_axis				= "x+"
					mesh_property.rotate_speed				= -360
					mesh_property.length					= goingLength
					mesh_property.height					= height
					mesh_property.speed						= speed
					mesh_property.property					= property
					
					splited_mesh = MakeSplitedMesh( mesh_property )
					
					splited_mesh.split_count				= 5
					splited_mesh[5]							= {}
					splited_mesh[5].meshname				= "EFGHIJ"
					splited_mesh[5].rotate_point			= "middle"
					splited_mesh[5].direction				= 0
					splited_mesh[5].rotate_axis				= "x+"
					splited_mesh[5].rotate_speed			= -360
					splited_mesh[5].length					= goingLength
					splited_mesh[5].height					= height
					splited_mesh[5].speed					= speed
					splited_mesh[5].property				= property
					
				elseif finish_type == "big_finish" then
					local mesh_property						= {}
					mesh_property.meshname					= { "A", "B", "C", "D", "E", "F" }
					mesh_property.start_index				= 1
					mesh_property.end_index					= 6
					mesh_property.rotate_point				= "middle"
					mesh_property.direction					= 4
					mesh_property.rotate_axis				= "x+"
					mesh_property.rotate_speed				= -360
					mesh_property.length					= goingLength
					mesh_property.height					= height
					mesh_property.speed						= speed
					mesh_property.property					= property
					
					splited_mesh = MakeSplitedMesh( mesh_property )
					
					splited_mesh.split_count				= 7
					splited_mesh[7]							= {}
					splited_mesh[7].meshname				= "GHIJ"
					splited_mesh[7].rotate_point			= "middle"
					splited_mesh[7].direction				= 0
					splited_mesh[7].rotate_axis				= "x+"
					splited_mesh[7].rotate_speed			= -360
					splited_mesh[7].length					= goingLength
					splited_mesh[7].height					= height
					splited_mesh[7].speed					= speed
					splited_mesh[7].property				= property

-------------------------------------------------------------------------------------				
				elseif finish_type == "enormous_finish" then
					local mesh_property						= {}
					mesh_property.meshname					= { "A", "B", "C", "D", "EG", "FH" }
					mesh_property.start_index				= 1
					mesh_property.end_index					= 6
					mesh_property.rotate_point				= "middle"
					mesh_property.direction					= 4
					mesh_property.rotate_axis				= "x+"
					mesh_property.rotate_speed				= -360
					mesh_property.length					= goingLength
					mesh_property.height					= height
					mesh_property.speed						= speed
					mesh_property.property					= property
					
					splited_mesh = MakeSplitedMesh( mesh_property )
					
					splited_mesh.split_count				= 7
					splited_mesh[7]							= {}
					splited_mesh[7].meshname				= "IJ"
					splited_mesh[7].rotate_point			= "middle"
					splited_mesh[7].direction				= 0
					splited_mesh[7].rotate_axis				= "x+"
					splited_mesh[7].rotate_speed			= -360
					splited_mesh[7].length					= goingLength
					splited_mesh[7].height					= height
					splited_mesh[7].speed					= speed
					splited_mesh[7].property				= property
-------------------------------------------------------------------------------------
				else
					error( "Invalid finish type" .. finish_type )
				end
				
			elseif monster_type == "fourlimbs" then
			
				if finish_type == "normal_finish" then
				
					local mesh_property						= {}
					mesh_property.meshname					= { "A", "B", "C", "D" }
					mesh_property.start_index				= 1
					mesh_property.end_index					= 4
					mesh_property.rotate_point				= "middle"
					mesh_property.direction					= 4
					mesh_property.rotate_axis				= "x+"
					mesh_property.rotate_speed				= -360
					mesh_property.length					= goingLength
					mesh_property.height					= height
					mesh_property.speed						= speed
					mesh_property.property					= property
					
					splited_mesh = MakeSplitedMesh( mesh_property )
					
					splited_mesh.split_count				= 5
					splited_mesh[5]							= {}
					splited_mesh[5].meshname				= "EFIJ"
					splited_mesh[5].rotate_point			= "middle"
					splited_mesh[5].direction				= 0
					splited_mesh[5].rotate_axis				= "x+"
					splited_mesh[5].rotate_speed			= -360
					splited_mesh[5].length					= goingLength
					splited_mesh[5].height					= height
					splited_mesh[5].speed					= speed
					splited_mesh[5].property				= property
					
				elseif finish_type == "big_finish" then
					
					local mesh_property						= {}
					mesh_property.meshname					= { "A", "B", "C", "D", "E", "F" }
					mesh_property.start_index				= 1
					mesh_property.end_index					= 6
					mesh_property.rotate_point				= "middle"
					mesh_property.direction					= 4
					mesh_property.rotate_axis				= "x+"
					mesh_property.rotate_speed				= -360
					mesh_property.length					= goingLength
					mesh_property.height					= height
					mesh_property.speed						= speed
					mesh_property.property					= property
					
					splited_mesh = MakeSplitedMesh( mesh_property )
					
					splited_mesh.split_count				= 7
					splited_mesh[7]							= {}
					splited_mesh[7].meshname				= "IJ"
					splited_mesh[7].rotate_point			= "middle"
					splited_mesh[7].direction				= 0
					splited_mesh[7].rotate_axis				= "x+"
					splited_mesh[7].rotate_speed			= -360
					splited_mesh[7].length					= goingLength
					splited_mesh[7].height					= height
					splited_mesh[7].speed					= speed
					splited_mesh[7].property				= property
					
				elseif finish_type == "enormous_finish" then
				
					local mesh_property						= {}
					mesh_property.meshname					= { "A", "B", "C", "D", "E", "F", "I", "J" }
					mesh_property.start_index				= 1
					mesh_property.end_index					= 8
					mesh_property.rotate_point				= "middle"
					mesh_property.direction					= 4
					mesh_property.rotate_axis				= "x+"
					mesh_property.rotate_speed				= -360
					mesh_property.length					= goingLength
					mesh_property.height					= height
					mesh_property.speed						= speed
					mesh_property.property					= property
					
					splited_mesh = MakeSplitedMesh( mesh_property )
					
					splited_mesh.split_count				= 8
				else 
					error( "Invalid finish type " .. finish_type )
				end
				
			elseif monster_type == "insect" then
			
				if finish_type == "normal_finish" then
					local mesh_property						= {}
					mesh_property.meshname					= { "AC", "BD" }
					mesh_property.start_index				= 1
					mesh_property.end_index					= 2
					mesh_property.rotate_point				= "middle"
					mesh_property.direction					= 4
					mesh_property.rotate_axis				= "x+"
					mesh_property.rotate_speed				= -360
					mesh_property.length					= goingLength
					mesh_property.height					= height
					mesh_property.speed						= speed
					mesh_property.property					= property
					
					splited_mesh = MakeSplitedMesh( mesh_property )
					
				elseif finish_type == "big_finish" then
					local mesh_property						= {}
					mesh_property.meshname					= { "AC", "B", "D" }
					mesh_property.start_index				= 1
					mesh_property.end_index					= 3
					mesh_property.rotate_point				= "middle"
					mesh_property.direction					= 4
					mesh_property.rotate_axis				= "x+"
					mesh_property.rotate_speed				= -360
					mesh_property.length					= goingLength
					mesh_property.height					= height
					mesh_property.speed						= speed
					mesh_property.property					= property
					
					splited_mesh = MakeSplitedMesh( mesh_property )
					
				elseif finish_type == "enormous_finish" then
					local mesh_property						= {}
					mesh_property.meshname					= { "A", "C", "B", "D" }
					mesh_property.start_index				= 1
					mesh_property.end_index					= 4
					mesh_property.rotate_point				= "middle"
					mesh_property.direction					= 4
					mesh_property.rotate_axis				= "x+"
					mesh_property.rotate_speed				= -360
					mesh_property.length					= goingLength
					mesh_property.height					= height
					mesh_property.speed						= speed
					mesh_property.property					= property
					
					splited_mesh = MakeSplitedMesh( mesh_property )
				else 
					error( "Invalid finish type" .. finish_type )
				end
				
			elseif monster_type == "head" then
				local mesh_property						= {}
				mesh_property.meshname					= { "A", "B" }
				mesh_property.start_index				= 1
				mesh_property.end_index					= 2
				mesh_property.rotate_point				= "middle"
				mesh_property.direction					= 4
				mesh_property.rotate_axis				= "x+"
				mesh_property.rotate_speed				= -360
				mesh_property.length					= goingLength
				mesh_property.height					= height
				mesh_property.speed						= speed
				mesh_property.property					= property
				
				splited_mesh = MakeSplitedMesh( mesh_property )
				
			else
				error( "Invalid monster type " .. monster_type )
			end
			
		elseif attack_direction == "down_up" then
			if monster_type == "twolimbs" then
				if finish_type == "normal_finish" then
					local mesh_property						= {}
					mesh_property.meshname					= { "I", "J" }
					mesh_property.start_index				= 1
					mesh_property.end_index					= 2
					mesh_property.rotate_point				= "middle"
					mesh_property.direction					= 4
					mesh_property.rotate_axis				= "x+"
					mesh_property.rotate_speed				= -360
					mesh_property.length					= goingLength
					mesh_property.height					= height
					mesh_property.speed						= speed
					mesh_property.property					= property
					
					splited_mesh = MakeSplitedMesh( mesh_property )
					
					splited_mesh.split_count				= 3
					splited_mesh[3]							= {}
					splited_mesh[3].meshname				= "GECABDEH"
					splited_mesh[3].rotate_point			= "middle"
					splited_mesh[3].direction				= 0
					splited_mesh[3].rotate_axis				= "x+"
					splited_mesh[3].rotate_speed			= -360
					splited_mesh[3].length					= goingLength
					splited_mesh[3].height					= height
					splited_mesh[3].speed					= speed
					splited_mesh[3].property				= property
					
				elseif finish_type == "big_finish" then
					local mesh_property						= {}
					mesh_property.meshname					= { "I", "J", "G", "H" }
					mesh_property.start_index				= 1
					mesh_property.end_index					= 4
					mesh_property.rotate_point				= "middle"
					mesh_property.direction					= 4
					mesh_property.rotate_axis				= "x+"
					mesh_property.rotate_speed				= -360
					mesh_property.length					= goingLength
					mesh_property.height					= height
					mesh_property.speed						= speed
					mesh_property.property					= property
					
					splited_mesh = MakeSplitedMesh( mesh_property )
					
					splited_mesh.split_count				= 5
					splited_mesh[5]							= {}
					splited_mesh[5].meshname				= "ECABDF"
					splited_mesh[5].rotate_point			= "middle"
					splited_mesh[5].direction				= 0
					splited_mesh[5].rotate_axis				= "x+"
					splited_mesh[5].rotate_speed			= -360
					splited_mesh[5].length					= goingLength
					splited_mesh[5].height					= height
					splited_mesh[5].speed					= speed
					splited_mesh[5].property				= property
					
				elseif finish_type == "enormous_finish" then
					local mesh_property						= {}
					mesh_property.meshname					= { "I", "J", "G", "H", "E", "F" }
					mesh_property.start_index				= 1
					mesh_property.end_index					= 6
					mesh_property.rotate_point				= "middle"
					mesh_property.direction					= 4
					mesh_property.rotate_axis				= "x+"
					mesh_property.rotate_speed				= -360
					mesh_property.length					= goingLength
					mesh_property.height					= height
					mesh_property.speed						= speed
					mesh_property.property					= property
					
					splited_mesh = MakeSplitedMesh( mesh_property )
					
					splited_mesh.split_count				= 7
					splited_mesh[7]							= {}
					splited_mesh[7].meshname				= "ABCD"
					splited_mesh[7].rotate_point			= "middle"
					splited_mesh[7].direction				= 0
					splited_mesh[7].rotate_axis				= "x+"
					splited_mesh[7].rotate_speed			= -360
					splited_mesh[7].length					= goingLength
					splited_mesh[7].height					= height
					splited_mesh[7].speed					= speed
					splited_mesh[7].property				= property
				else
					error( "Invalid finish type " .. finish_type )
				end
				
			elseif monster_type == "fourlimbs" then
				
				if finish_type == "normal_finish" then
					local mesh_property						= {}
					mesh_property.meshname					= { "A", "B", "C", "D" }
					mesh_property.start_index				= 1
					mesh_property.end_index					= 4
					mesh_property.rotate_point				= "middle"
					mesh_property.direction					= 4
					mesh_property.rotate_axis				= "x+"
					mesh_property.rotate_speed				= -360
					mesh_property.length					= goingLength
					mesh_property.height					= height
					mesh_property.speed						= speed
					mesh_property.property					= property
					
					splited_mesh = MakeSplitedMesh( mesh_property )
					
					splited_mesh.split_count				= 5
					splited_mesh[5]							= {}
					splited_mesh[5].meshname				= "EFIJ"
					splited_mesh[5].rotate_point			= "middle"
					splited_mesh[5].direction				= 0
					splited_mesh[5].rotate_axis				= "x+"
					splited_mesh[5].rotate_speed			= -360
					splited_mesh[5].length					= goingLength
					splited_mesh[5].height					= height
					splited_mesh[5].speed					= speed
					splited_mesh[5].property				= property
					
				elseif finish_type == "big_finish" then
					local mesh_property						= {}
					mesh_property.meshname					= { "A", "B", "C", "D", "E", "F" }
					mesh_property.start_index				= 1
					mesh_property.end_index					= 6
					mesh_property.rotate_point				= "middle"
					mesh_property.direction					= 4
					mesh_property.rotate_axis				= "x+"
					mesh_property.rotate_speed				= -360
					mesh_property.length					= goingLength
					mesh_property.height					= height
					mesh_property.speed						= speed
					mesh_property.property					= property
					
					splited_mesh = MakeSplitedMesh(  mesh_property )
					
					splited_mesh.split_count				= 7
					splited_mesh[7]							= {}
					splited_mesh[7].meshname				= "IJ"
					splited_mesh[7].rotate_point			= "middle"
					splited_mesh[7].direction				= 0
					splited_mesh[7].rotate_axis				= "x+"
					splited_mesh[7].rotate_speed			= -360
					splited_mesh[7].length					= goingLength
					splited_mesh[7].height					= height
					splited_mesh[7].speed					= speed
					splited_mesh[7].property				= property
				
				elseif monster_type == "insect" then
				
					if finish_type == "normal_finish" then
						local mesh_property						= {}
						mesh_property.meshname					= { "AC", "BD" }
						mesh_property.start_index				= 1
						mesh_property.end_index					= 2
						mesh_property.rotate_point				= "middle"
						mesh_property.direction					= 4
						mesh_property.rotate_axis				= "x+"
						mesh_property.rotate_speed				= -360
						mesh_property.length					= goingLength
						mesh_property.height					= height
						mesh_property.speed						= speed
						mesh_property.property					= property
					
						splited_mesh = MakeSplitedMesh(  mesh_property )
					
					elseif finish_type == "big_finish" then
						
						local mesh_property						= {}
						mesh_property.meshname					= { "AC", "B", "D" }
						mesh_property.start_index				= 1
						mesh_property.end_index					= 3
						mesh_property.rotate_point				= "middle"
						mesh_property.direction					= 4
						mesh_property.rotate_axis				= "x+"
						mesh_property.rotate_speed				= -360
						mesh_property.length					= goingLength
						mesh_property.height					= height
						mesh_property.speed						= speed
						mesh_property.property					= property
					
						splited_mesh = MakeSplitedMesh(  mesh_property )
						
					elseif finish_type == "enormous_finish" then
						
						local mesh_property						= {}
						mesh_property.meshname					= { "A", "C", "B", "D" }
						mesh_property.start_index				= 1
						mesh_property.end_index					= 4
						mesh_property.rotate_point				= "middle"
						mesh_property.direction					= 4
						mesh_property.rotate_axis				= "x+"
						mesh_property.rotate_speed				= -360
						mesh_property.length					= goingLength
						mesh_property.height					= height
						mesh_property.speed						= speed
						mesh_property.property					= property
					
						splited_mesh = MakeSplitedMesh(  mesh_property )
						
					else
						error( "Invalid Monster Type " .. monster_type )
					end
					
				elseif monster_type == "head" then
					local mesh_property						= {}
					mesh_property.meshname					= { "A", "B" }
					mesh_property.start_index				= 1
					mesh_property.end_index					= 2
					mesh_property.rotate_point				= "middle"
					mesh_property.direction					= 4
					mesh_property.rotate_axis				= "x+"
					mesh_property.rotate_speed				= -360
					mesh_property.length					= goingLength
					mesh_property.height					= height
					mesh_property.speed						= speed
					mesh_property.property					= property
				
					splited_mesh = MakeSplitedMesh(  mesh_property )
				else
					error( "Invalid finish type" .. finish_type )
				end
			else
				error( "Invalid monster type" .. monster_type )
			end
		
		elseif attack_direction == "left_right" then
			if monster_type == "twolimbs" then
			
				if finish_type == "normal_finish" then
					local mesh_property						= {}
					mesh_property.meshname					= { "A", "B" }
					mesh_property.start_index				= 1
					mesh_property.end_index					= 2
					mesh_property.rotate_point				= "middle"
					mesh_property.direction					= 1
					mesh_property.rotate_axis				= "y-"
					mesh_property.rotate_speed				= -360
					mesh_property.length					= goingLength
					mesh_property.height					= height
					mesh_property.speed						= speed
					mesh_property.property					= property
					
					splited_mesh = MakeSplitedMesh( mesh_property )
					
					splited_mesh.split_count				= 3
					splited_mesh[3]							= {}
					splited_mesh[3].meshname				= "CDEFGHIJ"
					splited_mesh[3].rotate_point			= "middle"
					splited_mesh[3].direction				= 0
					splited_mesh[3].rotate_axis				= "y-"
					splited_mesh[3].rotate_speed			= -360
					splited_mesh[3].length					= goingLength
					splited_mesh[3].height					= height
					splited_mesh[3].speed					= speed
					splited_mesh[3].property				= property
					
				elseif finish_type == "big_finish" then
					local mesh_property						= {}
					mesh_property.meshname					= { "AB", "C", "E", "G" }
					mesh_property.start_index				= 1
					mesh_property.end_index					= 4
					mesh_property.rotate_point				= "middle"
					mesh_property.direction					= 1
					mesh_property.rotate_axis				= "y-"
					mesh_property.rotate_speed				= -360
					mesh_property.length					= goingLength
					mesh_property.height					= height
					mesh_property.speed						= speed
					mesh_property.property					= property
					
					splited_mesh = MakeSplitedMesh( mesh_property )
					
					splited_mesh.split_count				= 5
					splited_mesh[5]							= {}
					splited_mesh[5].meshname				= "DFHJI"
					splited_mesh[5].rotate_point			= "middle"
					splited_mesh[5].direction				= 0
					splited_mesh[5].rotate_axis				= "y-"
					splited_mesh[5].rotate_speed			= -360
					splited_mesh[5].length					= goingLength
					splited_mesh[5].height					= height
					splited_mesh[5].speed					= speed
					splited_mesh[5].property				= property
				
				elseif finish_type == "enormous_finish" then
					local mesh_property						= {}
					mesh_property.meshname					= { "AB", "CD", "E", "F", "G" }
					mesh_property.start_index				= 1
					mesh_property.end_index					= 5
					mesh_property.rotate_point				= "middle"
					mesh_property.direction					= 1
					mesh_property.rotate_axis				= "y-"
					mesh_property.rotate_speed				= -360
					mesh_property.length					= goingLength
					mesh_property.height					= height
					mesh_property.speed						= speed
					mesh_property.property					= property
					
					splited_mesh = MakeSplitedMesh( mesh_property )
					
					splited_mesh.split_count				= 6
					splited_mesh[6]							= {}
					splited_mesh[6].meshname				= "IJH"
					splited_mesh[6].rotate_point			= "middle"
					splited_mesh[6].direction				= 0
					splited_mesh[6].rotate_axis				= "y-"
					splited_mesh[6].rotate_speed			= -360
					splited_mesh[6].length					= goingLength
					splited_mesh[6].height					= height
					splited_mesh[6].speed					= speed
					splited_mesh[6].property				= property
				
				else
					error( "Invalid finish type" .. finish_type )
				end
				
			elseif monster_type == "four_limbs" then
			
				if finish_type == "normal_finish" then
					local mesh_property						= {}
					mesh_property.meshname					= { "AB" }
					mesh_property.start_index				= 1
					mesh_property.end_index					= 1
					mesh_property.rotate_point				= "middle"
					mesh_property.direction					= 1
					mesh_property.rotate_axis				= "y-"
					mesh_property.rotate_speed				= -360
					mesh_property.length					= goingLength
					mesh_property.height					= height
					mesh_property.speed						= speed
					mesh_property.property					= property
					
					splited_mesh = MakeSplitedMesh( mesh_property )
					
					splited_mesh.split_count				= 2
					splited_mesh[2]							= {}
					splited_mesh[2].meshname				= "CDEFIJ"
					splited_mesh[2].rotate_point			= "middle"
					splited_mesh[2].direction				= 0
					splited_mesh[2].rotate_axis				= "y-"
					splited_mesh[2].rotate_speed			= -360
					splited_mesh[2].length					= goingLength
					splited_mesh[2].height					= height
					splited_mesh[2].speed					= speed
					splited_mesh[2].property				= property
					
				elseif finish_type == "big_finish" then
					local mesh_property						= {}
					mesh_property.meshname					= { "AB", "C", "E" }
					mesh_property.start_index				= 1
					mesh_property.end_index					= 3
					mesh_property.rotate_point				= "middle"
					mesh_property.direction					= 1
					mesh_property.rotate_axis				= "y-"
					mesh_property.rotate_speed				= -360
					mesh_property.length					= goingLength
					mesh_property.height					= height
					mesh_property.speed						= speed
					mesh_property.property					= property
					
					splited_mesh = MakeSplitedMesh( mesh_property )
					
					splited_mesh.split_count				= 4
					splited_mesh[4]							= {}
					splited_mesh[4].meshname				= "DFJI"
					splited_mesh[4].rotate_point			= "middle"
					splited_mesh[4].direction				= 0
					splited_mesh[4].rotate_axis				= "y-"
					splited_mesh[4].rotate_speed			= -360
					splited_mesh[4].length					= goingLength
					splited_mesh[4].height					= height
					splited_mesh[4].speed					= speed
					splited_mesh[4].property				= property
					
				elseif finish_type == "enormous_finish" then
					local mesh_property						= {}
					mesh_property.meshname					= { "AB", "C", "E", "DF" }
					mesh_property.start_index				= 1
					mesh_property.end_index					= 4
					mesh_property.rotate_point				= "middle"
					mesh_property.direction					= 1
					mesh_property.rotate_axis				= "y-"
					mesh_property.rotate_speed				= -360
					mesh_property.length					= goingLength
					mesh_property.height					= height
					mesh_property.speed						= speed
					mesh_property.property					= property
					
					splited_mesh = MakeSplitedMesh( mesh_property )
					
					splited_mesh.split_count				= 5
					splited_mesh[5]							= {}
					splited_mesh[5].meshname				= "JI"
					splited_mesh[5].rotate_point			= "middle"
					splited_mesh[5].direction				= 0
					splited_mesh[5].rotate_axis				= "y-"
					splited_mesh[5].rotate_speed			= -360
					splited_mesh[5].length					= goingLength
					splited_mesh[5].height					= height
					splited_mesh[5].speed					= speed
					splited_mesh[5].property				= property
				else
					error( "Invalid finish type" .. finish_type )
				end
				
			elseif monster_type == "insect" then
				
				if finish_type == "normal_finish" then
					local mesh_property						= {}
					mesh_property.meshname					= { "AB", "DC" }
					mesh_property.start_index				= 1
					mesh_property.end_index					= 2
					mesh_property.rotate_point				= "middle"
					mesh_property.direction					= 1
					mesh_property.rotate_axis				= "y-"
					mesh_property.rotate_speed				= -360
					mesh_property.length					= goingLength
					mesh_property.height					= height
					mesh_property.speed						= speed
					mesh_property.property					= property
					
					splited_mesh = MakeSplitedMesh( mesh_property )
					
				elseif finish_type == "big_finish" then
					local mesh_property						= {}
					mesh_property.meshname					= { "AB", "D", "C" }
					mesh_property.start_index				= 1
					mesh_property.end_index					= 3
					mesh_property.rotate_point				= "middle"
					mesh_property.direction					= 1
					mesh_property.rotate_axis				= "y-"
					mesh_property.rotate_speed				= -360
					mesh_property.length					= goingLength
					mesh_property.height					= height
					mesh_property.speed						= speed
					mesh_property.property					= property
					
					splited_mesh = MakeSplitedMesh( mesh_property )
				
				elseif finish_type == "enormouse_finish" then
					local mesh_property						= {}
					mesh_property.meshname					= { "A", "B", "D", "C" }
					mesh_property.start_index				= 1
					mesh_property.end_index					= 4
					mesh_property.rotate_point				= "middle"
					mesh_property.direction					= 1
					mesh_property.rotate_axis				= "y-"
					mesh_property.rotate_speed				= -360
					mesh_property.length					= goingLength
					mesh_property.height					= height
					mesh_property.speed						= speed
					mesh_property.property					= property
					
					splited_mesh = MakeSplitedMesh( mesh_property )
					
				else
					error( "invalid finish type " .. finish_type )
				end
				
			elseif monster_type == "head" then
				local mesh_property						= {}
				mesh_property.meshname					= { "A", "B" }
				mesh_property.start_index				= 1
				mesh_property.end_index					= 2
				mesh_property.rotate_point				= "middle"
				mesh_property.direction					= 1
				mesh_property.rotate_axis				= "y-"
				mesh_property.rotate_speed				= -360
				mesh_property.length					= goingLength
				mesh_property.height					= height
				mesh_property.speed						= speed
				mesh_property.property					= property
				
				splited_mesh = MakeSplitedMesh( mesh_property )
			else
				error( "Invalid monster type" .. monster_type )
			end
			
		elseif attack_direction == "right_left" then
			
			if monster_type == "twolimbs" then
				
				if finish_type == "normal_finish" then
					local mesh_property						= {}
					mesh_property.meshname					= { "A", "B" }
					mesh_property.start_index				= 1
					mesh_property.end_index					= 2
					mesh_property.rotate_point				= "middle"
					mesh_property.direction					= 5
					mesh_property.rotate_axis				= "y+"
					mesh_property.rotate_speed				= -360
					mesh_property.length					= goingLength
					mesh_property.height					= height
					mesh_property.speed						= speed
					mesh_property.property					= property
					
					splited_mesh = MakeSplitedMesh( mesh_property )
					
					splited_mesh.split_count				= 3
					splited_mesh[3]							= {}
					splited_mesh[3].meshname				= "CDEFGHIJ"
					splited_mesh[3].rotate_point			= "middle"
					splited_mesh[3].direction				= 0
					splited_mesh[3].rotate_axis				= "y+"
					splited_mesh[3].rotate_speed			= -360
					splited_mesh[3].length					= goingLength
					splited_mesh[3].height					= height
					splited_mesh[3].speed					= speed
					splited_mesh[3].property				= property
					
				elseif finish_type == "big_finish" then
					local mesh_property						= {}
					mesh_property.meshname					= { "AB", "D", "F", "H" }
					mesh_property.start_index				= 1
					mesh_property.end_index					= 4
					mesh_property.rotate_point				= "middle"
					mesh_property.direction					= 1
					mesh_property.rotate_axis				= "y-"
					mesh_property.rotate_speed				= -360
					mesh_property.length					= goingLength
					mesh_property.height					= height
					mesh_property.speed						= speed
					mesh_property.property					= property
					
					splited_mesh = MakeSplitedMesh( mesh_property )
					
					splited_mesh.split_count				= 5
					splited_mesh[5]							= {}
					splited_mesh[5].meshname				= "CEGIJ"
					splited_mesh[5].rotate_point			= "middle"
					splited_mesh[5].direction				= 0
					splited_mesh[5].rotate_axis				= "y-"
					splited_mesh[5].rotate_speed			= -360
					splited_mesh[5].length					= goingLength
					splited_mesh[5].height					= height
					splited_mesh[5].speed					= speed
					splited_mesh[5].property				= property
				
				elseif finish_type == "enormous_finish" then
					local mesh_property						= {}
					mesh_property.meshname					= { "AB", "CD", "E", "F", "H"}
					mesh_property.start_index				= 1
					mesh_property.end_index					= 5
					mesh_property.rotate_point				= "middle"
					mesh_property.direction					= 1
					mesh_property.rotate_axis				= "y-"
					mesh_property.rotate_speed				= -360
					mesh_property.length					= goingLength
					mesh_property.height					= height
					mesh_property.speed						= speed
					mesh_property.property					= property
					
					splited_mesh = MakeSplitedMesh( mesh_property )
					
					splited_mesh.split_count				= 6
					splited_mesh[6]							= {}
					splited_mesh[6].meshname				= "IJG"
					splited_mesh[6].rotate_point			= "middle"
					splited_mesh[6].direction				= 0
					splited_mesh[6].rotate_axis				= "y-"
					splited_mesh[6].rotate_speed			= -360
					splited_mesh[6].length					= goingLength
					splited_mesh[6].height					= height
					splited_mesh[6].speed					= speed
					splited_mesh[6].property				= property
				else
					error( "invalid finish type " .. finish_type )
				end
				
			elseif monster_type == "fourlimbs" then
				
				if finish_type == "normal_finish" then
					local mesh_property						= {}
					mesh_property.meshname					= { "AB" }
					mesh_property.start_index				= 1
					mesh_property.end_index					= 1
					mesh_property.rotate_point				= "middle"
					mesh_property.direction					= 1
					mesh_property.rotate_axis				= "y-"
					mesh_property.rotate_speed				= -360
					mesh_property.length					= goingLength
					mesh_property.height					= height
					mesh_property.speed						= speed
					mesh_property.property					= property
					
					splited_mesh = MakeSplitedMesh( mesh_property )
					
					splited_mesh.split_count				= 2
					splited_mesh[2]							= {}
					splited_mesh[2].meshname				= "CDEFIJ"
					splited_mesh[2].rotate_point			= "middle"
					splited_mesh[2].direction				= 0
					splited_mesh[2].rotate_axis				= "y-"
					splited_mesh[2].rotate_speed			= -360
					splited_mesh[2].length					= goingLength
					splited_mesh[2].height					= height
					splited_mesh[2].speed					= speed
					splited_mesh[2].property				= property
				
				elseif finish_type == "big_finish" then
					local mesh_property						= {}
					mesh_property.meshname					= { "AB", "D", "F" }
					mesh_property.start_index				= 1
					mesh_property.end_index					= 3
					mesh_property.rotate_point				= "middle"
					mesh_property.direction					= 1
					mesh_property.rotate_axis				= "y-"
					mesh_property.rotate_speed				= -360
					mesh_property.length					= goingLength
					mesh_property.height					= height
					mesh_property.speed						= speed
					mesh_property.property					= property
					
					splited_mesh = MakeSplitedMesh( mesh_property )
					
					splited_mesh.split_count				= 4
					splited_mesh[4]							= {}
					splited_mesh[4].meshname				= "CEJI"
					splited_mesh[4].rotate_point			= "middle"
					splited_mesh[4].direction				= 0
					splited_mesh[4].rotate_axis				= "y-"
					splited_mesh[4].rotate_speed			= -360
					splited_mesh[4].length					= goingLength
					splited_mesh[4].height					= height
					splited_mesh[4].speed					= speed
					splited_mesh[4].property				= property
					
				elseif finish_type == "enormous_finish" then
					local mesh_property						= {}
					mesh_property.meshname					= { "AB", "D", "F", "CE" }
					mesh_property.start_index				= 1
					mesh_property.end_index					= 4
					mesh_property.rotate_point				= "middle"
					mesh_property.direction					= 1
					mesh_property.rotate_axis				= "y-"
					mesh_property.rotate_speed				= -360
					mesh_property.length					= goingLength
					mesh_property.height					= height
					mesh_property.speed						= speed
					mesh_property.property					= property
					
					splited_mesh = MakeSplitedMesh( mesh_property )
					
					splited_mesh.split_count				= 5
					splited_mesh[5]							= {}
					splited_mesh[5].meshname				= "JI"
					splited_mesh[5].rotate_point			= "middle"
					splited_mesh[5].direction				= 0
					splited_mesh[5].rotate_axis				= "y-"
					splited_mesh[5].rotate_speed			= -360
					splited_mesh[5].length					= goingLength
					splited_mesh[5].height					= height
					splited_mesh[5].speed					= speed
					splited_mesh[5].property				= property
				else
					error( "Invalid finish type " .. finish_type )
				end
				
			elseif monster_type == "insect" then
				if finish_type == "normal_finish" then
					local mesh_property						= {}
					mesh_property.meshname					= { "AB", "DC" }
					mesh_property.start_index				= 1
					mesh_property.end_index					= 2
					mesh_property.rotate_point				= "middle"
					mesh_property.direction					= 1
					mesh_property.rotate_axis				= "y-"
					mesh_property.rotate_speed				= -360
					mesh_property.length					= goingLength
					mesh_property.height					= height
					mesh_property.speed						= speed
					mesh_property.property					= property
					
					splited_mesh = MakeSplitedMesh( mesh_property )
				
				elseif finish_type == "big_finish" then
					local mesh_property						= {}
					mesh_property.meshname					= { "AB", "D", "C" }
					mesh_property.start_index				= 1
					mesh_property.end_index					= 3
					mesh_property.rotate_point				= "middle"
					mesh_property.direction					= 1
					mesh_property.rotate_axis				= "y-"
					mesh_property.rotate_speed				= -360
					mesh_property.length					= goingLength
					mesh_property.height					= height
					mesh_property.speed						= speed
					mesh_property.property					= property
					
					splited_mesh = MakeSplitedMesh( mesh_property )
					
				elseif finish_type == "enormous_finish" then
					local mesh_property						= {}
					mesh_property.meshname					= { "A", "B", "D", "C" }
					mesh_property.start_index				= 1
					mesh_property.end_index					= 4
					mesh_property.rotate_point				= "middle"
					mesh_property.direction					= 1
					mesh_property.rotate_axis				= "y-"
					mesh_property.rotate_speed				= -360
					mesh_property.length					= goingLength
					mesh_property.height					= height
					mesh_property.speed						= speed
					mesh_property.property					= property
					
					splited_mesh = MakeSplitedMesh( mesh_property )
					
				else
					error( "invalid finish type " .. finish_type )
				end
				
			elseif monster_type == "head" then
				local mesh_property						= {}
				mesh_property.meshname					= { "A", "B" }
				mesh_property.start_index				= 1
				mesh_property.end_index					= 2
				mesh_property.rotate_point				= "middle"
				mesh_property.direction					= 1
				mesh_property.rotate_axis				= "y-"
				mesh_property.rotate_speed				= -360
				mesh_property.length					= goingLength
				mesh_property.height					= height
				mesh_property.speed						= speed
				mesh_property.property					= property
				
				splited_mesh = MakeSplitedMesh( mesh_property )
						
			else
				error( "Invalid monster type" .. monster_type )
			end
			
		elseif attack_direction == "upleft_downright" then
		
			if monster_type == "twolimbs" then
				
				if finish_type == "normal_finish" then
					local mesh_property						= {}
					mesh_property.meshname					= { "A", "C" }
					mesh_property.start_index				= 1
					mesh_property.end_index					= 2
					mesh_property.rotate_point				= "middle"
					mesh_property.direction					= 4
					mesh_property.rotate_axis				= "z-"
					mesh_property.rotate_speed				= -360
					mesh_property.length					= goingLength
					mesh_property.height					= height
					mesh_property.speed						= speed
					mesh_property.property					= property
					
					splited_mesh = MakeSplitedMesh( mesh_property )
					
					splited_mesh.split_count				= 3
					splited_mesh[3]							= {}
					splited_mesh[3].meshname				= "BDFHJIGE"
					splited_mesh[3].rotate_point			= "middle"
					splited_mesh[3].direction				= 0
					splited_mesh[3].rotate_axis				= "z-"
					splited_mesh[3].rotate_speed			= -360
					splited_mesh[3].length					= goingLength
					splited_mesh[3].height					= height
					splited_mesh[3].speed					= speed
					splited_mesh[3].property				= property
				
				elseif finish_type == "big_finish" then
					local mesh_property						= {}
					mesh_property.meshname					= { "AB", "C", "E", "G" }
					mesh_property.start_index				= 1
					mesh_property.end_index					= 4
					mesh_property.rotate_point				= "middle"
					mesh_property.direction					= 4
					mesh_property.rotate_axis				= "z-"
					mesh_property.rotate_speed				= -360
					mesh_property.length					= goingLength
					mesh_property.height					= height
					mesh_property.speed						= speed
					mesh_property.property					= property
					
					splited_mesh = MakeSplitedMesh( mesh_property )
					
					splited_mesh.split_count				= 5
					splited_mesh[5]							= {}
					splited_mesh[5].meshname				= "DFHJI"
					splited_mesh[5].rotate_point			= "middle"
					splited_mesh[5].direction				= 0
					splited_mesh[5].rotate_axis				= "z-"
					splited_mesh[5].rotate_speed			= -360
					splited_mesh[5].length					= goingLength
					splited_mesh[5].height					= height
					splited_mesh[5].speed					= speed
					splited_mesh[5].property				= property
					
				elseif finish_type == "enormous_finish" then
					local mesh_property						= {}
					mesh_property.meshname					= { "AB", "C", "D", "E", "F", "G" }
					mesh_property.start_index				= 1
					mesh_property.end_index					= 6
					mesh_property.rotate_point				= "middle"
					mesh_property.direction					= 4
					mesh_property.rotate_axis				= "z-"
					mesh_property.rotate_speed				= -360
					mesh_property.length					= goingLength
					mesh_property.height					= height
					mesh_property.speed						= speed
					mesh_property.property					= property
					
					splited_mesh = MakeSplitedMesh( mesh_property )
					
					splited_mesh.split_count				= 7
					splited_mesh[7]							= {}
					splited_mesh[7].meshname				= "HJI"
					splited_mesh[7].rotate_point			= "middle"
					splited_mesh[7].direction				= 0
					splited_mesh[7].rotate_axis				= "z-"
					splited_mesh[7].rotate_speed			= -360
					splited_mesh[7].length					= goingLength
					splited_mesh[7].height					= height
					splited_mesh[7].speed					= speed
					splited_mesh[7].property				= property
				else
					error( "Invalid finish type " .. finish_type )
				end
			
			elseif monster_type == "fourlimbs" then
				
				if finish_type == "normal_finish" then
					local mesh_property						= {}
					mesh_property.meshname					= { "A", "C" }
					mesh_property.start_index				= 1
					mesh_property.end_index					= 2
					mesh_property.rotate_point				= "middle"
					mesh_property.direction					= 4
					mesh_property.rotate_axis				= "z-"
					mesh_property.rotate_speed				= -360
					mesh_property.length					= goingLength
					mesh_property.height					= height
					mesh_property.speed						= speed
					mesh_property.property					= property
					
					splited_mesh = MakeSplitedMesh( mesh_property )
					
					splited_mesh.split_count				= 3
					splited_mesh[3]							= {}
					splited_mesh[3].meshname				= "BDFJIE"
					splited_mesh[3].rotate_point			= "middle"
					splited_mesh[3].direction				= 0
					splited_mesh[3].rotate_axis				= "z-"
					splited_mesh[3].rotate_speed			= -360
					splited_mesh[3].length					= goingLength
					splited_mesh[3].height					= height
					splited_mesh[3].speed					= speed
					splited_mesh[3].property				= property
					
				elseif finish_type == "big_finish" then
					local mesh_property						= {}
					mesh_property.meshname					= { "AB", "D", "F" }
					mesh_property.start_index				= 1
					mesh_property.end_index					= 3
					mesh_property.rotate_point				= "middle"
					mesh_property.direction					= 4
					mesh_property.rotate_axis				= "z-"
					mesh_property.rotate_speed				= -360
					mesh_property.length					= goingLength
					mesh_property.height					= height
					mesh_property.speed						= speed
					mesh_property.property					= property
					
					splited_mesh = MakeSplitedMesh( mesh_property )
					
					splited_mesh.split_count				= 4
					splited_mesh[4]							= {}
					splited_mesh[4].meshname				= "CEJI"
					splited_mesh[4].rotate_point			= "middle"
					splited_mesh[4].direction				= 0
					splited_mesh[4].rotate_axis				= "z-"
					splited_mesh[4].rotate_speed			= -360
					splited_mesh[4].length					= goingLength
					splited_mesh[4].height					= height
					splited_mesh[4].speed					= speed
					splited_mesh[4].property				= property
					
				elseif finish_type == "enormous_finish" then
					local mesh_property						= {}
					mesh_property.meshname					= { "AB", "C", "D", "E", "F" }
					mesh_property.start_index				= 1
					mesh_property.end_index					= 5
					mesh_property.rotate_point				= "middle"
					mesh_property.direction					= 4
					mesh_property.rotate_axis				= "z-"
					mesh_property.rotate_speed				= -360
					mesh_property.length					= goingLength
					mesh_property.height					= height
					mesh_property.speed						= speed
					mesh_property.property					= property
					
					splited_mesh = MakeSplitedMesh( mesh_property )
					
					splited_mesh.split_count				= 6
					splited_mesh[6]							= {}
					splited_mesh[6].meshname				= "JI"
					splited_mesh[6].rotate_point			= "middle"
					splited_mesh[6].direction				= 0
					splited_mesh[6].rotate_axis				= "z-"
					splited_mesh[6].rotate_speed			= -360
					splited_mesh[6].length					= goingLength
					splited_mesh[6].height					= height
					splited_mesh[6].speed					= speed
					splited_mesh[6].property				= property
				else
					error( "Invalid finish type " .. finish_type )
				end
				
			elseif monster_type == "insect" then
			
				if finish_type == "normal_finish" then
					local mesh_property						= {}
					mesh_property.meshname					= { "AC", "BD" }
					mesh_property.start_index				= 1
					mesh_property.end_index					= 2
					mesh_property.rotate_point				= "middle"
					mesh_property.direction					= 4
					mesh_property.rotate_axis				= "z-"
					mesh_property.rotate_speed				= -360
					mesh_property.length					= goingLength
					mesh_property.height					= height
					mesh_property.speed						= speed
					mesh_property.property					= property
					
					splited_mesh = MakeSplitedMesh( mesh_property )
					
				elseif finish_type == "big_finish" then
					local mesh_property						= {}
					mesh_property.meshname					= { "AC", "B", "D" }
					mesh_property.start_index				= 1
					mesh_property.end_index					= 3
					mesh_property.rotate_point				= "middle"
					mesh_property.direction					= 4
					mesh_property.rotate_axis				= "z-"
					mesh_property.rotate_speed				= -360
					mesh_property.length					= goingLength
					mesh_property.height					= height
					mesh_property.speed						= speed
					mesh_property.property					= property
					
					splited_mesh = MakeSplitedMesh( mesh_property )
				
				elseif finish_type == "enormous_finish" then
					local mesh_property						= {}
					mesh_property.meshname					= { "A", "C", "B", "D" }
					mesh_property.start_index				= 1
					mesh_property.end_index					= 4
					mesh_property.rotate_point				= "middle"
					mesh_property.direction					= 4
					mesh_property.rotate_axis				= "z-"
					mesh_property.rotate_speed				= -360
					mesh_property.length					= goingLength
					mesh_property.height					= height
					mesh_property.speed						= speed
					mesh_property.property					= property
					
					splited_mesh = MakeSplitedMesh( mesh_property )
				
				else
					error( "invalid finish type " .. finish_type )
				end
			
				
			elseif monster_type == "head" then
				local mesh_property						= {}
				mesh_property.meshname					= { "A", "B" }
				mesh_property.start_index				= 1
				mesh_property.end_index					= 2
				mesh_property.rotate_point				= "middle"
				mesh_property.direction					= 4
				mesh_property.rotate_axis				= "z-"
				mesh_property.rotate_speed				= -360
				mesh_property.length					= goingLength
				mesh_property.height					= height
				mesh_property.speed						= speed
				mesh_property.property					= property
				
				splited_mesh = MakeSplitedMesh( mesh_property )
			
			else 
				error( "Invalid monster type" .. monster_type )
			end
		
		elseif attack_direction == "upright_downleft" then
			
			if monster_type == "twolimbs" then
				
				if finish_type == "normal_finish" then
					local mesh_property						= {}
					mesh_property.meshname					= { "B", "D" }
					mesh_property.start_index				= 1
					mesh_property.end_index					= 2
					mesh_property.rotate_point				= "middle"
					mesh_property.direction					= 2
					mesh_property.rotate_axis				= "z+"
					mesh_property.rotate_speed				= -360
					mesh_property.length					= goingLength
					mesh_property.height					= height
					mesh_property.speed						= speed
					mesh_property.property					= property
					
					splited_mesh = MakeSplitedMesh( mesh_property )
					
					splited_mesh.split_count				= 3
					splited_mesh[3]							= {}
					splited_mesh[3].meshname				= "ACFHJIGE"
					splited_mesh[3].rotate_point			= "middle"
					splited_mesh[3].direction				= 0
					splited_mesh[3].rotate_axis				= "z+"
					splited_mesh[3].rotate_speed			= -360
					splited_mesh[3].length					= goingLength
					splited_mesh[3].height					= height
					splited_mesh[3].speed					= speed
					splited_mesh[3].property				= property
				
				elseif finish_type == "big_finish" then
					local mesh_property						= {}
					mesh_property.meshname					= { "AB", "D", "F", "H" }
					mesh_property.start_index				= 1
					mesh_property.end_index					= 4
					mesh_property.rotate_point				= "middle"
					mesh_property.direction					= 2
					mesh_property.rotate_axis				= "z+"
					mesh_property.rotate_speed				= -360
					mesh_property.length					= goingLength
					mesh_property.height					= height
					mesh_property.speed						= speed
					mesh_property.property					= property
					
					splited_mesh = MakeSplitedMesh( mesh_property )
					
					splited_mesh.split_count				= 5
					splited_mesh[5]							= {}
					splited_mesh[5].meshname				= "CEGJI"
					splited_mesh[5].rotate_point			= "middle"
					splited_mesh[5].direction				= 0
					splited_mesh[5].rotate_axis				= "z+"
					splited_mesh[5].rotate_speed			= -360
					splited_mesh[5].length					= goingLength
					splited_mesh[5].height					= height
					splited_mesh[5].speed					= speed
					splited_mesh[5].property				= property
					
				elseif finish_type == "enormous_finish" then
					local mesh_property						= {}
					mesh_property.meshname					= { "AB", "C", "D", "E", "F", "H" }
					mesh_property.start_index				= 1
					mesh_property.end_index					= 6
					mesh_property.rotate_point				= "middle"
					mesh_property.direction					= 2
					mesh_property.rotate_axis				= "z+"
					mesh_property.rotate_speed				= -360
					mesh_property.length					= goingLength
					mesh_property.height					= height
					mesh_property.speed						= speed
					mesh_property.property					= property
					
					splited_mesh = MakeSplitedMesh( mesh_property )
					
					splited_mesh.split_count				= 7
					splited_mesh[7]							= {}
					splited_mesh[7].meshname				= "GJI"
					splited_mesh[7].rotate_point			= "middle"
					splited_mesh[7].direction				= 0
					splited_mesh[7].rotate_axis				= "z+"
					splited_mesh[7].rotate_speed			= -360
					splited_mesh[7].length					= goingLength
					splited_mesh[7].height					= height
					splited_mesh[7].speed					= speed
					splited_mesh[7].property				= property
				else
					error( "Invalid finish type " .. finish_type )
				end
				
			elseif monster_type == "fourlimbs" then
			
				if finish_type == "normal_finish" then
					local mesh_property						= {}
					mesh_property.meshname					= { "A", "C" }
					mesh_property.start_index				= 1
					mesh_property.end_index					= 2
					mesh_property.rotate_point				= "middle"
					mesh_property.direction					= 2
					mesh_property.rotate_axis				= "z+"
					mesh_property.rotate_speed				= -360
					mesh_property.length					= goingLength
					mesh_property.height					= height
					mesh_property.speed						= speed
					mesh_property.property					= property
					
					splited_mesh = MakeSplitedMesh( mesh_property )
					
					splited_mesh.split_count				= 3
					splited_mesh[3]							= {}
					splited_mesh[3].meshname				= "BDFJIE"
					splited_mesh[3].rotate_point			= "middle"
					splited_mesh[3].direction				= 0
					splited_mesh[3].rotate_axis				= "z+"
					splited_mesh[3].rotate_speed			= -360
					splited_mesh[3].length					= goingLength
					splited_mesh[3].height					= height
					splited_mesh[3].speed					= speed
					splited_mesh[3].property				= property
					
				elseif finish_type == "big_finish" then
					local mesh_property						= {}
					mesh_property.meshname					= { "AB", "C", "E" }
					mesh_property.start_index				= 1
					mesh_property.end_index					= 3
					mesh_property.rotate_point				= "middle"
					mesh_property.direction					= 2
					mesh_property.rotate_axis				= "z+"
					mesh_property.rotate_speed				= -360
					mesh_property.length					= goingLength
					mesh_property.height					= height
					mesh_property.speed						= speed
					mesh_property.property					= property
					
					splited_mesh = MakeSplitedMesh( mesh_property )
					
					splited_mesh.split_count				= 4
					splited_mesh[4]							= {}
					splited_mesh[4].meshname				= "DFJI"
					splited_mesh[4].rotate_point			= "middle"
					splited_mesh[4].direction				= 0
					splited_mesh[4].rotate_axis				= "z+"
					splited_mesh[4].rotate_speed			= -360
					splited_mesh[4].length					= goingLength
					splited_mesh[4].height					= height
					splited_mesh[4].speed					= speed
					splited_mesh[4].property				= property
					
				elseif finish_type == "enormous_finish" then
					local mesh_property						= {}
					mesh_property.meshname					= { "AB", "C", "D", "E", "F" }
					mesh_property.start_index				= 1
					mesh_property.end_index					= 5
					mesh_property.rotate_point				= "middle"
					mesh_property.direction					= 2
					mesh_property.rotate_axis				= "z+"
					mesh_property.rotate_speed				= -360
					mesh_property.length					= goingLength
					mesh_property.height					= height
					mesh_property.speed						= speed
					mesh_property.property					= property
					
					splited_mesh = MakeSplitedMesh( mesh_property )
					
					splited_mesh.split_count				= 6
					splited_mesh[6]							= {}
					splited_mesh[6].meshname				= "JI"
					splited_mesh[6].rotate_point			= "middle"
					splited_mesh[6].direction				= 0
					splited_mesh[6].rotate_axis				= "z+"
					splited_mesh[6].rotate_speed			= -360
					splited_mesh[6].length					= goingLength
					splited_mesh[6].height					= height
					splited_mesh[6].speed					= speed
					splited_mesh[6].property				= property
				
				else 
					error( "Invalid finish type " .. finish_type )
				end
			
			elseif monster_type == "insect" then
			
				if finish_type == "normal_finish" then
					local mesh_property						= {}
					mesh_property.meshname					= { "AC", "BD" }
					mesh_property.start_index				= 1
					mesh_property.end_index					= 2
					mesh_property.rotate_point				= "middle"
					mesh_property.direction					= 2
					mesh_property.rotate_axis				= "z+"
					mesh_property.rotate_speed				= -360
					mesh_property.length					= goingLength
					mesh_property.height					= height
					mesh_property.speed						= speed
					mesh_property.property					= property
					
					splited_mesh = MakeSplitedMesh( mesh_property )
					
				elseif finish_type == "big_finish" then
					local mesh_property						= {}
					mesh_property.meshname					= { "AC", "B", "D" }
					mesh_property.start_index				= 1
					mesh_property.end_index					= 3
					mesh_property.rotate_point				= "middle"
					mesh_property.direction					= 2
					mesh_property.rotate_axis				= "z+"
					mesh_property.rotate_speed				= -360
					mesh_property.length					= goingLength
					mesh_property.height					= height
					mesh_property.speed						= speed
					mesh_property.property					= property
					
					splited_mesh = MakeSplitedMesh( mesh_property )
					
				elseif finish_type == "enormous_finish" then
					local mesh_property						= {}
					mesh_property.meshname					= { "A", "C", "B", "D" }
					mesh_property.start_index				= 1
					mesh_property.end_index					= 4
					mesh_property.rotate_point				= "middle"
					mesh_property.direction					= 2
					mesh_property.rotate_axis				= "z+"
					mesh_property.rotate_speed				= -360
					mesh_property.length					= goingLength
					mesh_property.height					= height
					mesh_property.speed						= speed
					mesh_property.property					= property
					
					splited_mesh = MakeSplitedMesh( mesh_property )
					
				else
					error( "invalid finish type " .. finish_type )
				end
			
			elseif monster_type == "head" then
				local mesh_property						= {}
				mesh_property.meshname					= { "A", "B" }
				mesh_property.start_index				= 1
				mesh_property.end_index					= 2
				mesh_property.rotate_point				= "middle"
				mesh_property.direction					= 2
				mesh_property.rotate_axis				= "z+"
				mesh_property.rotate_speed				= -360
				mesh_property.length					= goingLength
				mesh_property.height					= height
				mesh_property.speed						= speed
				mesh_property.property					= property
				
				splited_mesh = MakeSplitedMesh( mesh_property )
			
			else
				error( "Invalid monster type " .. monster_type )
			end
		
		elseif attack_direction == "downleft_upright" then
			
			if monster_type == "twolimbs" then
				
				if finish_type == "normal_finish" then
					local mesh_property						= {}
					mesh_property.meshname					= { "G" }
					mesh_property.start_index				= 1
					mesh_property.end_index					= 1
					mesh_property.rotate_point				= "middle"
					mesh_property.direction					= 2
					mesh_property.rotate_axis				= "z+"
					mesh_property.rotate_speed				= -360
					mesh_property.length					= goingLength
					mesh_property.height					= height
					mesh_property.speed						= speed
					mesh_property.property					= property
					
					splited_mesh = MakeSplitedMesh( mesh_property )
					
					splited_mesh.split_count				= 2
					splited_mesh[2]							= {}
					splited_mesh[2].meshname				= "ABCDEFHIJ"
					splited_mesh[2].rotate_point			= "middle"
					splited_mesh[2].direction				= 0
					splited_mesh[2].rotate_axis				= "z+"
					splited_mesh[2].rotate_speed			= -360
					splited_mesh[2].length					= goingLength
					splited_mesh[2].height					= height
					splited_mesh[2].speed					= speed
					splited_mesh[2].property				= property
				
				elseif finish_type == "big_finish" then
					local mesh_property						= {}
					mesh_property.meshname					= { "G", "E", "C" }
					mesh_property.start_index				= 1
					mesh_property.end_index					= 3
					mesh_property.rotate_point				= "middle"
					mesh_property.direction					= 2
					mesh_property.rotate_axis				= "z+"
					mesh_property.rotate_speed				= -360
					mesh_property.length					= goingLength
					mesh_property.height					= height
					mesh_property.speed						= speed
					mesh_property.property					= property
					
					splited_mesh = MakeSplitedMesh( mesh_property )
					
					splited_mesh.split_count				= 4
					splited_mesh[4]							= {}
					splited_mesh[4].meshname				= "ABDFHIJ"
					splited_mesh[4].rotate_point			= "middle"
					splited_mesh[4].direction				= 0
					splited_mesh[4].rotate_axis				= "z+"
					splited_mesh[4].rotate_speed			= -360
					splited_mesh[4].length					= goingLength
					splited_mesh[4].height					= height
					splited_mesh[4].speed					= speed
					splited_mesh[4].property				= property
					
				elseif finish_type == "enormous_finish" then
					local mesh_property						= {}
					mesh_property.meshname					= { "G", "E", "C", "AB", "D" }
					mesh_property.start_index				= 1
					mesh_property.end_index					= 5
					mesh_property.rotate_point				= "middle"
					mesh_property.direction					= 2
					mesh_property.rotate_axis				= "z+"
					mesh_property.rotate_speed				= -360
					mesh_property.length					= goingLength
					mesh_property.height					= height
					mesh_property.speed						= speed
					mesh_property.property					= property
					
					splited_mesh = MakeSplitedMesh( mesh_property )
					
					splited_mesh.split_count				= 6
					splited_mesh[6]							= {}
					splited_mesh[6].meshname				= "HIJ"
					splited_mesh[6].rotate_point			= "middle"
					splited_mesh[6].direction				= 0
					splited_mesh[6].rotate_axis				= "z+"
					splited_mesh[6].rotate_speed			= -360
					splited_mesh[6].length					= goingLength
					splited_mesh[6].height					= height
					splited_mesh[6].speed					= speed
					splited_mesh[6].property				= property
					
				else	
					error( "Invalid finish type " .. finish_type )
				end
				
			elseif monster_type == "fourlimbs" then
				
				if finish_type == "normal_finish" then
					local mesh_property						= {}
					mesh_property.meshname					= { "E" }
					mesh_property.start_index				= 1
					mesh_property.end_index					= 1
					mesh_property.rotate_point				= "middle"
					mesh_property.direction					= 2
					mesh_property.rotate_axis				= "z+"
					mesh_property.rotate_speed				= -360
					mesh_property.length					= goingLength
					mesh_property.height					= height
					mesh_property.speed						= speed
					mesh_property.property					= property
					
					splited_mesh = MakeSplitedMesh( mesh_property )
					
					splited_mesh.split_count				= 2
					splited_mesh[2]							= {}
					splited_mesh[2].meshname				= "ABCDFIJ"
					splited_mesh[2].rotate_point			= "middle"
					splited_mesh[2].direction				= 0
					splited_mesh[2].rotate_axis				= "z+"
					splited_mesh[2].rotate_speed			= -360
					splited_mesh[2].length					= goingLength
					splited_mesh[2].height					= height
					splited_mesh[2].speed					= speed
					splited_mesh[2].property				= property
					
				elseif finish_type == "big_finish" then
					local mesh_property						= {}
					mesh_property.meshname					= { "E", "C", "A" }
					mesh_property.start_index				= 1
					mesh_property.end_index					= 3
					mesh_property.rotate_point				= "middle"
					mesh_property.direction					= 2
					mesh_property.rotate_axis				= "z+"
					mesh_property.rotate_speed				= -360
					mesh_property.length					= goingLength
					mesh_property.height					= height
					mesh_property.speed						= speed
					mesh_property.property					= property
					
					splited_mesh = MakeSplitedMesh( mesh_property )
					
					splited_mesh.split_count				= 4
					splited_mesh[4]							= {}
					splited_mesh[4].meshname				= "BDFJI"
					splited_mesh[4].rotate_point			= "middle"
					splited_mesh[4].direction				= 0
					splited_mesh[4].rotate_axis				= "z+"
					splited_mesh[4].rotate_speed			= -360
					splited_mesh[4].length					= goingLength
					splited_mesh[4].height					= height
					splited_mesh[4].speed					= speed
					splited_mesh[4].property				= property
				
				elseif finish_type == "enormous_finish" then
					local mesh_property						= {}
					mesh_property.meshname					= { "E", "C", "AB", "D", "F" }
					mesh_property.start_index				= 1
					mesh_property.end_index					= 5
					mesh_property.rotate_point				= "middle"
					mesh_property.direction					= 2
					mesh_property.rotate_axis				= "z+"
					mesh_property.rotate_speed				= -360
					mesh_property.length					= goingLength
					mesh_property.height					= height
					mesh_property.speed						= speed
					mesh_property.property					= property
					
					splited_mesh = MakeSplitedMesh( mesh_property )
					
					splited_mesh.split_count				= 6
					splited_mesh[6]							= {}
					splited_mesh[6].meshname				= "JI"
					splited_mesh[6].rotate_point			= "middle"
					splited_mesh[6].direction				= 0
					splited_mesh[6].rotate_axis				= "z+"
					splited_mesh[6].rotate_speed			= -360
					splited_mesh[6].length					= goingLength
					splited_mesh[6].height					= height
					splited_mesh[6].speed					= speed
					splited_mesh[6].property				= property
					
				else 
					error( "Invalid finish type " .. finish_type )
				end
				
			elseif monster_type == "insect" then
			
				if finish_type == "normal_finish" then
					local mesh_property						= {}
					mesh_property.meshname					= { "AC", "BD" }
					mesh_property.start_index				= 1
					mesh_property.end_index					= 2
					mesh_property.rotate_point				= "middle"
					mesh_property.direction					= 2
					mesh_property.rotate_axis				= "z+"
					mesh_property.rotate_speed				= -360
					mesh_property.length					= goingLength
					mesh_property.height					= height
					mesh_property.speed						= speed
					mesh_property.property					= property
					
					splited_mesh = MakeSplitedMesh( mesh_property )
					
				elseif finish_type == "big_finish" then
					local mesh_property						= {}
					mesh_property.meshname					= { "AC", "B", "D" }
					mesh_property.start_index				= 1
					mesh_property.end_index					= 3
					mesh_property.rotate_point				= "middle"
					mesh_property.direction					= 2
					mesh_property.rotate_axis				= "z+"
					mesh_property.rotate_speed				= -360
					mesh_property.length					= goingLength
					mesh_property.height					= height
					mesh_property.speed						= speed
					mesh_property.property					= property
					
					splited_mesh = MakeSplitedMesh( mesh_property )
				
				elseif finish_type == "enormous_finish" then
					local mesh_property						= {}
					mesh_property.meshname					= { "A", "C", "B", "D" }
					mesh_property.start_index				= 1
					mesh_property.end_index					= 4
					mesh_property.rotate_point				= "middle"
					mesh_property.direction					= 2
					mesh_property.rotate_axis				= "z+"
					mesh_property.rotate_speed				= -360
					mesh_property.length					= goingLength
					mesh_property.height					= height
					mesh_property.speed						= speed
					mesh_property.property					= property
					
					splited_mesh = MakeSplitedMesh( mesh_property )
				
				else
					error( "invalid finish type " .. finish_type )
				end
			
			elseif monster_type == "head" then
				local mesh_property						= {}
				mesh_property.meshname					= { "A", "B" }
				mesh_property.start_index				= 1
				mesh_property.end_index					= 2
				mesh_property.rotate_point				= "middle"
				mesh_property.direction					= 2
				mesh_property.rotate_axis				= "z+"
				mesh_property.rotate_speed				= -360
				mesh_property.length					= goingLength
				mesh_property.height					= height
				mesh_property.speed						= speed
				mesh_property.property					= property
				
				splited_mesh = MakeSplitedMesh( mesh_property )
				
			else
				error( "Invalid monster type " .. monster_type )
			end
		
		elseif attack_direction == "downright_upleft" then
			
			if monster_type == "twolimbs" then
				
				if finish_type == "normal_finish" then
					local mesh_property						= {}
					mesh_property.meshname					= { "H" }
					mesh_property.start_index				= 1
					mesh_property.end_index					= 1
					mesh_property.rotate_point				= "middle"
					mesh_property.direction					= 4
					mesh_property.rotate_axis				= "z-"
					mesh_property.rotate_speed				= -360
					mesh_property.length					= goingLength
					mesh_property.height					= height
					mesh_property.speed						= speed
					mesh_property.property					= property
					
					splited_mesh = MakeSplitedMesh( mesh_property )
					
					splited_mesh.split_count				= 2
					splited_mesh[2]							= {}
					splited_mesh[2].meshname				= "ABCDEFGIJ"
					splited_mesh[2].rotate_point			= "middle"
					splited_mesh[2].direction				= 0
					splited_mesh[2].rotate_axis				= "z-"
					splited_mesh[2].rotate_speed			= -360
					splited_mesh[2].length					= goingLength
					splited_mesh[2].height					= height
					splited_mesh[2].speed					= speed
					splited_mesh[2].property				= property
					
				elseif finish_type == "big_finish" then
					local mesh_property						= {}
					mesh_property.meshname					= { "H", "F", "D" }
					mesh_property.start_index				= 1
					mesh_property.end_index					= 3
					mesh_property.rotate_point				= "middle"
					mesh_property.direction					= 4
					mesh_property.rotate_axis				= "z-"
					mesh_property.rotate_speed				= -360
					mesh_property.length					= goingLength
					mesh_property.height					= height
					mesh_property.speed						= speed
					mesh_property.property					= property
					
					splited_mesh = MakeSplitedMesh( mesh_property )
					
					splited_mesh.split_count				= 4
					splited_mesh[4]							= {}
					splited_mesh[4].meshname				= "ABCEGIJ"
					splited_mesh[4].rotate_point			= "middle"
					splited_mesh[4].direction				= 0
					splited_mesh[4].rotate_axis				= "z-"
					splited_mesh[4].rotate_speed			= -360
					splited_mesh[4].length					= goingLength
					splited_mesh[4].height					= height
					splited_mesh[4].speed					= speed
					splited_mesh[4].property				= property
				
				elseif finish_type == "enormous_finish" then
					local mesh_property						= {}
					mesh_property.meshname					= { "H", "F", "D", "AB", "C" }
					mesh_property.start_index				= 1
					mesh_property.end_index					= 5
					mesh_property.rotate_point				= "middle"
					mesh_property.direction					= 4
					mesh_property.rotate_axis				= "z-"
					mesh_property.rotate_speed				= -360
					mesh_property.length					= goingLength
					mesh_property.height					= height
					mesh_property.speed						= speed
					mesh_property.property					= property
					
					splited_mesh = MakeSplitedMesh( mesh_property )
					
					splited_mesh.split_count				= 6
					splited_mesh[6]							= {}
					splited_mesh[6].meshname				= "GIJ"
					splited_mesh[6].rotate_point			= "middle"
					splited_mesh[6].direction				= 0
					splited_mesh[6].rotate_axis				= "z-"
					splited_mesh[6].rotate_speed			= -360
					splited_mesh[6].length					= goingLength
					splited_mesh[6].height					= height
					splited_mesh[6].speed					= speed
					splited_mesh[6].property				= property
					
				else
					error( "Invalid finish type" .. finish_type )
				end
				
			elseif monster_type == "fourlimbs" then
				
				if finish_type == "normal_finish" then
					local mesh_property						= {}
					mesh_property.meshname					= { "F" }
					mesh_property.start_index				= 1
					mesh_property.end_index					= 1
					mesh_property.rotate_point				= "middle"
					mesh_property.direction					= 4
					mesh_property.rotate_axis				= "z-"
					mesh_property.rotate_speed				= -360
					mesh_property.length					= goingLength
					mesh_property.height					= height
					mesh_property.speed						= speed
					mesh_property.property					= property
					
					splited_mesh = MakeSplitedMesh( mesh_property )
					
					splited_mesh.split_count				= 2
					splited_mesh[2]							= {}
					splited_mesh[2].meshname				= "ABCDEIJ"
					splited_mesh[2].rotate_point			= "middle"
					splited_mesh[2].direction				= 0
					splited_mesh[2].rotate_axis				= "z-"
					splited_mesh[2].rotate_speed			= -360
					splited_mesh[2].length					= goingLength
					splited_mesh[2].height					= height
					splited_mesh[2].speed					= speed
					splited_mesh[2].property				= property
				
				elseif finish_type == "big_finish" then
					local mesh_property						= {}
					mesh_property.meshname					= { "F", "D", "B" }
					mesh_property.start_index				= 1
					mesh_property.end_index					= 3
					mesh_property.rotate_point				= "middle"
					mesh_property.direction					= 4
					mesh_property.rotate_axis				= "z-"
					mesh_property.rotate_speed				= -360
					mesh_property.length					= goingLength
					mesh_property.height					= height
					mesh_property.speed						= speed
					mesh_property.property					= property
					
					splited_mesh = MakeSplitedMesh( mesh_property )
					
					splited_mesh.split_count				= 4
					splited_mesh[4]							= {}
					splited_mesh[4].meshname				= "ACEIJ"
					splited_mesh[4].rotate_point			= "middle"
					splited_mesh[4].direction				= 0
					splited_mesh[4].rotate_axis				= "z-"
					splited_mesh[4].rotate_speed			= -360
					splited_mesh[4].length					= goingLength
					splited_mesh[4].height					= height
					splited_mesh[4].speed					= speed
					splited_mesh[4].property				= property
					
				elseif finish_type == "enormous_finish" then
					local mesh_property						= {}
					mesh_property.meshname					= { "E", "C", "AB", "D", "F" }
					mesh_property.start_index				= 1
					mesh_property.end_index					= 5
					mesh_property.rotate_point				= "middle"
					mesh_property.direction					= 4
					mesh_property.rotate_axis				= "z-"
					mesh_property.rotate_speed				= -360
					mesh_property.length					= goingLength
					mesh_property.height					= height
					mesh_property.speed						= speed
					mesh_property.property					= property
					
					splited_mesh = MakeSplitedMesh( mesh_property )
					
					splited_mesh.split_count				= 6
					splited_mesh[6]							= {}
					splited_mesh[6].meshname				= "IJ"
					splited_mesh[6].rotate_point			= "middle"
					splited_mesh[6].direction				= 0
					splited_mesh[6].rotate_axis				= "z-"
					splited_mesh[6].rotate_speed			= -360
					splited_mesh[6].length					= goingLength
					splited_mesh[6].height					= height
					splited_mesh[6].speed					= speed
					splited_mesh[6].property				= property
				
				else
					error( "Invalid finish type " .. finish_type )
				end
				
			elseif monster_type == "insect" then
				
				if finish_type == "normal_finish" then
					local mesh_property						= {}
					mesh_property.meshname					= { "AC", "BD" }
					mesh_property.start_index				= 1
					mesh_property.end_index					= 2
					mesh_property.rotate_point				= "middle"
					mesh_property.direction					= 4
					mesh_property.rotate_axis				= "z-"
					mesh_property.rotate_speed				= -360
					mesh_property.length					= goingLength
					mesh_property.height					= height
					mesh_property.speed						= speed
					mesh_property.property					= property
					
					splited_mesh = MakeSplitedMesh( mesh_property )
					
				elseif finish_type == "big_finish" then
					local mesh_property						= {}
					mesh_property.meshname					= { "AC", "B", "D" }
					mesh_property.start_index				= 1
					mesh_property.end_index					= 3
					mesh_property.rotate_point				= "middle"
					mesh_property.direction					= 4
					mesh_property.rotate_axis				= "z-"
					mesh_property.rotate_speed				= -360
					mesh_property.length					= goingLength
					mesh_property.height					= height
					mesh_property.speed						= speed
					mesh_property.property					= property
					
					splited_mesh = MakeSplitedMesh( mesh_property )
				
				elseif finish_type == "enormous_finish" then
					local mesh_property						= {}
					mesh_property.meshname					= { "A", "C", "B", "D" }
					mesh_property.start_index				= 1
					mesh_property.end_index					= 4
					mesh_property.rotate_point				= "middle"
					mesh_property.direction					= 4
					mesh_property.rotate_axis				= "z-"
					mesh_property.rotate_speed				= -360
					mesh_property.length					= goingLength
					mesh_property.height					= height
					mesh_property.speed						= speed
					mesh_property.property					= property
					
					splited_mesh = MakeSplitedMesh( mesh_property )
				
				else
					error( "invliad finish type " .. finish_type )
				end
			
			elseif monster_type == "head" then
				local mesh_property						= {}
				mesh_property.meshname					= { "A", "B" }
				mesh_property.start_index				= 1
				mesh_property.end_index					= 2
				mesh_property.rotate_point				= "middle"
				mesh_property.direction					= 4
				mesh_property.rotate_axis				= "z-"
				mesh_property.rotate_speed				= -360
				mesh_property.length					= goingLength
				mesh_property.height					= height
				mesh_property.speed						= speed
				mesh_property.property					= property
				
				splited_mesh = MakeSplitedMesh( mesh_property )
				
			else
				error( "Invalid monster type" .. monster_type )
			end
		else
			error( "Invalid Attack Dir" .. attack_direction )
		end
		
	else
		error( "Invalid attack type" .. attack_type )
	end
	
	return splited_mesh
end

function MakeSplitedMesh( mesh_property )
	local mesh = {}
	
	local count = 0
	
	for index = mesh_property.start_index, mesh_property.end_index do
		mesh[ index ]		= {}
		
		mesh[index].meshname		= mesh_property.meshname[ index ]
		mesh[index].rotate_point	= mesh_property.rotate_point
		mesh[index].direction		= mesh_property.direction
		mesh[index].rotate_axis	= mesh_property.rotate_axis
		mesh[index].rotate_speed	= mesh_property.rotate_speed
		mesh[index].length			= mesh_property.length
		mesh[index].height			= mesh_property.height
		mesh[index].speed			= mesh_property.speed
		mesh[index].property		= mesh_property.property
		count						= count + 1
	end
	
	mesh.split_count				= count;
	
	return mesh
end
		
							

function GetLength( damage, total_hp, weapon_class )

	local length_ratio	= 0
	
	if 	weapon_class 		== "short_sword" or 
		weapon_class 		== "sword" or 
		weapon_class 		== "long_sword" or
		weapon_class 		== "great_sword"  	then
		
		length_ratio	= 0	
		
	elseif	weapon_class	== "spear" 	then
	
		length_ratio	= 30
		
	elseif	weapon_class	== "mace" or 
			weapon_class	== "axe" or
			weapon_class	== "wand"	then
			
		length_ratio	= 40
		
	elseif	weapon_class	== "bow" or
			weapon_class	== "cross_bow"	then
			
		length_ratio	= 30
		
	elseif weapon_class	== "magic" then
		
		length_ratio	= 40
		
	else error( "Invalid weapon class" ) end
		 
	
	local goingLength		= (damage / total_hp) * (50 + length_ratio) * 0.1
	
	if goingLength > 3 then 
		goingLength = 3 
	end
	
	return goingLength
end

function GetSpeed( going_length )
	-- 5 보다 멀리 날아가면, 무조건 0.5초간 체공
	if going_length > 5 then
		return going_length / 0.5
	else
	-- 그외의 경우에는 0.3초에서 0.5초 사이로 체공
		local flyTime = 0.04 * going_length + 0.3
		return going_length / flyTime
	end
end

function GetHeight( going_length )
	return ( going_length / 5 ) + 0.5
end

function GetProperty( finish_type )
	local property
	
	if finish_type == "normal_finish" then
		property	= "preserve"
	elseif finish_type == "big_finish" then
		property	= "reaction"
	elseif finish_type == "enormous_finish" then
		property	= "break"
	else
		error("Invalud finish type")
	end
	
	return property
end

--[[
-----------------------------
-- input 형태
-- attack_direction: 	up_down, down_up, right_left, upright_downleft, downright_upleft,
-- 						left_right, upleft_downright, downleft_upright

-- attack_type:			strike, cut

-- weapon_class:		short_sword, sword, long_sword, great_sword,
--						spear,
--						mace, axe, wand,
--						bow, cross_bow, magic

-- monster_type:		twolimbs, fourlimbs

-- finish_type:			normal_finish, big_finish, enormous_finish

splited_mesh = SplitMesh( "down_up", "strike", 100, "normal_finish", 500, "sword", "twolimbs" )

for i,v in ipairs( splited_mesh ) do
	print("-----------------------------------------------")
	print( "Key: " .. i )
	for index, value in pairs( v ) do
		print( index, value )
	end
end
--]]