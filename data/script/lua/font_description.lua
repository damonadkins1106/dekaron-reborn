-- define font description
font_description = {	
-- 1
	{
		height			= 10,
		width			= 0,
		weight			= 400,
		shadow			= false,
		facename		= "MS Sans Serif"
	},		
-- 2
	{
		height			= 10,
		width			= 0,
		weight			= 700,
		shadow			= false,
		facename		= "MS Sans Serif"
	},		
-- 3
	{ 	height 			= 13,
		width			= 0,
		weight			= 400,
		shadow			= false,
		facename		= "MS Sans Serif"
	},
-- 4
	{	height			= 13,
		width			= 0,
		weight			= 400,
		shadow			= false,
		facename		= "MS Sans Serif"
	},
-- 5
	{	height			= 13,
		width			= 0,
		weight			= 700,
		shadow			= false,
		facename		= "MS Sans Serif"
	},
-- 6
	{	height			= 13,
		width			= 0,
		weight			= 700,
		shadow			= true,
		facename		= "MS Sans Serif"
	},
-- 7
	{	height			= 16,
		width			= 0,
		weight			= 400,
		shadow			= false,
		facename		= "MS Sans Serif"
	},
-- 8
	{	height			= 16,
		width			= 0,
		weight			= 700,
		shadow			= false,
		facename		= "MS Sans Serif"
	},
-- 9
	{	height			= 18,
		width			= 0,
		weight			= 900,
		shadow			= false,
		facename		= "MS Sans Serif"
	},
-- 10
	{	height			= 18,
		width			= 0,
		weight			= 900,
		shadow			= false,
		facename		= "MS Sans Serif"
	},
-- 11
	{	height			= 20,
		width			= 0,
		weight			= 400,
		shadow			= false,
		facename		= "MS Sans Serif"
	},
-- 12
	{	height			= 20,
		width			= 0,
		weight			= 700,
		shadow			= false,
		facename		= "MS Sans Serif"
	},
-- 13
	{	height			= 24,
		width			= 0,
		weight			= 400,
		shadow			= false,
		facename		= "MS Sans Serif"
	},
-- 14
	{	height			= 24,
		width			= 0,
		weight			= 700,
		shadow			= false,
		facename		= "MS Sans Serif"
	},
-- 15
	{	height			= 18,
		width			= 0,
		weight			= 700,
		shadow			= true,
		facename		= "MS Sans Serif"
	}
}