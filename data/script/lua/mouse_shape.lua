mouse_shape = 
{
	{
		name			= "attack", 
		mouseres		= "mouse\\cursorattack.tpack"
	},
	{
		name			= "grip",
		mouseres		= "mouse\\cursorgrip.tpack"
	},
	{
		name			= "restrict",
		mouseres		= "mouse\\restrict.tpack"
	},
	{
		name			= "talk",
		mouseres		= "mouse\\cursortalk.tpack"
	},
	{
		name			= "target",
		mouseres		= "mouse\\target.tpack"
	},
	{
		name			= "input",
		mouseres		= "mouse\\input.tpack"
	},
	{
		name			= "servantnormal",
		mouseres		= "mouse\\servantnormal.tpack"
	},
	{
		name			= "servantrestrict",
		mouseres		= "mouse\\servantrestrict.tpack"
	},
	{
		name			= "servantattack", 
		mouseres		= "mouse\\servantattack.tpack"
	},
	{
		name			= "forceattack", 
		mouseres		= "mouse\\forceattack.tpack"
	},
	{
		name			= "fishing", 
		mouseres		= "mouse\\fishing.tpack"
	},
	{
		name			= "unfishing", 
		mouseres		= "mouse\\unfishing.tpack"
	},
	{
		name			= "minimap", 
		mouseres		= "mouse\\minimap.tpack"
	},
	{
		name			= "normal",
		mouseres		= "mouse\\normal.tpack"
	}
}

--[[
function get_mouseshape( current_state )

	for i, v in ipairs( mouse_shape ) do
		if v.name	== current_state then
			return v.texture
		end		
	end		
	
	error("invalid mouse state" .. current_state )
end
--]]