ChatType = 
{
	-- 일반
	{
		shortcut			= "/일반",
		ChatType			= 0
	},
	{
		shortcut			= "/일",
		ChatType			= 0
	},
	-- 외치기
	{
		shortcut			= "/외치기",
		ChatType			= 1
	},
	{
		shortcut			= "/외",
		ChatType			= 1
	},
	-- 귓속말
	{
		shortcut			= "/귓속말",
		ChatType			= 3
	},
	{
		shortcut			= "/귓",
		ChatType			= 3
	},
	-- 파티
	{
		shortcut			= "/파티",
		ChatType			= 4
	},
	{
		shortcut			= "/파",
		ChatType			= 4
	},
	-- 원정대
	{
		shortcut			= "/원정대",
		ChatType			= 4
	},
	{
		shortcut			= "/원",
		ChatType			= 4
	},
	-- 길드
	{
		shortcut			= "/길드",
		ChatType			= 7
	},
	{
		shortcut			= "/길",
		ChatType			= 7
	},
	-- 길드임원
	{
		shortcut			= "/임원",
		ChatType			= 10
	},
	{
		shortcut			= "/임",
		ChatType			= 10
	},

}	

