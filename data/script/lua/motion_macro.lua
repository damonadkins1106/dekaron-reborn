emotimotion = 
{
	-- 10
	{
		shortcut			= "/yo",
		motion				= 10
	},
	
	-- 11
	{	
		shortcut			= "/hi",
		motion				= 11
	},
	-- 12
	{	
		shortcut			= "/rawr",
		motion				= 15
	},
	
	-- 13
	{	
		shortcut			= "/cheer",
		motion				= 16
	},
	
	-- 14
	{	
		shortcut			= "/clap",
		motion				= 17
	},
		
	-- 15
	{	
		shortcut			= "/cry",
		motion				= 18
	},
	
	-- 16
	{	
		shortcut			= "/dance",
		motion				= 19
	},
	
	-- 17
	{	
		shortcut			= "/lol",
		motion				= 20
	},
	
	-- 18
	{	
		shortcut			= "/str",
		motion				= 21
	},

	
	-- 19
	{	
		shortcut			= "/sit",
		motion				= 28
	},
	-- 22
	{	
		shortcut			= "/sleep",
		motion				= 29
	},
		
	-- 20
	{	
		shortcut			= "/sry",
		motion				= 27
	},
	
	-- 21
	{	
		shortcut			= "/anger",
		motion				= 25
	},
	{	
		shortcut			= "/provoke",
		motion				= 26
	},
	{	
		shortcut			= "/scared",
		motion				= 23
	},
	{	
		shortcut			= "/noo",
		motion				= 24
	},
	{	
		shortcut			= "/wcd",
		motion				= 22
	},
}	

