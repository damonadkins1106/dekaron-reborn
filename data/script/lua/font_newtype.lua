-- define font description

---------------------------------------
-- font header
---------------------------------------
font_advance = 1

han_font_name = "MS Sans Serif"
han_font_size = 40
han_font_dpi = 0

other_font_name = "MS Sans Serif"
other_font_size = 40
other_font_dpi = 0

---------------------------------------
-- shadow 0 : no shadow
-- shadow 1 : normal shadow
-- shadow 2 : full shadow
---------------------------------------

font_description = {	
--0
	{
		height			= 18,
		width			= 16,
		shadow			= 2,
	},
--1 로딩화면
	{
		height			= 18,
		width			= 16,
		shadow			= 2,
	},
	{
-- 2 기본폰트
		height			= 18,
		width			= 16,
		shadow			= 2,
	},
	{
-- 3 로그인 아이디 비번
		height			= 13,
		width			= 13,
		shadow			= 2,
	},
	{
-- 4 로그인 시스템
		height			= 16,
		width			= 16,
		shadow			= 0,
	},
-- 5 서버선택 서버이름
	{
		height			= 13,
		width			= 13,
		shadow			= 2,
	},
-- 6 캐릭터 선택 캐릭터 이름
	{
		height			= 13,
		width			= 13,
		shadow			= 2,
	},
-- 7 캐릭터선택 서버군
	{
		height			= 13,
		width			= 13,
		shadow			= 2,
	},
-- 8 캐릭터 선택 현재지역
	{
		height			= 13,
		width			= 13,
		shadow			= 2,
	},
-- 9 캐릭터 선택 레벨 직업
	{
		height			= 12,
		width			= 10,
		shadow			= 2,
	}
	,
-- 10 퀘스트 창 내용
	{
		height			= 12,
		width			= 10,
		shadow			= 2,
	}
	,
--11 미니도움말
	{
		height			= 18,
		width			= 16,
		shadow			= 2,
	},
	{
-- 12 NPC대화 폰트
		height			= 18,
		width			= 16,
		shadow			= 2,
	},
	{
-- 13 NPC대화 선택문
		height			= 13,
		width			= 13,
		shadow			= 2,
	},
	{
-- 14 도움말 내용
		height			= 16,
		width			= 16,
		shadow			= 0,
	},
-- 15 채팅 일반
	{
		height			= 13,
		width			= 13,
		shadow			= 2,
	},
-- 16 채팅 파티
	{
		height			= 13,
		width			= 13,
		shadow			= 2,
	},
-- 17 채팅 길드
	{
		height			= 13,
		width			= 13,
		shadow			= 2,
	},
-- 18 채팅 퀘스트
	{
		height			= 13,
		width			= 13,
		shadow			= 2,
	},
-- 19 채팅 전투+ 시스템전체
	{
		height			= 12,
		width			= 10,
		shadow			= 2,
	}
	,
-- 20 채팅 상점
	{
		height			= 12,
		width			= 10,
		shadow			= 2,
	}
	,
--21 채팅 귓속말
	{
		height			= 18,
		width			= 16,
		shadow			= 2,
	},
	{
-- 22 채팅 기타 시스템
		height			= 18,
		width			= 16,
		shadow			= 2,
	},
	{
-- 23 타겟창 타겟 이름
		height			= 13,
		width			= 13,
		shadow			= 2,
	},
	{
-- 24 타겟창 소유권 메시지
		height			= 16,
		width			= 16,
		shadow			= 0,
	},
-- 25 미니맵 맵이름
	{
		height			= 13,
		width			= 13,
		shadow			= 2,
	},
-- 26 미니맵 좌표
	{
		height			= 13,
		width			= 13,
		shadow			= 2,
	},
-- 27 확대 맵 이름
	{
		height			= 13,
		width			= 13,
		shadow			= 2,
	},
-- 28 메인바 캐릭터 이름
	{
		height			= 13,
		width			= 13,
		shadow			= 2,
	},
-- 29 메인바 캐릭터 레벨
	{
		height			= 12,
		width			= 10,
		shadow			= 2,
	}
	,
-- 30 메인바 경험치
	{
		height			= 12,
		width			= 10,
		shadow			= 2,
	}
	,
--31 메인바 남은 포션수
	{
		height			= 18,
		width			= 16,
		shadow			= 2,
	},
	{
-- 32 메인바 HP MP
		height			= 18,
		width			= 16,
		shadow			= 2,
	},
	{
-- 33 접속한 서버 서버군
		height			= 13,
		width			= 13,
		shadow			= 2,
	},
	{
-- 34 현재 ping
		height			= 16,
		width			= 16,
		shadow			= 0,
	},
-- 35 파티창 파티구호
	{
		height			= 13,
		width			= 13,
		shadow			= 2,
	},
-- 36 파티창 캐릭터 이름
	{
		height			= 13,
		width			= 13,
		shadow			= 2,
	},
-- 37 파티창 캐릭터 레벨
	{
		height			= 13,
		width			= 13,
		shadow			= 2,
	},
-- 38 파티창 캐릭터 현재 지역
	{
		height			= 13,
		width			= 13,
		shadow			= 2,
	},
-- 39 파티창 아이템 분배방식
	{
		height			= 12,
		width			= 10,
		shadow			= 2,
	}
	,
-- 40 스킬 툴팁 이름
	{
		height			= 12,
		width			= 10,
		shadow			= 2,
	}
	,
--41 스킬 툴팁 설명
	{
		height			= 18,
		width			= 16,
		shadow			= 2,
	},
	{
-- 42 스킬 툴팁 쿨타임 MP등
		height			= 18,
		width			= 16,
		shadow			= 2,
	},
	{
-- 43 아이템 툴팁 이름
		height			= 13,
		width			= 13,
		shadow			= 2,
	},
	{
-- 44 아이템 툴팁 아이템 옵션
		height			= 16,
		width			= 16,
		shadow			= 0,
	},
-- 45 아이템 툴팁 아이템 내용
	{
		height			= 13,
		width			= 13,
		shadow			= 2,
	},
-- 46 길드 공지사항
	{
		height			= 13,
		width			= 13,
		shadow			= 2,
	},
-- 47 길드 일반 내용( 길드원리스트, 직업, 계급,접속여부....)
	{
		height			= 13,
		width			= 13,
		shadow			= 2,
	},
-- 48 길드 메뉴 내용( 관리 메뉴, 기능메뉴,..)
	{
		height			= 13,
		width			= 13,
		shadow			= 2,
	},
-- 49 길드 길드 정보( 길드 이름, 레벨, 인원,..)
	{
		height			= 12,
		width			= 10,
		shadow			= 2,
	}
	,
-- 50 스킬창 스킬레벨
	{
		height			= 12,
		width			= 10,
		shadow			= 2,
	}
	,
-- 51 스킬창 스킬레벨업창
	{
		height			= 18,
		width			= 16,
		shadow			= 2,
	},
	{
-- 52 우편함 우편 내용
		height			= 18,
		width			= 16,
		shadow			= 2,
	},
	{
-- 53 우편함 제목
		height			= 13,
		width			= 13,
		shadow			= 2,
	},
	{
-- 54 우편함 남은시간
		height			= 16,
		width			= 16,
		shadow			= 0,
	},
-- 55 우편함 보낸 사람
	{
		height			= 13,
		width			= 13,
		shadow			= 2,
	},
-- 56 게임화면 캐릭터 이름
	{
		height			= 13,
		width			= 13,
		shadow			= 2,
	},
-- 57 게임화면 길드이름
	{
		height			= 13,
		width			= 13,
		shadow			= 2,
	},
-- 58 게임화면 몬스터이름
	{
		height			= 13,
		width			= 13,
		shadow			= 2,
	},
-- 59 게임화면 NPC이름
	{
		height			= 12,
		width			= 10,
		shadow			= 2,
	}
	,
-- 60 게임화면 아이템이름
	{
		height			= 12,
		width			= 10,
		shadow			= 2,
	}
	,
-- 61 일반 툴팁
	{
		height			= 12,
		width			= 10,
		shadow			= 2,
	}
	,
-- 62 메시지용
	{
		height			= 12,
		width			= 10,
		shadow			= 2,
	}
	,
-- 63 메시지용
	{
		height			= 12,
		width			= 10,
		shadow			= 2,
	}
}