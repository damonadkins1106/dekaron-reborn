<SKYEFFECT>
timeofday 	1
	<LENSFLARE>
	texturefile 	"Environment/Lensflare/lens_flare1.dds"
	texturefile 	"Environment/Lensflare/lens_flare2.dds"

		<FLARE>
			size 			0.5
			linepos			1
			color			204 204 255
			obscured		true
			textureid		0
		</FLARE>
		<FLARE>
			size			0.1
			linepos 		0.8
			color 			178 127 0
			obscured		false
			textureid 		1
		</FLARE>
		<FLARE>
			size			0.01
			linepos 		0.7
			color 			255 0 0
			obscured		false
			textureid 		1
		</FLARE>
		<FLARE>
			size			0.16
			linepos 		0.6
			color 			255 255 0
			obscured		false
			textureid 		0
		</FLARE>
		<FLARE>
			size			0.07
			linepos 		0.4
			color 			255 255 0
			obscured		false
			textureid 		1
		</FLARE>
		<FLARE>
			size			0.04
			linepos 		0.3
			color 			255 255 0
			obscured		false
			textureid 		1
		</FLARE>
	</LENSFLARE>
</SKYEFFECT>

<SKYEFFECT>
timeofday 	2
	<LENSFLARE>
	texturefile 	"Environment/Lensflare/lens_flare1.dds"
	texturefile 	"Environment/Lensflare/lens_flare2.dds"
		
		<FLARE>
			size 			0.5
			linepos			1
			color			204 204 255
			obscured		true
			textureid		0
		</FLARE>
		<FLARE>
			size			0.1
			linepos 		0.8
			color 			178 127 0
			obscured		false
			textureid 		1
		</FLARE>
		<FLARE>
			size			0.01
			linepos 		0.7
			color 			255 0 0
			obscured		false
			textureid 		1
		</FLARE>
		<FLARE>
			size			0.16
			linepos 		0.6
			color 			255 255 0
			obscured		false
			textureid 		0
		</FLARE>
		<FLARE>
			size			0.07
			linepos 		0.4
			color 			255 255 0
			obscured		false
			textureid 		1
		</FLARE>
		<FLARE>
			size			0.04
			linepos 		0.3
			color 			255 255 0
			obscured		false
			textureid 		1
		</FLARE>
	</LENSFLARE>
</SKYEFFECT>


<SKYEFFECT>
timeofday 	3
	<LENSFLARE>
	texturefile 	"Environment/Lensflare/lens_flare1.dds"
	texturefile 	"Environment/Lensflare/lens_flare2.dds"
			
		<FLARE>
			size 			0.5
			linepos			1
			color			204 204 255
			obscured		true
			textureid		0
		</FLARE>
		<FLARE>
			size			0.1
			linepos 		0.8
			color 			178 127 0
			obscured		false
			textureid 		1
		</FLARE>
		<FLARE>
			size			0.01
			linepos 		0.7
			color 			255 0 0
			obscured		false
			textureid 		1
		</FLARE>
		<FLARE>
			size			0.16
			linepos 		0.6
			color 			255 255 0
			obscured		false
			textureid 		0
		</FLARE>
		<FLARE>
			size			0.07
			linepos 		0.4
			color 			255 255 0
			obscured		false
			textureid 		1
		</FLARE>
		<FLARE>
			size			0.04
			linepos 		0.3
			color 			255 255 0
			obscured		false
			textureid 		1
		</FLARE>
	</LENSFLARE>
</SKYEFFECT>

<SKYEFFECT>
timeofday 	4
	<LENSFLARE>
	texturefile 	"Environment/Lensflare/lens_flare1.dds"
	texturefile 	"Environment/Lensflare/lens_flare2.dds"
			
		<FLARE>
			size 			0.5
			linepos			1
			color			204 204 255
			obscured		true
			textureid		0
		</FLARE>
		<FLARE>
			size			0.1
			linepos 		0.8
			color 			178 127 0
			obscured		false
			textureid 		1
		</FLARE>
		<FLARE>
			size			0.01
			linepos 		0.7
			color 			255 0 0
			obscured		false
			textureid 		1
		</FLARE>
		<FLARE>
			size			0.16
			linepos 		0.6
			color 			255 255 0
			obscured		false
			textureid 		0
		</FLARE>
		<FLARE>
			size			0.07
			linepos 		0.4
			color 			255 255 0
			obscured		false
			textureid 		1
		</FLARE>
		<FLARE>
			size			0.04
			linepos 		0.3
			color 			255 255 0
			obscured		false
			textureid 		1
		</FLARE>
	</LENSFLARE>
</SKYEFFECT>

<SKYEFFECT>
timeofday 	5
	<LENSFLARE>
	texturefile 	"Environment/Lensflare/moon.dds"

		<FLARE>
			size 			0.1
			linepos			1
			color			204 204 255
			obscured		true
			textureid		0
		</FLARE>	
	</LENSFLARE>
</SKYEFFECT>

<SKYEFFECT>
timeofday 	6
	<LENSFLARE>
	texturefile 	"Environment/Lensflare/moon.dds"

		<FLARE>
			size 			0.1
			linepos			1
			color			204 204 255
			obscured		true
			textureid		0
		</FLARE>	
	</LENSFLARE>
</SKYEFFECT>

<SKYEFFECT>
timeofday 	7
	<LENSFLARE>
	texturefile 	"Environment/Lensflare/moon2.dds"

		<FLARE>
			size 			0.4
			linepos			1
			color			204 204 255
			obscured		true
			textureid		0
		</FLARE>	
	</LENSFLARE>
</SKYEFFECT>
