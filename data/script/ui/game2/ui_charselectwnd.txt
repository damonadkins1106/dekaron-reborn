<window>
	name			char_select_win
	enable			true
	visible			true
	layer			100
	position		inverse inverse
	rect			0 0 0 0
	titlerect		0 0 0 0
	<panel>
		id		pan_widebackup
		enable		true
		visible		true
		intersect	false
		layer		99
		position	inverse top
		rect		0 0 0 48
		<image>
			key	0
			color	ff010101
			screen	common
			index	1
		</image>
	</panel>
	<panel>
		id		pan_widebackup_1
		enable		true
		visible		true
		intersect	false
		layer		99
		position	inverse top
		rect		0 48 0 32
		<image>
			key	0
			color	ff010101
			screen	login_wide
			index	2
		</image>
	</panel>
	<panel>
		id			pan_widebackdown
		enable		true
		visible		true
		intersect	false
		layer		99
		position	inverse bottom
		rect		0 -48 0 48
		<image>
			key	0
			color	ff010101
			screen	common
			index	1
		</image>
	</panel>
	<panel>
		id			pan_widebackdown_1
		enable		true
		visible		true
		intersect	false
		layer		99
		position	inverse bottom
		rect		0 -80 0 32
		<image>
			key	0
			color	ff010101
			screen	login_wide
			index	1
		</image>
	</panel>
	<label>
		id		lbl_server_title
		enable		true
		visible		true
		layer		120
		position	right top
		align       center
		rect		-362 32 119 17
		<title>
			color		ffffffff
			fontIndex	8
			text2		2000 43
		</title>
	</label>
	<label>
		id			lbl_char_title
		enable		true
		visible		true
		layer		120
		position	right top
		align       center
		rect		-232 32 191 17
		<title>
			color		ffffffff
			fontIndex	8
			text2		30 21
		</title>
	</label>
	<panel>
		id		pan_class_arrow1
		enable		true
		visible		true
		intersect 	false
		layer		111
		position	right top
		rect		-449 0 449 106
		<image>   
			key		0
			screen	Character_new
			index	121
		</image>
	</panel>

	<panel>
		id			pan_class_select1
		enable		true
		visible		true
		layer		102
		position	right top
		rect		-232 60 191 29
		<image>   
			key		0
			screen	Character_new
			index	125
		</image>
	</panel>
	<panel>
		id			pan_class_select2
		enable		true
		visible		true
		layer		102
		position	right top
		rect		-232 89 191 15
		<image>   
			key		0
			screen	Character_new
			index	126
		</image>
	</panel>
	<panel>
		id			pan_class_select3
		enable		true
		visible		true
		layer		102
		position	right top
		rect		-232 104 191 27
		<image>
			key		0
			screen	Character_new
			index	127
		</image>
		<button>
			id		btn_page_1
			enable	false
			visible	false
			layer	100
			blink 		true
			blinktime	500
			rect	76 6 37 19
			<image>
				key		none
				screen	Character_new
				index	128
			</image>
			<image>
				key		over
				screen	Character_new
				index	130
			</image>
			<image>
				key		blink
				screen	Character_new
				index	130
			</image>

		</button>
		<button>
			id		btn_page_2
			enable	false
			visible	false
			layer	100
			blink 		true
			blinktime	500
			rect	76 6 37 19
			<image>
				key		none
				screen	Character_new
				index	128
			</image>
			<image>
				key		over
				screen	Character_new
				index	129
			</image>
			<image>
				key		blink
				screen	Character_new
				index	129
			</image>
		</button>
	</panel>
	<panel>
		id			pan_bg
		enable		true
		visible		true
		layer		150
		position	right top
		rect		-227 60 180 453
		<button>
			id			character_list_btn_0
			enable		true
			visible		false
			layer		200
			type		0
			multiline	true
			lineheight	15
			rect		0 21 180 48
			<image>
				key		none
				color   0x00FFFFFF
				index	0
			</image>
			<image>
				key		over
				screen	Character_new
				index	11
			</image>
			<image>
				key		down
				screen	Character_new
				index	11
			</image>
			<image>
				key		check
				screen	Character_new
				index	11
			</image>
			<title>
				state		none
				color		ffffffff
				fontIndex	2
			</title>
			<title>
				state		over
				color		ff76f856
				fontIndex	2
			</title>
			<title>
				state		down
				color		ff76f856
				fontIndex	2
			</title>
			<title>
				state		check
				color		ff76f856
				fontIndex	2
			</title>
		</button>
		<button>
			id			character_list_btn_1
			enable		true
			visible		false
			layer		200
			type		0
			multiline	true
			lineheight	15
			rect		0 69 180 48
			<image>
				key		none
				color   0x00FFFFFF
				index	0
			</image>
			<image>
				key		over
				screen	Character_new
				index	11
			</image>
			<image>
				key		down
				screen	Character_new
				index	11
			</image>
			<image>
				key		check
				screen	Character_new
				index	11
			</image>
			<title>
				state		none
				color		ffffffff
				fontIndex	2
			</title>
			<title>
				state		over
				color		ff76f856
				fontIndex	2
			</title>
			<title>
				state		down
				color		ff76f856
				fontIndex	2
			</title>
			<title>
				state		check
				color		ff76f856
				fontIndex	2
			</title>
		</button>
		<button>
			id			character_list_btn_2
			enable		true
			visible		false
			layer		200
			type		0
			multiline	true
			lineheight	15
			rect		0 117 180 48
			<image>
				key		none
				color   0x00FFFFFF
				index	0
			</image>
			<image>
				key		over
				screen	Character_new
				index	11
			</image>
			<image>
				key		down
				screen	Character_new
				index	11
			</image>
			<image>
				key		check
				screen	Character_new
				index	11
			</image>
			<title>
				state		none
				color		ffffffff
				fontIndex	2
			</title>
			<title>
				state		over
				color		ff76f856
				fontIndex	2
			</title>
			<title>
				state		down
				color		ff76f856
				fontIndex	2
			</title>
			<title>
				state		check
				color		ff76f856
				fontIndex	2
			</title>
		</button>
		<button>
			id			character_list_btn_3
			enable		true
			visible		false
			layer		200
			type		0
			multiline	true
			lineheight	15
			rect		0 165 180 48
			<image>
				key		none
				color   0x00FFFFFF
				index	0
			</image>
			<image>
				key		over
				screen	Character_new
				index	11
			</image>
			<image>
				key		down
				screen	Character_new
				index	11
			</image>
			<image>
				key		check
				screen	Character_new
				index	11
			</image>
			<title>
				state		none
				color		ffffffff
				fontIndex	2
			</title>
			<title>
				state		over
				color		ff76f856
				fontIndex	2
			</title>
			<title>
				state		down
				color		ff76f856
				fontIndex	2
			</title>
			<title>
				state		check
				color		ff76f856
				fontIndex	2
			</title>
		</button>
		<button>
			id			character_list_btn_4
			enable		true
			visible		false
			layer		200
			type		0
			multiline	true
			lineheight	15
			rect		0 213 180 48
			<image>
				key		none
				color   0x00FFFFFF
				index	0
			</image>
			<image>
				key		over
				screen	Character_new
				index	11
			</image>
			<image>
				key		down
				screen	Character_new
				index	11
			</image>
			<image>
				key		check
				screen	Character_new
				index	11
			</image>
			<title>
				state		none
				color		ffffffff
				fontIndex	2
			</title>
			<title>
				state		over
				color		ff76f856
				fontIndex	2
			</title>
			<title>
				state		down
				color		ff76f856
				fontIndex	2
			</title>
			<title>
				state		check
				color		ff76f856
				fontIndex	2
			</title>
		</button>
		<button>
			id			character_list_btn_5
			enable		true
			visible		false
			layer		200
			type		0
			multiline	true
			lineheight	15
			rect		0 261 180 48
			<image>
				key		none
				color   0x00FFFFFF
				index	0
			</image>
			<image>
				key		over
				screen	Character_new
				index	11
			</image>
			<image>
				key		down
				screen	Character_new
				index	11
			</image>
			<image>
				key		check
				screen	Character_new
				index	11
			</image>
			<title>
				state		none
				color		ffffffff
				fontIndex	2
			</title>
			<title>
				state		over
				color		ff76f856
				fontIndex	2
			</title>
			<title>
				state		down
				color		ff76f856
				fontIndex	2
			</title>
			<title>
				state		check
				color		ff76f856
				fontIndex	2
			</title>
		</button>
		<button>
			id			character_list_btn_6
			enable		true
			visible		false
			layer		200
			type		0
			multiline	true
			lineheight	15
			rect		0 309 180 48
			<image>
				key		none
				color   0x00FFFFFF
				index	0
			</image>
			<image>
				key		over
				screen	Character_new
				index	11
			</image>
			<image>
				key		down
				screen	Character_new
				index	11
			</image>
			<image>
				key		check
				screen	Character_new
				index	11
			</image>
			<title>
				state		none
				color		ffffffff
				fontIndex	2
			</title>
			<title>
				state		over
				color		ff76f856
				fontIndex	2
			</title>
			<title>
				state		down
				color		ff76f856
				fontIndex	2
			</title>
			<title>
				state		check
				color		ff76f856
				fontIndex	2
			</title>
		</button>
		<button>
			id			character_list_btn_7
			enable		true
			visible		false
			layer		200
			type		0
			multiline	true
			lineheight	15
			rect		0 357 180 48
			<image>
				key		none
				color   0x00FFFFFF
				index	0
			</image>
			<image>
				key		over
				screen	Character_new
				index	11
			</image>
			<image>
				key		down
				screen	Character_new
				index	11
			</image>
			<image>
				key		check
				screen	Character_new
				index	11
			</image>
			<title>
				state		none
				color		ffffffff
				fontIndex	2
			</title>
			<title>
				state		over
				color		ff76f856
				fontIndex	2
			</title>
			<title>
				state		down
				color		ff76f856
				fontIndex	2
			</title>
			<title>
				state		check
				color		ff76f856
				fontIndex	2
			</title>
		</button>
		<button>
			id			character_list_btn_8
			enable		true
			visible		false
			layer		200
			type		0
			multiline	true
			lineheight	15
			rect		0 405 180 48
			<image>
				key		none
				color   0x00FFFFFF
				index	0
			</image>
			<image>
				key		over
				screen	Character_new
				index	11
			</image>
			<image>
				key		down
				screen	Character_new
				index	11
			</image>
			<image>
				key		check
				screen	Character_new
				index	11
			</image>
			<title>
				state		none
				color		ffffffff
				fontIndex	2
			</title>
			<title>
				state		over
				color		ff76f856
				fontIndex	2
			</title>
			<title>
				state		down
				color		ff76f856
				fontIndex	2
			</title>
			<title>
				state		check
				color		ff76f856
				fontIndex	2
			</title>
		</button>
	</panel>
	<button>
		id			btn_servertransfer
		enable		false
		visible		false
		layer		200
		type		0
		position	right bottom
		rect		-290 -42 90 16
		<title>
			state		none
			color		ffffffff
			fontIndex	8
			text2		275 5
                        effect          1001 1 3000 0
		</title>
		<title>
			state		over
			color		ffc0e7ff
			fontIndex	8
			effect          1001 2 500 1
		</title>
		<title>
			state		down
			color		ffc0e7ff
			fontIndex	8
			position	2 2
                       		 effect          1000 3 500 1
		</title>
	</button>
	<button>
		id			btn_cloning_char
		enable		false
		visible		false
		layer		200
		type		0
		position	right bottom
		rect		-310 -42 90 16
		<title>
			state		none
			color		ffffffff
			fontIndex	8
			text2		30 34
                        effect          1001 1 3000 0
		</title>
		<title>
			state		over
			color		ffc0e7ff
			fontIndex	8
			effect          1001 2 500 1
		</title>
		<title>
			state		down
			color		ffc0e7ff
			fontIndex	8
			position	2 2
                       		 effect          1000 3 500 1
		</title>
	</button>
	<button>
		id			btn_create_char
		enable		true
		visible		true
		layer		200
		type		0
		position	right bottom
		rect		-232 -109 109 44
		align		center
		<image>
			key		none
			screen	Character
			index	21
		</image>
		<image>
			key		over
			screen	Character
			index	22
		</image>
		<image>
			key		down
			screen	Character
			index	23
		</image>
		<title>
			state		none
			color		ffffffff
			fontIndex	8
			text2		30 22
			position	0 14
                        effect          1000 1 3000 0
		</title>
		<title>
			state		over
			color		ffc0e7ff
			fontIndex	8
			position	0 14
                        effect          1000 2 500 1
		</title>
		<title>
			state		down
			color		ffc0e7ff
			fontIndex	8
			position	2 16
                        effect          1000 3 500 1
		</title>
	</button>
	<button>
		id			btn_delete_char
		enable		true
		visible		true
		layer		200
		type		0
		position	right bottom
		rect		-116 -109 109 44
		align		center
		<image>
			key		none
			screen	Character
			index	24
		</image>
		<image>
			key		over
			screen	Character
			index	25
		</image>
		<image>
			key		down
			screen	Character
			index	26
		</image>
		<title>
			state		none
			color		ffffffff
			fontIndex	8
			text2		30 23
			position	0 14
                        effect          1000 1 3000 0
		</title>
		<title>
			state		over
			color		ffc0e7ff
			fontIndex	8
			position	0 14
                        effect          1000 2 500 1
		</title>
		<title>
			state		down
			color		ffc0e7ff
			fontIndex	8
			position	2 16
                        effect          1000 3 500 1
		</title>
	</button>
	<panel>
		id			pan_select_char
		enable		true
		visible		false
		layer		120
		position	center top
		rect		-102 105 213 61
		<image>
				key		0
				screen	Character_new
				index	12
		</image>
		<label>
			id			lbl_select_msg
			enable		true
			visible		true
			layer		121
			rect		26 10 163 44
			align		center
			multiline 	true
			<title>
				color		ffffffff
				fontIndex	5
				position	1 1
			</title>
		</label>
	</panel>
	<panel>
		id			pan_connect
		enable		true
		visible		true
		layer		109
		position	center bottom
		rect		-78 -139 158 44
		<image>
				key		0
				screen	Character_new
				index	13
		</image>
		<button>
			id			btn_connect
			enable		false
			visible		true
			layer		110
			type		0
			align		center
			rect		0 0 152 51
			<image>
				key		none
				screen	Character
				index	8
			</image>
			<image>
				key		over
				screen	Character
				index	9
			</image>
			<image>
				key		down
				screen	Character
				index	10
			</image>
			<title>
				state		none
				color		ffffffff
				fontIndex	8
				text2		30 24
				position	0 15
				effect          1000 1 3000 0
			</title>
			<title>
				state		over
				color		ffc0e7ff
				fontIndex	8
				position	0 15
				effect          1000 2 500 1
			</title>
			<title>
				state		down
				color		ffc0e7ff
				fontIndex	8
				position	2 17
				effect          1000 3 500 1
			</title>
			<title>
				state		disable
				color		ffffffff
				fontIndex	8
				position	0 15
			</title>
		</button>
	</panel>	<button>
		id			btn_exit
		enable		true
		visible		true
		layer		200
		type		0
		position	left bottom
		rect		7 -109 109 44
		align		center
		<image>
			key		none
			screen	Character
			index	24
		</image>
		<image>
			key		over
			screen	Character
			index	25
		</image>
		<image>
			key		down
			screen	Character
			index	26
		</image>
		<title>
			state		none
			color		ffffffff
			fontIndex	8
			text2		2000 13
			position	0 14
                        effect          1000 1 3000 0
		</title>
		<title>
			state		over
			color		ffc0e7ff
			fontIndex	8
			position	0 14
                        effect          1000 2 500 1
		</title>
		<title>
			state		down
			color		ffc0e7ff
			fontIndex	8
			position	2 16
                        effect          1000 3 500 1
		</title>
		<title>
			state		disable
			color		ffffffff
			fontIndex	8
		</title>
	</button>
	<button>
		id			btn_logout
		enable		true
		visible		true
		layer		200
		type		0
		position	left bottom
		rect		123 -109 109 44
		align		center
		<image>
			key		none
			screen	Character
			index	21
		</image>
		<image>
			key		over
			screen	Character
			index	22
		</image>
		<image>
			key		down
			screen	Character
			index	23
		</image>
		<title>
			state		none
			color		ffffffff
			fontIndex	8
			text2		30 20
			position	0 14
                        effect          1000 1 3000 0
		</title>
		<title>
			state		over
			color		ffc0e7ff
			fontIndex	8
			position	0 14
                        effect          1000 2 500 1
		</title>
		<title>
			state		down
			color		ffc0e7ff
			fontIndex	8
			position	2 16
                        effect          1000 3 500 1
		</title>
		<title>
			state		disable
			color		ffffffff
			fontIndex	8
		</title
</window>