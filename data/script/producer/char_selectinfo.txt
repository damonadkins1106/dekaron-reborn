stand		0	2.70
stand_special		0	6.70
mapindex		0	100
scale		0	1.20
horizon		0	0.02
vertical		0	0.03
cameratarget	0	120.78	2.45	56.24
position		0	120.93	63.96
direction		0	359.22


stand		1	2.00
stand_special		1	2.20
mapindex		1	101
scale		1	1.10
horizon		1	0.01
vertical		1	0.04
cameratarget	1	66.37	0.94	50.31
position		1	66.19	43.12
direction		1	360.42


stand		2	2.70
stand_special		2	6.00
mapindex		2	102
scale		2	1.20
horizon		2	3.15
vertical		2	-0.05
cameratarget	2	64.06	0.55	89.95
position		2	64.06	82.78
direction		2	182.42


stand		3	2.70
stand_special		3	3.00
mapindex		3	103
scale		3	1.10
horizon		3	0.02
vertical		3	-0.02
cameratarget	3	66.20	-0.34	37.44
position		3	66.26	44.85
direction		3	11.87


stand		4	3.00
stand_special		4	5.00
mapindex		4	104
scale		4	1.20
horizon		4	6.28
vertical		4	0.00
cameratarget	4	65.46	1.63	33.30
position		4	65.41	41.08
direction		4	0.40


stand		5	3.00
stand_special		5	5.00
mapindex		5	106
scale		5	1.20
horizon		5	4.70
vertical		5	0.05
cameratarget	5	92.14	7.81	63.89
position		5	98.00	63.97
direction		5	-81.77


stand		6	2.70
stand_special		6	5.00
mapindex		6	107
scale		6	1.20
horizon		6	0.00
vertical		6	0.00
cameratarget	6	120.51	-0.23	57.65
position		6	120.43	64.64
direction		6	0.70

stand		7	6.70
stand_special		7	6.70
mapindex		7	100
scale		7	1.20
horizon		7	0.02
vertical		7	0.03
cameratarget	7	120.78	2.45	56.24
position		7	120.93	63.96
direction		7	359.22

stand		9	10.5
stand_special		9	10.5
mapindex		9	102
scale		9	1.20
horizon		9	3.15
vertical		9	-0.05
cameratarget	9	64.06	0.55	89.95
position		9	64.06	82.78
direction		9	182.42

stand		10	6.20
stand_special		10	6.20
mapindex		10	103
scale		10	1.10
horizon		10	0.02
vertical7	10	-0.02
cameratarget	10	66.20	-0.34	37.44
position		10	66.26	44.85
direction		10	11.87

stand		11	6.00
stand_special		11	6.00
mapindex		11	104
scale		11	1.20
horizon		11	6.28
vertical		11	0.00
cameratarget	11	65.46	1.63	33.30
position		11	65.41	41.08
direction		11	0.40

stand		12	11.33
stand_special		12	11.33
mapindex		12	106
scale		12	1.40
horizon		12	4.70
vertical		12	0.05
cameratarget	12	92.14	7.81	63.89
position		12	98.00	63.97
direction		12	-81.77

