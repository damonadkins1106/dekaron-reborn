block begin

rc_envmap "water/skyCube=.dds"

maptexturepath		MapTile\norak\
load_map		prison.map
load_object		prison.mol
load_effect		prison.mel
load_attribute		prison.mal

3dcamerainfo_near	1	1	88
3dcamerainfo_far	1000	1 	88
3dcamerainfo_height 	1.3	1
camera_horizontal	3.14

mainlightpos		1 1000 1
mainlightdistance	0
mainlightintensity	0

ambientlightcolor		.5 .5 .5
ambientlightintensity		.7
directionallight_intensity	1.5
directionallight_dir		1 3 1
directionallight_color		1 1 1

shadelight_pos			0 0 0
shadelight_distance		10000

//ambientlightcolor		.5 .7 .5
//ambientlightintensity		.7
//directionallight_intensity	2
//directionallight_dir		1 3 1
//directionallight_color	1 1 1

//dirbacklight_intensity	1.0
//dirbacklight_dir	1 -3 1
//dirbacklight_color	1 1 1

//shadelight_pos			0 0 0
//shadelight_distance		4

setposition_toolplayer	55 0 48

rc_fog 0
3dcamerafog 0 1000

block end