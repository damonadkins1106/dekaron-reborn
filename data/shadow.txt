[init]
title			Dekaron
mode			1
buffer_count		1
texture_depth		16
z_depth			16
display_depth		16
display_width		1024
display_height		768
trilinear_filtering	0
anisotropy_filtering	0 
backlight		1


[path]
PackIO			0
path_action		Action\
path_texture		Texture\
path_map		Map\
path_mapobject		Map\
path_mapkeepout		Share\MapRedCell\
path_mapeffect		Map\
path_mappack		Map\
path_anidesc		Property\AnimDescription\
path_actiondesc		Property\ActionDescription\
path_bone		Skel\
path_bonemesh		Mesh\
path_boneanimation	Anim\
path_shader		Binary\
path_objectpack		Object\Pack\
path_simpleobject	Object\Static\
path_effect		Property\Effect\
path_water		Property\
path_property		Property\
path_texturepack	TexturePack\
path_effectpd		Property\Effect\
path_script		Script\
path_itemtable		Etc\
path_monstertable	Etc\
path_exptable		Etc\
path_luascript		Script\Lua\
path_preload		property\preload\
path_sound		Sound\

[camera]
camera_distance		12.0f
camera_latitude		0.8f
camera_longitude	1.0f